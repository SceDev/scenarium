/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

import static com.jogamp.opengl.GL.GL_FLOAT;
import static com.jogamp.opengl.fixedfunc.GLPointerFunc.GL_VERTEX_ARRAY;

import java.nio.FloatBuffer;
import java.util.function.Function;

import org.scenarium.struct.GeographicCoordinate;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL2;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class CrossMarker extends GeographicalMarker {
	private final double crossSize;
	private final Color color;
	private final boolean skewed;

	public CrossMarker(GeographicCoordinate coordinate, double crossSize, Color color, boolean skewed) {
		super(coordinate);
		this.crossSize = crossSize;
		this.color = color;
		this.skewed = skewed;
	}

	public CrossMarker(GeographicCoordinate coordinate, double crossSize, Color color) {
		this(coordinate, crossSize, color, false);
	}

	public CrossMarker(GeographicCoordinate coordinate, double crossSize) {
		this(coordinate, crossSize, Color.BLACK);
	}

	public CrossMarker(GeographicCoordinate coordinate) {
		this(coordinate, 1, Color.BLACK);
	}

	@Override
	public void draw(GraphicsContext gc, Function<GeographicCoordinate, Point2D> geoToScreen) {
		gc.setStroke(this.color);
		Point2D p = geoToScreen.apply(coordinate);
		if (this.skewed) {
			gc.strokeLine(p.getX() + 5, p.getY(), p.getX() - 5, p.getY());
			gc.strokeLine(p.getX(), p.getY() + 5, p.getX(), p.getY() - 5);
		} else {
			gc.strokeLine(p.getX() + 5, p.getY() + 5, p.getX() - 5, p.getY() - 5);
			gc.strokeLine(p.getX() + 5, p.getY() - 5, p.getX() - 5, p.getY() + 5);
		}
	}

	@Override
	public void draw(GL2 gl, Color color, Function<GeographicCoordinate, Point2D> geoToOpenGL) {
		Point2D pos = geoToOpenGL.apply(coordinate);
		float[] data = new float[1 * 4 * 3];
		int index = 0;
		data[index] = (float) (pos.getX() + this.crossSize);
		data[index + 1] = (float) (pos.getY() + this.crossSize);
		data[index + 2] = (float) 0.03;
		data[index + 3] = (float) (pos.getX() - this.crossSize);
		data[index + 4] = (float) (pos.getY() - this.crossSize);
		data[index + 5] = (float) 0.03;
		data[index + 6] = (float) (pos.getX() + this.crossSize);
		data[index + 7] = (float) (pos.getY() - this.crossSize);
		data[index + 8] = (float) 0.03;
		data[index + 9] = (float) (pos.getX() - this.crossSize);
		data[index + 10] = (float) (pos.getY() + this.crossSize);
		data[index + 11] = (float) 0.03;
		FloatBuffer dataBuffer = Buffers.newDirectFloatBuffer(data);
		dataBuffer.rewind();
		gl.glPointSize(3);
		gl.glLineWidth(1);
		gl.glEnableClientState(GL_VERTEX_ARRAY);
		gl.glVertexPointer(3, GL_FLOAT, 0, dataBuffer);
		gl.glColor3f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue());
		gl.glDrawArrays(1, 0, data.length / 3);
		gl.glDisableClientState(GL_VERTEX_ARRAY);
	}

	@Override
	public CrossMarker clone() {
		return new CrossMarker(coordinate, this.crossSize, this.color, this.skewed);
	}
}
