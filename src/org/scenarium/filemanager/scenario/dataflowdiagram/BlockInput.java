/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.util.concurrent.locks.ReentrantLock;

import org.scenarium.filemanager.scenario.dataflowdiagram.operator.LinkChangeListener;

public class BlockInput extends Input implements BlockIO {
	protected final Block block;
	public final boolean isVarArgs;

	public BlockInput(Block block, Class<?> type, String name, boolean isVarArgs) {
		super(type, name);
		this.block = block;
		this.isVarArgs = isVarArgs;
	}

	@Override
	public Block getBlock() {
		return this.block;
	}

	@Override
	public IOComponent getComponent() {
		return this.block;
	}

	@Override
	public Link getLink() {
		return this.link;
	}

	public boolean isVarArgs() {
		return this.isVarArgs;
	}

	@Override
	public boolean setLink(Link link) {
		return setLink(link, true);
	}

	boolean setLink(Link link, boolean needToUpdateVarArgs) {
		ReentrantLock lock = this.block.getIOLock();
		lock.lock(); // Permet de bloquer le run pendant les modification de lien
		try {
			Input oldInput = this.link == null ? null : this.link.getInput();
			boolean success = super.setLink(link);
			if (this.isVarArgs && needToUpdateVarArgs)
				this.block.varArgsInputChanged(this);
			if (oldInput != null)
				this.block.fireInputLinksChangeListener(oldInput);
			if (link != null && link.getInput() != null)
				this.block.fireInputLinksChangeListener(link.getInput());
			if (this.block.operator instanceof LinkChangeListener) {
				if (oldInput != null)
					((LinkChangeListener) this.block.operator).linkChanged(this.block.getInputs().indexOf(oldInput));
				if (link != null && link.getInput() != null)
					((LinkChangeListener) this.block.operator).linkChanged(this.block.getInputs().indexOf(link.getInput()));
			}
			return success;
		} finally {
			lock.unlock();
		}
	}
}
