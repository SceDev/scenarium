/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.dataprocessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.LinkChangeListener;
import org.scenarium.tools.SchedulerUtils;

public class ReSampler extends EvolvedOperator implements LinkChangeListener {
	@PropertyInfo(index = 0, info = "The resampled step time.", unit = "ms")
	@NumberInfo(min = 1)
	private int stepTime = 50;
	private volatile ScheduledExecutorService timer;
	private ArrayBlockingQueue<Runnable> tasks;

	private int nbData;
	private Object[] outputData;
	private boolean isAllNonNull = false;

	@Override
	public void initStruct() {
		String[] oltin = getOutputLinkToInputNames();
		Class<?>[] oltit = getOutputLinkToInputTypes();
		this.nbData = oltin.length - 1;
		this.outputData = new Object[this.nbData];
		String[] names = new String[this.nbData];
		Class<?>[] types = new Class<?>[this.nbData];
		for (int i = 0; i < types.length; i++) {
			names[i] = oltin[i] + "-" + i;
			types[i] = oltit[i];
			if (types[i] == null)
				return;
		}
		updateOutputs(names, types);
	}

	@Override
	public void birth() {
		onStart(this::startTimer);
		onResume(this::startTimer);
		onPause(this::death);
	}

	private synchronized void startTimer() {
		this.timer = SchedulerUtils.getTimer(getBlockName() + "-Timer");
		this.timer.scheduleAtFixedRate(() -> {
			if (!this.isAllNonNull) {
				for (Object o : this.outputData)
					if (o == null)
						return;
				this.isAllNonNull = true;
			}
			triggerOutput(this.outputData);
		}, 0, this.stepTime, TimeUnit.MILLISECONDS);
	}

	public void process(Object... in) {
		if (in.length == this.outputData.length)
			for (int i = 0; i < in.length; i++)
				if (in[i] != null)
					this.outputData[i] = in[i];
	}

	@Override
	public synchronized void death() {
		if (this.timer != null) {
			this.timer.shutdownNow();
			this.timer = null;
			ArrayList<Runnable> rest = new ArrayList<>();
			if (this.tasks != null) {
				this.tasks.drainTo(rest);
				this.tasks = null;
				for (Runnable runnable : rest)
					runnable.run();
			}
		}
		if (this.outputData != null)
			Arrays.fill(this.outputData, null);
		this.isAllNonNull = false;
	}

	public int getStepTime() {
		return this.stepTime;
	}

	public void setStepTime(int stepTime) {
		int oldStepTime = this.stepTime;
		this.stepTime = stepTime;
		this.pcs.firePropertyChange("stepTime", oldStepTime, this.stepTime);
		restartLater();
	}

	@Override
	public void linkChanged(int indexOfInput) {
		runLater(() -> runSynchronously(this::initStruct));
	}

	private synchronized void runSynchronously(Runnable task) {
		if (this.timer != null) {
			if (this.tasks == null)
				this.tasks = new ArrayBlockingQueue<>(2);
			try {
				this.tasks.put(task);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.timer.execute(() -> {
				ReSampler.this.tasks.remove(task);
				task.run();
			});
		} else
			task.run();
	}
}
