/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.util.Objects;

import javax.swing.event.EventListenerList;

import org.beanmanager.editors.TransientProperty;
import org.beanmanager.tools.WrapperTools;

import javafx.geometry.Point2D;

public abstract class IO {
	private final EventListenerList listeners = new EventListenerList();
	protected Class<?> type;
	private String name;
	private Point2D position;
	@TransientProperty
	private String errorMessage;

	public IO(Class<?> type, String name) {
		this.type = WrapperTools.toWrapper(type);
		this.name = name;
	}

	public void addIONameChangeListener(IONameChangeListener listener) {
		this.listeners.add(IONameChangeListener.class, listener);
	}

	public void removeIONameChangeListener(IONameChangeListener listener) {
		this.listeners.remove(IONameChangeListener.class, listener);
	}

	public void fireNameChange() {
		for (IONameChangeListener listener : this.listeners.getListeners(IONameChangeListener.class))
			listener.nameChanged(this);
	}

	public String getName() {
		return this.name;
	}

	public Point2D getPosition() {
		return this.position;
	}

	public Class<?> getType() {
		return this.type;
	}

	public void setName(String name) {
		this.name = name;
		fireNameChange();
	}

	public void setPosition(Point2D position) {
		this.position = position;
	}

	public void setError(String message) {
		if (!Objects.equals(this.errorMessage, message)) {
			this.errorMessage = message;
			fireErrorChange();
		}
	}

	public String getError() {
		return this.errorMessage;
	}

	public void addErrorChangeListener(InputErrorChangeListener listener) {
		this.listeners.add(InputErrorChangeListener.class, listener);
	}

	public void removeErrorChangeListener(InputErrorChangeListener listener) {
		this.listeners.remove(InputErrorChangeListener.class, listener);
	}

	public void fireErrorChange() {
		for (InputErrorChangeListener listener : this.listeners.getListeners(InputErrorChangeListener.class))
			listener.errorChange();
	}

	@Override
	public String toString() {
		return this.name;
	}
}

enum Type {
	PROPERTY, STATIC, DYNAMIC, VAR_ARGS
}
