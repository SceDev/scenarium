/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test.io;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;

public class SimpleIOTest {
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	private SubMyOp prop;
	private int ours = 5;

	public void birth() {
		new Thread(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			process(0, 0.0, (short) 0);
		}).start();
	}

	public void death() throws Exception {

	}

	public boolean process(Integer a, Double b, Short... c) {
		return true;
	}

	public int getOutput2() {
		return this.prop.getVache() + 7;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		if (Arrays.asList(this.pcs.getPropertyChangeListeners()).contains(listener))
			System.out.println("remove ok");
		this.pcs.removePropertyChangeListener(listener);
	}

	public SubMyOp getProp() {
		return this.prop;
	}

	public void setProp(SubMyOp prop) {
		SubMyOp oldProp = this.prop;
		this.prop = prop;
		this.pcs.firePropertyChange("prop", oldProp, prop);
	}

	public int getOurs() {
		return this.ours;
	}

	public void setOurs(int ours) {
		int oldValue = this.ours;
		this.ours = ours;
		this.pcs.firePropertyChange("ours", oldValue, ours);
	}
}
