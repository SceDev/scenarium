package org.scenarium.operator.dataprocessing;

public enum SynchronizerType {
	TIMESTAMP, ALL_AND_LAST_ARRIVED
}
