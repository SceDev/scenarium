/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.util.Arrays;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class ListenerTest extends EvolvedOperator {

	@Override
	public void birth() throws Exception {
		onStart(() -> System.out.println(getBlockName() + ": start"));
		onResume(() -> System.out.println(getBlockName() + ": resume"));
		onPause(() -> System.out.println(getBlockName() + ": pause"));
		onStop(() -> System.out.println(getBlockName() + ": stop"));
		// onRecording();
		addBlockNameChangeListener((oldBeanDesc, newBeanDesc) -> System.out.println(getBlockName() + ": name changed from: " + oldBeanDesc.name + " to: " + newBeanDesc.name));
		addDeclaredInputChangeListener((names, types) -> System.out.println(getBlockName() + ": declaredInput changed to: " + Arrays.toString(names) + " " + Arrays.toString(types)));
		// addRecordingChangeListener(isRecording -> System.out.println(getBlockName() + ": Recording state changed to: " + isRecording));
	}

	public Integer process() {
		return null;
	}

	@Override
	public void death() throws Exception {}

}
