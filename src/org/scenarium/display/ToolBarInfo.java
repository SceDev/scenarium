/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import org.scenarium.display.toolbarclass.Tool;

import javafx.scene.input.KeyCode;

public class ToolBarInfo {
	public final Class<? extends Tool> type;
	public final KeyCode keyCode;
	public final boolean onlyMainFrame;

	public ToolBarInfo(Class<? extends Tool> toolBarClass, KeyCode keyCode, boolean onlyMainFrame) {
		this.type = toolBarClass;
		this.keyCode = keyCode;
		this.onlyMainFrame = onlyMainFrame;
	}

	@Override
	public String toString() {
		return this.type.getSimpleName() + "KeyCode: " + this.keyCode;
	}
}
