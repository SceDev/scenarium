/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.bugSize;

import java.io.File;
import java.util.HashSet;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.struct.Selection;
import org.scenarium.communication.can.dbc.CanDBC;
import org.scenarium.communication.can.dbc.EncodingMessageProperties;
import org.scenarium.communication.can.dbc.MessageIdentifier;
import org.scenarium.editors.can.dbc.EncodingMessagePropertiesEditor;
import org.scenarium.operator.network.encoder.CanDBCEncoder;

import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class Bmaccor {

	private Bmaccor() {}

	public static void main(String[] args) {
		PropertyEditorManager.registerEditor(EncodingMessageProperties[].class, EncodingMessagePropertiesEditor.class);

		CanDBCEncoder bean = new CanDBCEncoder();
		bean.setCanDBC(new CanDBC(new File("/home/revilloud/Téléchargements/Fwd TR  Mobileye Extended Protocol documentation + dbc files(1)/ExtLogData2.dbc")));

		int i = 0;
		HashSet<MessageIdentifier> h = new HashSet<>();
		for (MessageIdentifier mi : bean.getCanDBC().getMessagesIdentifier()) {
			if (i++ == 10)
				break;
			h.add(mi);
		}

		bean.setFilters(new Selection<>(h, MessageIdentifier.class));
		final BeanManager beanGui = new BeanManager(bean, "");
		BeanEditor.registerBean(bean, "");
		// beanGui.load();
		new FxTest().launchIHM(args, s -> {
			GridPane editor = beanGui.getEditor();
			ScrollPane sp = new ScrollPane(editor);

			beanGui.addSizeChangeListener(() -> {
				s.sizeToScene();
			});
			return new VBox(sp, new Button("down"));
		}, false);
	}
}
