/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class RmiOperatorTest extends EvolvedOperator {
	private int patate;
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		// System.out.println("addPropertyChangeListener from " + ProcessHandle.current().pid());
		this.pcs.addPropertyChangeListener(listener);
	}

	@Override
	public void birth() {
		System.out.println("birth, pid: " + ProcessHandle.current().pid() + " patate: " + this.patate + " isAlive: " + isAlive() + " running: " + isRunning());
		addBlockNameChangeListener((old, newOne) -> System.out.println(newOne.name + " From: " + ProcessHandle.current().pid()));
		addInputLinksChangeListener((indexOfInput) -> System.out.println("InputLinksChangeListener: " + indexOfInput + " From: " + ProcessHandle.current().pid()));
		onStart(() -> System.out.println("Started from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning()));
		onResume(() -> System.out.println("Resumed from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning()));
		onPause(() -> System.out.println("Paused from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning()));
		onStop(() -> System.out.println("Stopped from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning()));
		setPatate(10);
	}

	@Override
	public void death() {
		System.out.println("death, pid: " + ProcessHandle.current().pid() + " patate: " + this.patate + " isAlive: " + isAlive() + " running: " + isRunning());
	}

	public int getPatate() {
		return this.patate;
	}

	public Integer process(Byte i, Integer j, Byte... k) {
		System.out.println("Process from " + ProcessHandle.current().pid() + " isAlive: " + isAlive() + " running: " + isRunning());
		// System.out.println("TimeStamp: " + getTimeStamp(0));
		// System.out.println("process, pid: " + ProcessHandle.current().pid() + " patate: " + patate);
		if (i == null || j == null || k == null)
			return 5;
		int res = i + j;
		for (int l = 0; l < k.length; l++)
			res += k[l] == null ? 0 : k[l];
		return res;
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public void setPatate(int patate) {
		// System.out.println("setPatate, pid: " + ProcessHandle.current().pid() + " patate: " + patate);
		int oldPatate = this.patate;
		this.patate = patate;
		this.pcs.firePropertyChange("patate", oldPatate, patate);
	}
}
