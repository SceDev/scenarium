/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import javax.vecmath.Point2i;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.SubBeanExpansionListener;
import org.beanmanager.editors.DynamicSizeEditorChangeListener;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.rmi.RMIBeanManager;
import org.beanmanager.rmi.client.RemoteBean;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.beanmanager.struct.TreeRoot;
import org.beanmanager.tools.FxUtils;
import org.beanmanager.tools.Tuple;
import org.beanmanager.tools.WrapperTools;
import org.scenarium.display.AlertUtil;
import org.scenarium.display.AnimationTimerConsumer;
import org.scenarium.display.RenderPane;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.toolBar.Operators;
import org.scenarium.editors.NotChangeableAtRuntime;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockIO;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockOutput;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramOutput;
import org.scenarium.filemanager.scenario.dataflowdiagram.IO;
import org.scenarium.filemanager.scenario.dataflowdiagram.IllegalInputArgument;
import org.scenarium.filemanager.scenario.dataflowdiagram.Input;
import org.scenarium.filemanager.scenario.dataflowdiagram.Link;
import org.scenarium.filemanager.scenario.dataflowdiagram.LocalisedObject;
import org.scenarium.filemanager.scenario.dataflowdiagram.ModificationType;
import org.scenarium.filemanager.scenario.dataflowdiagram.Output;
import org.scenarium.filemanager.scenario.dataflowdiagram.ProcessMode;
import org.scenarium.filemanager.scenario.dataflowdiagram.RunningStateChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.RemoteOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.DiagramScheduler;
import org.scenarium.filemanager.scenariomanager.LocalScenario;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.timescheduler.Scheduler;
import org.scenarium.timescheduler.SchedulerPropertyChangeListener;
import org.scenarium.timescheduler.SchedulerState;
import org.scenarium.timescheduler.VisuableSchedulable;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Bounds;
import javafx.geometry.Dimension2D;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class DiagramDrawer extends GeometricDrawer implements SchedulerPropertyChangeListener, FlowDiagramChangeListener {
	private static final String STATIC_DIAGRAM_FILTERS = "Static Diagram Info";
	private static final String DYNAMIC_DIAGRAM_FILTERS = "Dynamic Diagram Info";
	private static final String BLOCK = "Block";
	private static final String SHADOW = "Shadow";
	private static final String LINK = "Link";

	private static final String BLOCK_NAME = "Block Name";
	private static final String IO = "I/O";
	private static final String STACK_STATE = "Stack state";
	private static final String CPUUSAGE = "CPU usage";
	private static final String HEAT_LINK = "Heat link";
	private static final String MISSED_DATA = "Missed Data";
	private static final String COUNT = "Count";
	private static final String IO_NAME = "I/O Name";

	private boolean filterBlock;
	private boolean filterShadow;
	private boolean filterLink;
	private boolean filterIO;

	private boolean filterStackState;
	private boolean filterCPUUsage;
	private boolean filterHeatLink;
	private boolean filterMissedData;
	private boolean filterCount;
	private boolean filterIOName;
	private boolean filterBlockName;
	
	private Color diagramBackground = new Color(0.450980392, 0.450980392, 0.450980392, 1);
	private Color grid = new Color(0.411764706, 0.411764706, 0.411764706, 1);
	@NumberInfo(min = 1)
	private float gridLenght = 30;
	private Color component = new Color(0.588235294, 0.588235294, 0.588235294, 1);
	private Color componentText = Color.WHITE;
	@NumberInfo(min = 0)
	private int componentCornerRadius = 12;
	@NumberInfo(min = 0)
	private float headerSize = 10;
	private Color componentHeaderLocal = new Color(0.5, 0.5, 0.5, 1);
	private Color componentHeaderIsolated = new Color(0.4, 0.4, 0.6, 1);
	private Color componentHeaderRemote = new Color(0.6, 0.6, 0.4, 1);
	private Color componentBorder = new Color(0.341176471, 0.341176471, 0.341176471, 1);
	private Color componentPropertyIO = new Color(0.39215687, 0.58431375, 0.92941177, 1);
	private Color componentStaticIO = new Color(0.784313725, 0.784313725, 0.156862745, 1);
	private Color componentDynamicIO = new Color(0.784313725, 0.156862745, 0.156862745, 1);
	private Color componentVarArgsIO = new Color(0.156862745, 0.784313725, 0.156862745, 1);
	private Color shadow = Color.GRAY.darker();// new Color(0.392156863, 0.392156863, 0.392156863, 0.68627451);
	private float shadowRadius = 15;
	private Color countForeground = new Color(0, 0, 0, 1);
	private Color cpuBarBackground = new Color(0.5, 0.5, 0.5, 0.5);
	private Color cpuBarForeground = new Color(0, 0.5, 0, 0.5);
	private int cpuBarHeight = 10;
	private int cpuBarVerticalGap = 10;
	private int ioFlowDiagramSize = 8;
	@NumberInfo(min = 1)
	private int ioRoundRadius = 5;
	private Color ioTextColor = Color.BLACK;
	private Color link = Color.WHITE;
	private float linkSelectionDistance = 8;
	private Color selectionColor = Color.LIME;
	@PropertyInfo(info = "Refresh rate for the dynamic diagram information")
	@NumberInfo(min = Double.MIN_VALUE)
	private double dynamicDiagramInfoRefreshRate = 10;
	
	private double dynamicDiagramInfoRefreshPeriod;
	private final HashMap<Object, Stage> visibleProperties = new HashMap<>();
	private FlowDiagram oldFd = null;
	private Group drawingElementGroup;
	private double oldScale;
	private HashMap<Block, DrawableBlock> drawingBlocks;
	private LinkedHashMap<LinkDescriptor, CubicCurve> drawingLinks;
	private HashMap<FlowDiagramInput, DrawableInput> drawingInput;
	private HashMap<FlowDiagramOutput, DrawableOutput> drawingOutput;
	private Link tempLink;
	private Rectangle selectAreaRectNode;
	private Rectangle drawingRoi;
	private CanvasInfo canvasInfo;
	private final HashSet<Object> selectedElements = new HashSet<>();
	private final HashSet<Object> areaSelectedElements = new HashSet<>();
	private Object selection;
	private Point2D anchorPoint;
	private final HashMap<Object, Point2D> selectedElementsPosition = new HashMap<>();
	private ContextMenu contextMenu;
	private AnimationTimerConsumer animationTimerConsumer;
	private ArrayList<Node> notChangeableAtRuntimeNodes;
	private boolean isRunning;
	private boolean dragging;
	// Properties link to diagram stop
	private Alert alert;
	private AnimationTimer at;
	private ArrayList<Block> oldAlivedBlocks;
	private boolean interruptedMode = false;
	private AnimationTimer stoppingAnimationTimer; // Used to see waitingdata while stopping a diagram

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		setOnKeyPressed(e -> {
			KeyCode code = e.getCode();
			switch (code) {
			case DELETE:
				if (this.selectedElements.size() != 0) {
					ArrayList<Object> removeList = new ArrayList<>(this.selectedElements);
					for (int i = 0; i < removeList.size(); i++) {
						Object ei = removeList.get(i);
						if (ei instanceof DrawableDiagramIO)
							removeList.set(i, ((DrawableDiagramIO<?>) ei).io);
						else if (ei instanceof LinkDescriptor)
							removeList.set(i, ((LinkDescriptor) ei).link);
					}
					for (Object element : ((FlowDiagram) getDrawableElement()).removeElements(removeList, isRunning(), e.isShiftDown()))
						if (element instanceof DrawableDiagramIO)
							this.selectedElements.removeIf(ele -> ele instanceof DrawableDiagramIO && ((DrawableDiagramIO<?>) ele).io == element);
						else if (element instanceof Link)
							this.selectedElements.removeIf(ele -> ele instanceof LinkDescriptor && ((LinkDescriptor) ele).link == element);
						else
							this.selectedElements.remove(element);
					clearSelectedElements();
				}
				break;
			case UP:
			case DOWN:
			case RIGHT:
			case LEFT:
				if (!e.isControlDown())
					for (Object selectedElement : this.selectedElements)
						if (selectedElement instanceof LocalisedObject) {
							LocalisedObject element = (LocalisedObject) selectedElement;
							double invScale = 1 / getScale();
							Point2D position = element.getPosition();
							double newX = position.getX() + (code == KeyCode.RIGHT ? invScale : code == KeyCode.LEFT ? -invScale : 0);
							double newY = position.getY() + (code == KeyCode.DOWN ? invScale : code == KeyCode.UP ? -invScale : 0);
							if (!iscollision(element, newX, newY)) {
								element.setPosition(newX, newY);
								this.drawingBlocks.get(selectedElement).updatePosition();
							}
						}
				break;
			case D:
				if (this.selectedElements.size() != 0) {
					boolean isOneBlockEnable = false;
					for (Object selectedElement : this.selectedElements)
						if (selectedElement instanceof Block)
							if (((Block) selectedElement).isEnable()) {
								isOneBlockEnable = true;
								break;
							}
					for (Object selectedElement : this.selectedElements)
						if (selectedElement instanceof Block)
							((Block) selectedElement).setEnable(!isOneBlockEnable);
					e.consume();
				}
				break;
			case I:
			case R:
				if (this.selectedElements.size() != 0) {
					for (Object selectedElement : this.selectedElements)
						if (selectedElement instanceof Block) {
							Block block = (Block) selectedElement;
							ProcessMode mode = code == KeyCode.I ? ProcessMode.ISOLATED : ProcessMode.REMOTE;
							block.setProcessMode(block.getProcessMode() == mode ? ProcessMode.LOCAL : mode);
						}
					e.consume();
				}
				break;
			case P:
				if (this.selectedElements.size() == 1) {
					Object selectedElement = this.selectedElements.iterator().next();
					if (selectedElement instanceof Block) {
						showBlockProperties((Block) selectedElement);
						e.consume();
					}
				}
			default:
				break;
			}
		});
		addEventHandler(DragEvent.DRAG_OVER, e -> {
			if (e.isConsumed())
				return;
			Dragboard db = e.getDragboard();
			if (db.hasContent(Operators.OPERATOR_FORMAT) || db.hasContent(Operators.OPERATOR_CLASS_FORMAT)) {
				Point2D pos = toGeometricCoordinate(e.getX(), e.getY());
				if (!iscollision(null, Block.getBlockMinimumBounds(pos, true), pos.getX(), pos.getY()))
					e.acceptTransferModes(TransferMode.MOVE);
				e.consume();
			}
		});
		addEventHandler(DragEvent.DRAG_DROPPED, e -> {
			// System.out.println("dragg ok");
			Dragboard db = e.getDragboard();
			if (db.hasContent(Operators.OPERATOR_FORMAT) || db.hasContent(Operators.OPERATOR_CLASS_FORMAT)) {
				Point2D pos = toGeometricCoordinate(e.getX(), e.getY());
				if (!iscollision(null, Block.getBlockMinimumBounds(pos, true), pos.getX(), pos.getY())) {
					e.acceptTransferModes(TransferMode.MOVE);
					FlowDiagram fd = (FlowDiagram) getDrawableElement();
					BeanDesc<?> beanDesc = null;
					if (db.hasContent(Operators.OPERATOR_CLASS_FORMAT))
						try {
							beanDesc = BeanEditor.registerCreatedBeanAndCreateSubBean(
									BeanManager.getClassFromDescriptor((String) e.getDragboard().getContent(Operators.OPERATOR_CLASS_FORMAT)).getConstructor().newInstance(), BeanManager.defaultDir);
						} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException
								| ClassNotFoundException ex) {
							ex.printStackTrace();
						}
					else
						try {
							beanDesc = BeanEditor.getBeanDescWithAlias((String) e.getDragboard().getContent(Operators.OPERATOR_FORMAT));
						} catch (ClassNotFoundException ex) {
							ex.printStackTrace();
						}
					if (beanDesc == null)
						return;
					int index;
					if ((index = fd.contains(beanDesc.bean)) != -1) {
						showMessage("The block: " + beanDesc + " is already in the diagram", true);
						clearSelectedElements();
						this.selectedElements.add(this.drawingBlocks.get(fd.getBlocks().get(index)));
					} else
						try {
							fd.addBlock(new Block(beanDesc.bean, pos, true));
						} catch (IllegalInputArgument ex) {
							System.err.println("Cannot place the Block: " + ex.getMessage());
						}
					clearSelectedElements();
				}
				e.setDropCompleted(true);
				e.consume();
			}
		});
		addEventFilter(MouseEvent.MOUSE_DRAGGED, e -> {
			this.dragging = true;
			if (this.contextMenu != null) {
				this.contextMenu.hide();
				this.contextMenu = null;
			}
			if (e.isConsumed())
				return;
			for (Object selectedElement : this.selectedElements) {
				Point2D selectedElementPosition = this.selectedElementsPosition.get(selectedElement);
				if (selectedElement != null && selectedElement instanceof LocalisedObject && this.anchorPoint != null) {
					LocalisedObject element = (LocalisedObject) selectedElement;
					double newX = (e.getX() - getxTranslate()) / getScale() - this.anchorPoint.getX() + selectedElementPosition.getX();
					double newY = (e.getY() - getyTranslate()) / getScale() - this.anchorPoint.getY() + selectedElementPosition.getY();
					if (!iscollision(element, newX, newY))
						updatePosition(newX, newY, element);
					else {
						Point2D oldPos = element.getPosition();
						double oldX = oldPos.getX();
						double oldy = oldPos.getY();
						if (!iscollision(element, oldX, newY))
							updatePosition(oldX, newY, element);
						else if (!iscollision(element, newX, oldy))
							updatePosition(newX, oldy, element);
					}
				}
			}
		});
		addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
			this.selectedElementsPosition.clear();
			this.anchorPoint = null;
			if (this.getSelectAreaRect() != null) {
				this.setSelectAreaRect(null);
				repaint(false);
			}
			this.areaSelectedElements.clear();
		});
		addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
			this.dragging = false;
			if (this.contextMenu != null)
				this.contextMenu.hide();
			if (this.selection != null) {
				if (!this.selectedElements.contains(this.selection)) {
					if (!e.isControlDown())
						clearSelectedElements();
					this.selectedElements.add(this.selection);
				}
				this.selectedElementsPosition.clear();
				this.anchorPoint = toGeometricCoordinate(e.getX(), e.getY());// new Point2D((e.getX() - getxTranslate()) / getScale(), (e.getY() - getyTranslate()) / getScale());
				for (Object obj : this.selectedElements)
					if (obj instanceof LocalisedObject)
						this.selectedElementsPosition.put(obj, ((LocalisedObject) obj).getPosition());
				this.selection = null;
				e.consume();
				return;
			}
			if (e.isConsumed())
				return;
			if (e.getButton() == MouseButton.PRIMARY) {
				if (!e.isControlDown()) // Que si MouseButton.PRIMARY, sinon perte des blocks et autres quand déplace
					clearSelectedElements();
				Point2D mousePoint = new Point2D(e.getX() - getxTranslate(), e.getY() - getyTranslate());
				LinkDescriptor closestLink = null;
				double minDist = Double.MAX_VALUE;
				for (LinkDescriptor link : this.drawingLinks.keySet()) {
					CubicCurve curve = this.drawingLinks.get(link);
					Bounds cb = curve.getBoundsInLocal();
					float lsd = this.linkSelectionDistance;
					if (new Rectangle2D(cb.getMinX() - lsd, cb.getMinY() - lsd, cb.getWidth() + lsd * 2, cb.getHeight() + lsd * 2).contains(mousePoint)) {
						double dist = distance(curve, mousePoint);
						if (dist < minDist) {
							minDist = dist;
							closestLink = link;
						}
					}
				}
				if (minDist < this.linkSelectionDistance * this.linkSelectionDistance) {
					this.selectedElements.add(closestLink);
					this.drawingLinks.get(closestLink).setStroke(this.selectionColor);
					e.consume();
				}
			}
		});
		setOnContextMenuRequested(e -> {
			if (!this.dragging && this.tempLink == null)
				showDiagramContextMenu(e);
			e.consume();
		});
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
		if (scheduler != null) {
			scheduler.addPropertyChangeListener(this);
			this.isRunning = scheduler.isRunning();
		} else
			this.isRunning = false;
		widthProperty().addListener(e -> setClip(new Rectangle(getWidth(), getHeight())));
		heightProperty().addListener(e -> setClip(new Rectangle(getWidth(), getHeight())));
		setBackground(new Background(new BackgroundFill(this.diagramBackground, null, null)));
	}

	@Override
	public void death() {
		FlowDiagram de = (FlowDiagram) getDrawableElement();
		if (de != null)
			de.removeFlowDiagramChangeListener(this);
		if (this.getScheduler() != null)
			this.getScheduler().removePropertyChangeListener(this);
		for (Stage stage : this.visibleProperties.values())
			stage.close();
		this.visibleProperties.clear();
		super.death();
	}

	@Override
	public void setDrawableElement(Object drawableElement) {
		for (Stage stage : this.visibleProperties.values())
			stage.close();
		this.visibleProperties.clear();
		FlowDiagram oldDrawableElement = (FlowDiagram) getDrawableElement();
		if (oldDrawableElement != null)
			oldDrawableElement.removeFlowDiagramChangeListener(this);
		super.setDrawableElement(drawableElement);
		((FlowDiagram) getDrawableElement()).setManagingCpuUsage(this.filterCPUUsage);
		((FlowDiagram) getDrawableElement()).addFlowDiagramChangeListener(this);
		((FlowDiagram) getDrawableElement()).setStopTimeOutTask((waitingBlocks, interruptProcess) -> {
			this.interruptedMode = false;
			Platform.runLater(() -> {
				ButtonType interruptButton = new ButtonType(interruptProcess != null ? "Interrupt them" : "Ok");
				this.alert = new Alert(AlertType.WARNING, null, interruptButton);
				updateStopAlert(waitingBlocks, true);
				this.alert.setHeaderText("Blocks below seems blocked");
				this.alert.resultProperty().addListener(e -> {
					if (this.alert.getResult() == interruptButton) {
						interruptProcess.run();
						this.alert = null;
						this.interruptedMode = true;
					}
				});
				this.alert.show();
				this.at = new AnimationTimer() {
					private long oltTime;
					private boolean phase = true;
					private int timeEllapseInInterruptedMode = 0;
	
					@Override
					public void handle(long time) {
						if (time - this.oltTime > 500000000) {
							this.phase = !this.phase;
							updateStopAlert(waitingBlocks, this.phase);
							// canvasInfo.paint(); //TODO HERE redéclencher dans un AnimationTimer le canvasInfo
							this.oltTime = time;
							if (DiagramDrawer.this.interruptedMode) {
								this.timeEllapseInInterruptedMode++;
								if (this.timeEllapseInInterruptedMode == 4) {
									ButtonType restartButton = new ButtonType("Quit");
									ButtonType saveButton = new ButtonType("Save and Quit");
									DiagramDrawer.this.alert = new Alert(AlertType.ERROR, null, saveButton, restartButton);
									DiagramDrawer.this.alert.setHeaderText("Blocks below seems blocked while interrupted\nThis prevent Scenarium from continuing");
									DiagramDrawer.this.alert.resultProperty().addListener(e -> {
										if (DiagramDrawer.this.alert.getResult() == saveButton)
											saveScenario();
										if (DiagramDrawer.this.alert.getResult() == saveButton || DiagramDrawer.this.alert.getResult() == restartButton)
											System.exit(-1);
									});
									updateStopAlert(waitingBlocks, this.phase);
									DiagramDrawer.this.alert.show();
								}
							}
						}
					}
				};
				this.at.start();
			});
			synchronized (this) {
				try {
					wait();
				} catch (InterruptedException ex) {
					if (this.at != null) {
						this.at.stop();
						this.at = null;
					}
					Platform.runLater(() -> {
						if (this.alert != null) {
							this.alert.getButtonTypes().add(new ButtonType("", ButtonData.CANCEL_CLOSE));
							this.alert.close();
							this.alert = null;
						}
						updateStopAlert(waitingBlocks, false);
						this.oldAlivedBlocks = null;
					});
				}
			}
		});
	}

	// Attention!!! pas de FxUtils.runLaterIfNeeded, cela peut changer l'ordre des tache à exécuter...
		@Override
		public void flowDiagramChanged(Object element, String property, ModificationType modificationType) {
	//		 System.out.println("Element: " + element + " ModificationType: " + modificationType + " From fx thread: " + Platform.isFxApplicationThread());
			if (element instanceof Link) { // removeLink OK createDrawingLink input output
				LinkDescriptor linkDescriptor = new LinkDescriptor((Link) element);
				Platform.runLater(() -> {
					int index = (this.isFilterGrid() ? 1 : 0) + (this.isFilterBorder() ? 1 : 0);
					if (modificationType == ModificationType.NEW)
						this.drawingElementGroup.getChildren().add(index, createDrawingLink(linkDescriptor));
					else if (modificationType == ModificationType.DELETE)
						removeLink(linkDescriptor);
					else if (modificationType == ModificationType.CHANGE) {
						LinkDescriptor linkToRemove = null;
						for (LinkDescriptor link : this.drawingLinks.keySet())
							if (link.input.io == linkDescriptor.input.io) {
								linkToRemove = link;
								break;
							}
						if (linkToRemove != null)
							removeLink(linkToRemove);
						this.drawingElementGroup.getChildren().add(index, createDrawingLink(linkDescriptor));
					}
				});
			}
			if (element instanceof Block) {
				if (modificationType == ModificationType.CHANGE && property == "name") {
					String name = ((Block) element).getName();
					Platform.runLater(() -> this.drawingBlocks.get(element).updateBlockName(name));
				} else {
					IoDescriptor[] ioDescriptors = modificationType == ModificationType.DELETE ? null : getIODescriptors((Block) element);
					Platform.runLater(() -> {
						Block block = (Block) element;
						if (modificationType == ModificationType.NEW)
							this.drawingElementGroup.getChildren().add(new DrawableBlock(block, getScale(), ioDescriptors));
						else if (modificationType == ModificationType.DELETE) {
							this.drawingElementGroup.getChildren().remove(this.drawingBlocks.get(block));
							this.drawingBlocks.remove(block).delete();
							deleteElementFromSelectionAndPropertiesStage(block);
						} else if (modificationType == ModificationType.CHANGE)
							this.drawingBlocks.get(block).updateIOStruct(ioDescriptors);
						if (this.canvasInfo != null)
							this.canvasInfo.repaint();
					});
				}
			} else if (element instanceof FlowDiagramInput) {
				IoDescriptor[] ioDescriptors = modificationType == ModificationType.NEW ? getIODescriptors((FlowDiagramInput) element) : null;
				Platform.runLater(() -> {
					if (modificationType == ModificationType.NEW)
						this.drawingElementGroup.getChildren().add(new DrawableInput((FlowDiagramInput) element, getScale(), ioDescriptors));
					else if (modificationType == ModificationType.DELETE) {
						this.drawingElementGroup.getChildren().remove(this.drawingInput.get(element));
						this.drawingInput.remove(element);
						deleteElementFromSelectionAndPropertiesStage(element);
					} else if (modificationType == ModificationType.CHANGE)
						((DrawableInput) this.drawingElementGroup.getChildren().get(this.drawingElementGroup.getChildren().indexOf(this.drawingOutput.get(element))))
								.nameChange((org.scenarium.filemanager.scenario.dataflowdiagram.IO) element);
					if (this.canvasInfo != null)
						this.canvasInfo.repaint();
				});
			} else if (element instanceof FlowDiagramOutput) {
				IoDescriptor[] ioDescriptors = modificationType == ModificationType.NEW ? getIODescriptors((FlowDiagramOutput) element) : null;
				Platform.runLater(() -> {
					if (modificationType == ModificationType.NEW)
						this.drawingElementGroup.getChildren().add(new DrawableOutput((FlowDiagramOutput) element, getScale(), ioDescriptors));
					else if (modificationType == ModificationType.DELETE) {
						this.drawingElementGroup.getChildren().remove(this.drawingOutput.get(element));
						this.drawingOutput.remove(element);
						deleteElementFromSelectionAndPropertiesStage(element);
					} else if (modificationType == ModificationType.CHANGE)
						((DrawableOutput) this.drawingElementGroup.getChildren().get(this.drawingElementGroup.getChildren().indexOf(this.drawingOutput.get(element))))
								.nameChange((org.scenarium.filemanager.scenario.dataflowdiagram.IO) element);
					if (this.canvasInfo != null)
						this.canvasInfo.repaint();
				});
			}
		}

	@Override
	public void stateChanged(SchedulerState state) {
		FxUtils.runLaterIfNeeded(() -> { // OK
			if (state == SchedulerState.STARTED) {
				this.isRunning = true;
				if (this.notChangeableAtRuntimeNodes != null)
					this.notChangeableAtRuntimeNodes.forEach(node -> node.setDisable(this.isRunning));
				if (this.animationTimerConsumer == null)
					return;
				if ((this.filterStackState || this.filterCPUUsage || this.filterHeatLink || this.filterCount || this.filterMissedData) && this.canvasInfo == null) {
					this.canvasInfo = new CanvasInfo();
					this.canvasInfo.setManaged(false);
					this.canvasInfo.widthProperty().bind(widthProperty());
					this.canvasInfo.heightProperty().bind(heightProperty());
					this.canvasInfo.setMouseTransparent(true);
					this.canvasInfo.setFocusTraversable(false);
					getChildren().add(this.canvasInfo);
					if (this.animationTimerConsumer != null)
						this.animationTimerConsumer.register(this.canvasInfo);
					this.canvasInfo.repaint();
				}
			} else if (state == SchedulerState.PRESTOP) {
				if (this.stoppingAnimationTimer != null)
					this.stoppingAnimationTimer.stop();
				this.stoppingAnimationTimer = new AnimationTimer() {
	
					@Override
					public void handle(long now) {
						CanvasInfo canvasInfo = DiagramDrawer.this.canvasInfo;
						if (canvasInfo != null)
							canvasInfo.paint();
	
					}
				};
				this.stoppingAnimationTimer.start();
			} else if (state == SchedulerState.STOPPED) {
				this.isRunning = false;
				if (this.notChangeableAtRuntimeNodes != null)
					this.notChangeableAtRuntimeNodes.forEach(node -> node.setDisable(this.isRunning));
				if (this.canvasInfo != null) {
					if (this.animationTimerConsumer != null)
						this.animationTimerConsumer.unRegister(this.canvasInfo);
					getChildren().remove(this.canvasInfo);
					// ((FlowDiagram) getDrawableElement()).removeDynamicFlowDiagramDepictionChangeListener(this);
					this.canvasInfo = null;
					resetDrawingLinksColor();
				}
				if (this.stoppingAnimationTimer != null) {
					this.stoppingAnimationTimer.stop();
					this.stoppingAnimationTimer = null;
				}
			}
		});
	}

	@Override
	public Dimension2D getDimension() {
		double minX = 800;
		double minY = 600;
		FlowDiagram fd = (FlowDiagram) getDrawableElement();
		for (Block block : fd.getBlocks()) {
			Rectangle2D rect = block.getRectangle();
			if (rect.getMaxX() + 10 > minX)
				minX = rect.getMaxX() + 10;
			if (rect.getMaxY() + 10 > minY)
				minY = rect.getMaxY() + 10;
		}
		for (FlowDiagramInput fdInput : fd.getInputs()) {
			Point2D inputPos = fdInput.getPosition();
			if (inputPos.getX() + 20 + 1 + this.ioRoundRadius > minX)
				minX = inputPos.getX() + 20 + 1 + this.ioRoundRadius;
			if (inputPos.getY() + 10 > minY)
				minY = inputPos.getY() + 10;
		}
		for (FlowDiagramOutput fdOutput : fd.getOutputs()) {
			Point2D inputPos = fdOutput.getPosition();
			if (inputPos.getX() + 20 + 1 + this.ioRoundRadius > minX)
				minX = inputPos.getX() + 20 + 1 + this.ioRoundRadius;
			if (inputPos.getY() + 10 > minY)
				minY = inputPos.getY() + 10;
		}
		return new Dimension2D(minX, minY);
	}

	@Override
	public float getIntelligentZoomThreshold() {
		return 10;
	}

	@Override
	public float getZoomScale() {
		return 2;
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		String filterName = filterPath[filterPath.length - 1];
		if (filterName.equals(BLOCK)) {
			this.filterBlock = value;
			if (this.drawingBlocks != null)
				for (DrawableBlock db : this.drawingBlocks.values())
					db.setVisible(value);
		} else if (filterName.equals(LINK)) {
			this.filterLink = value;
			if (this.drawingLinks != null)
				for (CubicCurve cc : this.drawingLinks.values())
					cc.setVisible(value);
		} else if (filterName.equals(IO)) {
			this.filterIO = value;
			if (this.drawingBlocks != null)
				for (DrawableBlock db : this.drawingBlocks.values())
					for (DrawableIOComponent<? extends Shape>.DrawingIo di : db.drawingIOs.values())
						di.circle.setVisible(value);
		} else if (filterName.equals(STACK_STATE)) {
			this.filterStackState = value;
			updatePaneInfo();
		} else if (filterName.equals(CPUUSAGE)) {
			this.filterCPUUsage = value;
			updatePaneInfo();
			if (getDrawableElement() != null)
				((FlowDiagram) getDrawableElement()).setManagingCpuUsage(this.filterCPUUsage);
		} else if (filterName.equals(HEAT_LINK)) {
			this.filterHeatLink = value;
			if (!this.filterHeatLink)
				resetDrawingLinksColor();
			updatePaneInfo();
		} else if (filterName.equals(MISSED_DATA)) {
			this.filterMissedData = value;
			updatePaneInfo();
		} else if (filterName.equals(COUNT)) {
			this.filterCount = value;
			updatePaneInfo();
		} else if (filterName.equals(IO_NAME)) {
			this.filterIOName = value;
			if (this.drawingBlocks != null)
				for (DrawableBlock db : this.drawingBlocks.values())
					for (DrawableIOComponent<? extends Shape>.DrawingIo di : db.drawingIOs.values())
						di.text.setVisible(value);
		} else if (filterName.equals(BLOCK_NAME)) {
			this.filterBlockName = value;
			if (this.drawingBlocks != null)
				for (DrawableBlock db : this.drawingBlocks.values())
					db.blockName.setVisible(value);
		} else if (filterName.equals(SHADOW)) {
			this.filterShadow = value;
			if (this.drawingBlocks != null)
				for (DrawableBlock db : this.drawingBlocks.values())
					db.core.setEffect(value ? new DropShadow(this.shadowRadius, this.shadow) : null);
		} else {
			if (filterName.equals(GRID) && this.drawingElementGroup != null)
				if (value)
					this.drawingElementGroup.getChildren().add(0, creatGrid());
				else
					this.drawingElementGroup.getChildren().removeIf(e -> e.getId() == GRID);
			if (filterName.equals(BORDER) && this.drawingElementGroup != null)
				if (value)
					this.drawingElementGroup.getChildren().add(0, createDrawingborder());
				else
					this.drawingElementGroup.getChildren().removeIf(e -> e.getId() == BORDER);
			return super.updateFilterWithPath(filterPath, value);
		}
		repaint(false);
		return true;
	}

	@Override
	public void setAnimationTimerConsumer(AnimationTimerConsumer animationTimerConsumer) {
		stateChanged(SchedulerState.STOPPED);
		this.animationTimerConsumer = animationTimerConsumer;
		stateChanged(SchedulerState.STARTED);
	}

	@Override
	protected void paint(Object dataElement) {
		FlowDiagram fd = (FlowDiagram) dataElement;
		double scale = getScale();
		if (scale == 0)
			return;
		if (this.oldFd == null || this.oldFd != fd) {
			clearSelectedElements();
			if (this.drawingElementGroup != null)
				for (Node node : this.drawingElementGroup.getChildren())
					if (node instanceof DrawableBlock)
						((DrawableBlock) node).delete();
			getChildren().remove(this.drawingElementGroup);
			this.drawingRoi = null;
			this.drawingBlocks = new HashMap<>();
			ArrayList<Node> drawingElements = new ArrayList<>();
			if (this.isFilterGrid())
				drawingElements.add(creatGrid());
			if (this.isFilterBorder())
				drawingElements.add(createDrawingborder());
			int index = drawingElements.size();
			for (Block block : fd.getBlocks())
				drawingElements.add(new DrawableBlock(block, getScale(), getIODescriptors(block)));
			this.drawingInput = new HashMap<>();
			fd.getInputs().forEach(fdi -> drawingElements.add(index, new DrawableInput(fdi, getScale(), getIODescriptors(fdi))));
			this.drawingOutput = new HashMap<>();
			fd.getOutputs().forEach(fdo -> drawingElements.add(index, new DrawableOutput(fdo, getScale(), getIODescriptors(fdo))));
			this.drawingLinks = new LinkedHashMap<>();
			for (Link link : fd.getAllLinks())
				drawingElements.add(index, createDrawingLink(new LinkDescriptor(link)));
			this.drawingElementGroup = new Group(drawingElements);
			getChildren().add(this.drawingElementGroup);
		}
		this.drawingElementGroup.setTranslateX((int) getxTranslate());
		this.drawingElementGroup.setTranslateY((int) getyTranslate());
		if (getScale() != this.oldScale)
			if (this.drawingElementGroup != null) {
				ArrayList<Runnable> taskAfterScaleChanged = null;
				for (Node node : this.drawingElementGroup.getChildren())
					if (node instanceof DrawableIOComponent<?>) {
						Runnable runTaskAfterScaleChanged = ((DrawableIOComponent<?>) node).setScale(getScale());
						if (runTaskAfterScaleChanged != null) {
							if (taskAfterScaleChanged == null)
								taskAfterScaleChanged = new ArrayList<>();
							taskAfterScaleChanged.add(runTaskAfterScaleChanged);
						}
					} else if (node.getId() == GRID)
						setGridScale((Group) node, scale);
					else if (node.getId() == BORDER)
						setBorderScale((Rectangle) node, scale);
				if (taskAfterScaleChanged != null)
					taskAfterScaleChanged.forEach(Runnable::run);
			}
		if (this.isFilterROI()) {
			if (this.drawingRoi == null) {
				this.drawingRoi = new Rectangle();
				this.drawingRoi.setFill(null);
				this.drawingRoi.setStroke(Color.RED);
				this.drawingRoi.setViewOrder(Integer.MIN_VALUE);
				this.drawingElementGroup.getChildren().add(this.drawingRoi);
			}
			float[] roi = getRoi();
			this.drawingRoi.setX(roi[0] * scale + 0.5);
			this.drawingRoi.setY(roi[1] * scale + 0.5);
			this.drawingRoi.setWidth(roi[2] * scale + 0.5);
			this.drawingRoi.setHeight(roi[3] * scale + 0.5);
		} else if (this.drawingRoi != null) {
			this.drawingElementGroup.getChildren().remove(this.drawingRoi);
			this.drawingRoi = null;
		}
		Point2i[] selectAreaRect = getSelectAreaRect();
		if (selectAreaRect == null ^ this.selectAreaRectNode == null) { // ou exclusif changement dans la selection
			if (selectAreaRect == null) {
				this.drawingElementGroup.getChildren().remove(this.selectAreaRectNode);
				this.selectAreaRectNode = null;
			} else if (this.selectAreaRectNode == null) {
				this.selectAreaRectNode = new Rectangle(selectAreaRect[0].x * scale + 0.5, selectAreaRect[0].y * scale + 0.5, selectAreaRect[1].x * scale + 0.5,
						selectAreaRect[1].y * scale + 0.5);
				this.selectAreaRectNode.setStroke(this.selectionColor);
				this.selectAreaRectNode.setFill(null);
				this.drawingElementGroup.getChildren().add(this.selectAreaRectNode);
			}
		} else if (this.getSelectAreaRect() != null && this.selectAreaRectNode != null) {
			this.selectAreaRectNode.setX(selectAreaRect[0].x * scale + 0.5);
			this.selectAreaRectNode.setY(selectAreaRect[0].y * scale + 0.5);
			this.selectAreaRectNode.setWidth(selectAreaRect[1].x * scale + 0.5);
			this.selectAreaRectNode.setHeight(selectAreaRect[1].y * scale + 0.5);
		}
		if (this.canvasInfo != null)
			this.canvasInfo.repaint();
		this.oldFd = fd;
		this.oldScale = scale;
	}

	@Override
	protected void populateTheaterFilter(TreeRoot<BooleanProperty> theaterFilter) {
		super.populateTheaterFilter(theaterFilter);
		TreeNode<BooleanProperty> filtersMap = new TreeNode<>(new BooleanProperty(STATIC_DIAGRAM_FILTERS, true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BLOCK, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(SHADOW, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(LINK, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BLOCK_NAME, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(IO, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(IO_NAME, true)));
		theaterFilter.addChild(filtersMap);
		filtersMap = new TreeNode<>(new BooleanProperty(DYNAMIC_DIAGRAM_FILTERS, true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(STACK_STATE, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(CPUUSAGE, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(HEAT_LINK, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(COUNT, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MISSED_DATA, false)));
		theaterFilter.addChild(filtersMap);
	}

	@Override
	protected void selectAreaChanged(boolean isCtrlDown) {
		FlowDiagram fd = (FlowDiagram) getDrawableElement();
		// clearSelectedElements();
		Point2i[] selectAreaRect = getSelectAreaRect();
		Rectangle areaRect = new Rectangle(selectAreaRect[0].x, selectAreaRect[0].y, selectAreaRect[1].x, selectAreaRect[1].y);
		for (Iterator<Object> iterator = this.areaSelectedElements.iterator(); iterator.hasNext();) {
			Object areaElement = iterator.next();
			if (areaElement instanceof Block) {
				Block block = (Block) areaElement;
				if (!areaRect.contains(block.getPosition())) {
					iterator.remove();
					this.drawingBlocks.get(block).resetCoreStroke();
					this.selectedElements.remove(areaElement);
				}
			} else if (areaElement instanceof DrawableIOComponent<?>) {
				DrawableDiagramIO<?> fdi = (DrawableDiagramIO<?>) areaElement;
				if (!areaRect.contains(fdi.getPosition())) {
					iterator.remove();
					fdi.resetCoreStroke();
					this.selectedElements.remove(areaElement);
				}
			}
		}
		for (Block block : fd.getBlocks())
			if (areaRect.contains(block.getPosition()) && !this.selectedElements.contains(block)) {
				this.selectedElements.add(block);
				this.areaSelectedElements.add(block);
				this.drawingBlocks.get(block).core.setStroke(this.selectionColor);
			}
		for (FlowDiagramInput fdInput : fd.getInputs())
			if (areaRect.contains(fdInput.getPosition()) && !this.selectedElements.contains(fdInput)) {
				DrawableInput element = this.drawingInput.get(fdInput);
				this.selectedElements.add(element);
				this.areaSelectedElements.add(element);
				element.core.setStroke(this.selectionColor);
			}
		for (FlowDiagramOutput fdOutput : fd.getOutputs())
			if (areaRect.contains(fdOutput.getPosition()) && !this.selectedElements.contains(fdOutput)) {
				DrawableOutput element = this.drawingOutput.get(fdOutput);
				this.selectedElements.add(element);
				this.areaSelectedElements.add(element);
				element.core.setStroke(this.selectionColor);
			}
	}

	@Override
	protected void adaptViewToScenario() {
		Dimension2D dim = getDimension();
		setScenarioSize((int) dim.getWidth(), (int) dim.getHeight());
		super.adaptViewToScenario();
	}

	@Override
	protected double adjustScale(double scale) {
		return scale;
	}

	@Override
	protected boolean isAreaSelectionPoint(int x, int y) {
		return true;
	}

	private void clearSelectedElements() {
		for (Object element : this.selectedElements)
			if (element instanceof Block)
				this.drawingBlocks.get(element).resetCoreStroke();
			else if (element instanceof DrawableIOComponent<?>)
				((DrawableIOComponent<?>) element).resetCoreStroke();
			else if (element instanceof BlockIO) {
				BlockIO blockIO = (BlockIO) element;
				this.drawingBlocks.get(blockIO.getBlock()).resetStroke((IO) blockIO);
			} else if (element instanceof FlowDiagramInput) {
				FlowDiagramInput fdInput = (FlowDiagramInput) element;
				this.drawingInput.get(fdInput).resetStroke(fdInput);
			} else if (element instanceof FlowDiagramOutput) {
				FlowDiagramOutput fdOutput = (FlowDiagramOutput) element;
				this.drawingOutput.get(fdOutput).resetStroke(fdOutput);
			} else if (this.canvasInfo == null)
				this.drawingLinks.get(element).setStroke(link);
			else
				this.canvasInfo.paint();
		this.selectedElements.clear();
	}

	private void setGridScale(Group g, double scale) { // TODO bug si trop petite, revoir cette grille
		ObservableList<Node> gridChildren = g.getChildren();
		int ls = (int) Math.round(this.getScenarioWidth() * scale);
		int hs = (int) Math.round(this.getScenarioHeight() * scale);
		int ls1 = ls + 1;
		int hs1 = hs + 1;
		int index = 0;
		for (float i = 0; i < ls1; i += scale * this.gridLenght) {
			if (index == gridChildren.size()) {
				Line line = new Line();
				line.setStroke(this.grid);
				gridChildren.add(line);
			}
			Line line = (Line) gridChildren.get(index++);
			line.setStartX((int) i + 0.5);
			line.setStartY(0 + 0.5);
			line.setEndX((int) i + 0.5);
			line.setEndY(hs + 0.5);
		}
		for (float i = 0; i < hs1; i += scale * this.gridLenght) {
			if (index == gridChildren.size()) {
				Line line = new Line();
				line.setStroke(this.grid);
				gridChildren.add(line);
			}
			Line line = (Line) gridChildren.get(index++);
			line.setStartX(0 + 0.5);
			line.setStartY((int) i + 0.5);
			line.setEndX(ls + 0.5);
			line.setEndY((int) i + 0.5);
		}
		if (index != gridChildren.size())
			gridChildren.remove(index, gridChildren.size());
	}

	private void addAsInputOutputMenuToEditor(Block block, String propertyNameSequence, Object operator, BeanManager beanManager, GridPane editor) {
		Function<String, String> propertyNameFullSequenceFunction = pns -> propertyNameSequence == null ? pns : propertyNameSequence + "-" + pns;
		try {
			PropertyDescriptor[] beanInfo = Introspector.getBeanInfo(operator.getClass()).getPropertyDescriptors();
			Arrays.sort(beanInfo, 0, beanInfo.length, (a, b) -> a.getName().compareTo(b.getName()));
			ArrayList<Label> propertyLabels = new ArrayList<>();
			for (Node node : editor.getChildren())
				if (GridPane.getColumnIndex(node) == 0) {
					Label propertyNameLabel = null;
					if (node instanceof Label)
						propertyNameLabel = (Label) node;
					else if (node instanceof HBox) {
						ObservableList<Node> children = ((HBox) node).getChildren();
						if (children.size() > 1) {
							Node secondChild = children.get(1);
							if (secondChild instanceof Label)
								propertyNameLabel = (Label) secondChild;
						}
					}
					if (propertyNameLabel != null) {
						int indexOfNameEnd = propertyNameLabel.getText().indexOf(":");
						if (indexOfNameEnd != -1)
							propertyLabels.add(propertyNameLabel);
					}
				}
			ArrayList<Node> notChangeableAtRuntimeNodes = new ArrayList<>();
			if (!propertyLabels.isEmpty()) {
				propertyLabels.sort((a, b) -> BeanManager.getPropertyNameFromLabelName(a.getText()).compareTo(BeanManager.getPropertyNameFromLabelName(b.getText())));
				int propertyLabelsIndex = 0;
				for (int j = 0; j < beanInfo.length; j++)
					if (BeanManager.getPropertyNameFromLabelName(propertyLabels.get(propertyLabelsIndex).getText()).equals(beanInfo[j].getName())) {
						if (BeanManager.getAnno(NotChangeableAtRuntime.class, operator.getClass(), beanInfo[j]) != null) {
							Integer ci = GridPane.getRowIndex(propertyLabels.get(propertyLabelsIndex));
							for (Node node : editor.getChildren())
								if (GridPane.getColumnIndex(node) == 1 && GridPane.getRowIndex(node) == ci)
									notChangeableAtRuntimeNodes.add(node);
						}
						if (DiagramScheduler.isCloneable(WrapperTools.toWrapper(beanInfo[j].getPropertyType())) && beanInfo[j].getWriteMethod() != null) {
							String propertyName = beanInfo[j].getName();
							boolean forbidden = false;
							List<BlockInput> inputs = block.getInputs();
							for (int i = block.getNbPropertyAsInput(); i < inputs.size(); i++) // OK pas grave, juste pour menu
								if (inputs.get(i).getName().equals(propertyName)) {
									forbidden = true;
									break;
								}
							if (!forbidden) {
								String propertyNameFullSequence = propertyNameFullSequenceFunction.apply(propertyName);
								CheckMenuItem asInputMenu = new CheckMenuItem(propertyNameFullSequence + " as input");
								CheckMenuItem asOutputMenu = new CheckMenuItem(propertyNameFullSequence + " as output");
								Object blockOperator = block.getOperator();
								asInputMenu.setSelected(block.isPropertyAsInput(propertyNameFullSequence));
								asInputMenu.setOnAction(e1 -> {
									try {
										if (asInputMenu.isSelected()) {
											if (blockOperator instanceof EvolvedOperator)
												((EvolvedOperator) blockOperator).addPropertyAsInput(propertyNameFullSequence);
											else
												block.addPropertyAsInput(propertyNameFullSequence);
										} else if (blockOperator instanceof EvolvedOperator)
											((EvolvedOperator) blockOperator).removePropertyAsInput(propertyNameFullSequence);
										else
											block.removePropertyAsInput(propertyNameFullSequence);
									} catch (IllegalArgumentException e) {
										AlertUtil.show(e, "Cannot " + (asInputMenu.isSelected() ? "add" : "remove") + " property \"" + propertyNameFullSequence + "\" as input of block \""
												+ block.getName() + "\"", false);
										asInputMenu.setSelected(block.isPropertyAsInput(propertyNameFullSequence));
									}
								});
								asOutputMenu.setSelected(block.isPropertyAsOutput(propertyNameFullSequence));
								asOutputMenu.setOnAction(e1 -> {
									try {
										if (asOutputMenu.isSelected()) {
											if (blockOperator instanceof EvolvedOperator)
												((EvolvedOperator) blockOperator).addPropertyAsOutput(propertyNameFullSequence);
											else
												block.addPropertyAsOutput(propertyNameFullSequence);
										} else if (blockOperator instanceof EvolvedOperator)
											((EvolvedOperator) blockOperator).removePropertyAsOutput(propertyNameFullSequence);
										else
											block.removePropertyAsOutput(propertyNameFullSequence);
									} catch (IllegalArgumentException e) {
										AlertUtil.show(e, "Cannot " + (asOutputMenu.isSelected() ? "add" : "remove") + " property \"" + propertyNameFullSequence + "\" as output of block \""
												+ block.getName() + "\"", false);
										asOutputMenu.setSelected(block.isPropertyAsOutput(propertyNameFullSequence));
									}
								});
								propertyLabels.get(propertyLabelsIndex).setContextMenu(new ContextMenu(asInputMenu, asOutputMenu));
							}
						}
						propertyLabelsIndex++;
						if (propertyLabelsIndex == propertyLabels.size())
							break;
					}
			}
			synchronized (DiagramDrawer.this) {
				if (this.notChangeableAtRuntimeNodes == null)
					this.notChangeableAtRuntimeNodes = notChangeableAtRuntimeNodes;
				else
					this.notChangeableAtRuntimeNodes.addAll(notChangeableAtRuntimeNodes);
				notChangeableAtRuntimeNodes.forEach(node -> node.setDisable(this.isRunning)); // ne pas reenable des éléments disable par fire
			}
		} catch (IntrospectionException ex) {
			ex.printStackTrace();
		}
		var listenerHandler = new Object() {
			SubBeanExpansionListener sbel;
		};
		listenerHandler.sbel = (expanded, subBeanName, subBeanManager, subBeanGridpane) -> {
			if (subBeanManager != null)
				if (expanded)
					addAsInputOutputMenuToEditor(block, propertyNameFullSequenceFunction.apply(subBeanName), subBeanManager.getBean(), subBeanManager, subBeanGridpane);
				else {
					subBeanManager.forEachExpendedSubView((name, sbm, sbgp) -> subBeanManager.removeSubBeanExpandedListener(listenerHandler.sbel));
					subBeanManager.removeSubBeanExpandedListener(listenerHandler.sbel);
				}
		};
		beanManager.forEachExpendedSubView(
				(subBeanName, subBeanManager, subBeanGridpane) -> listenerHandler.sbel.subBeanExpanded(true, propertyNameFullSequenceFunction.apply(subBeanName), subBeanManager, subBeanGridpane));
		beanManager.addSubBeanExpandedListener(listenerHandler.sbel);
	}

	private void createDiagramContextMenu() {
		MenuItem miaop = new MenuItem("Add Operator");
		miaop.setOnAction(e -> showTool(Operators.class));
		MenuItem miai = new MenuItem("Add Input");
		miai.setOnAction(e1 -> {
			Point2D localPos = DiagramDrawer.this.screenToLocal(new Point2D(this.contextMenu.getAnchorX(), this.contextMenu.getAnchorY()));
			Point2D pos = toGeometricCoordinate(localPos.getX(), localPos.getY());
			FlowDiagram fd = (FlowDiagram) getDrawableElement();
			List<FlowDiagramOutput> outputs = fd.getOutputs();
			String baseName = "in-";
			String name;
			boolean isValidName;
			int id = 0;
			do {
				isValidName = true;
				name = baseName + id++;
				for (FlowDiagramOutput output : outputs)
					if (output.getName().equals(name)) {
						isValidName = false;
						break;
					}
			} while (!isValidName || id == Integer.MAX_VALUE);
			fd.addOutput(new FlowDiagramOutput(null, name, pos));
		});
		MenuItem miao = new MenuItem("Add Output");
		miao.setOnAction(e1 -> {
			Point2D localPos = DiagramDrawer.this.screenToLocal(new Point2D(this.contextMenu.getAnchorX(), this.contextMenu.getAnchorY()));
			Point2D pos = toGeometricCoordinate(localPos.getX(), localPos.getY());
			FlowDiagram fd = (FlowDiagram) getDrawableElement();
			List<FlowDiagramInput> inputs = fd.getInputs();
			String baseName = "out-";
			String name;
			boolean isValidName;
			int id = 0;
			do {
				isValidName = true;
				name = baseName + id++;
				for (FlowDiagramInput input : inputs)
					if (input.getName().equals(name)) {
						isValidName = false;
						break;
					}
			} while (!isValidName || id == Integer.MAX_VALUE);
			fd.addInput(new FlowDiagramInput(fd, null, name, pos));
		});
		this.contextMenu = new ContextMenu(miaop, miai, miao);
	}

	private Node createDrawingborder() {
		Rectangle drawingBorder = new Rectangle(0.5, 0.5, this.getScenarioWidth() * getScale() + 0.5, this.getScenarioHeight() * getScale() + 0.5);
		drawingBorder.setId(BORDER);
		drawingBorder.setStroke(this.grid);
		drawingBorder.setFill(null);
		return drawingBorder;
	}

	private CubicCurve createDrawingLink(LinkDescriptor linkDesc) {
		IoDescriptor inputDesc = linkDesc.input;
		IoDescriptor outputDesc = linkDesc.output;
		CubicCurve cubic = new CubicCurve();
		if (inputDesc.io instanceof BlockIO)
			this.drawingBlocks.get(linkDesc.inputBlock).setPosition(inputDesc.io, cubic);
		else if (inputDesc.io instanceof FlowDiagramInput) {
			DrawableInput dinput = this.drawingInput.get(inputDesc.io);
			dinput.setPosition(inputDesc.io, cubic);
			dinput.updateIOInfo();
		}
		if (outputDesc.io instanceof BlockIO)
			this.drawingBlocks.get(linkDesc.outputBlock).setPosition(outputDesc.io, cubic);
		else if (outputDesc.io instanceof FlowDiagramOutput) {
			DrawableOutput doutput = this.drawingOutput.get(outputDesc.io);
			doutput.setPosition(outputDesc.io, cubic);
			doutput.updateIOInfo(); // output input input avant
		}
		InvalidationListener list = e -> updateCurveControlPoint(cubic);
		cubic.startXProperty().addListener(list);
		cubic.startYProperty().addListener(list);
		cubic.endXProperty().addListener(list);
		cubic.endYProperty().addListener(list);
		updateCurveControlPoint(cubic);
		cubic.setStroke(this.link);
		cubic.setFill(null);
		this.drawingLinks.put(linkDesc, cubic);
		cubic.setVisible(this.filterLink);
		return cubic;
	}

	private Group creatGrid() {
		Group g = new Group();
		g.setId(GRID);
		ObservableList<Node> gridChildren = g.getChildren();
		double scale = getScale();
		int ls = (int) Math.round(this.getScenarioWidth() * getScale());
		int hs = (int) Math.round(this.getScenarioHeight() * scale);
		int ls1 = ls + 1;
		int hs1 = hs + 1;
		for (float i = 0; i < ls1; i += scale * this.gridLenght) {
			Line line = new Line((int) i + 0.5, 0 + 0.5, (int) i + 0.5, hs + 0.5);
			line.setStroke(this.grid);
			line.setStrokeWidth(1);
			gridChildren.add(line);
		}
		for (float i = 0; i < hs1; i += scale * this.gridLenght) {
			Line line = new Line(0 + 0.5, (int) i + 0.5, ls + 0.5, (int) i + 0.5);
			line.setStroke(this.grid);
			line.setStrokeWidth(1);
			gridChildren.add(line);
		}
		return g;
	}

	private void deleteElementFromSelectionAndPropertiesStage(Object element) {
		this.selectedElements.remove(element);
		Stage propertiesStage = this.visibleProperties.remove(element);
		if (propertiesStage != null)
			propertiesStage.close();
	}

	private static double distance(CubicCurve curve, Point2D point) {
		float flatness = 0.01f;
		FlatteningPathIterator pit = new FlatteningPathIterator(new CubicIterator(curve), flatness);
		float[] coords = new float[2];
		float[] oldCoords = null;
		double minDist = Double.MAX_VALUE;
		while (!pit.isDone()) {
			pit.currentSegment(coords);
			if (oldCoords == null)
				oldCoords = new float[2];
			else {
				double dist = squaredDistancePointToSegment(oldCoords, coords, point);
				if (dist < minDist)
					minDist = dist;
			}
			oldCoords[0] = coords[0];
			oldCoords[1] = coords[1];
			pit.next();
		}
		return minDist;
	}

	private void forceRepaint() {
		if (getDrawableElement() != null) {
			this.oldFd = null;
			repaint(false);
		}
	}

	private void updateDynamicDiagramInfoRefreshPeriod() {
		this.dynamicDiagramInfoRefreshPeriod = 1.0 / (this.dynamicDiagramInfoRefreshRate / 1000.0);
	}

	private static IoDescriptor[] getIODescriptors(Block block) {
		ArrayList<IoDescriptor> ioDescriptors = new ArrayList<>();
		int index = 0;
		List<BlockInput> inputs = block.getInputs();
		for (int j = 0; j < block.getNbPropertyAsInput(); j++)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.PROPERTY));
		for (int j = 0; j < block.getNbStaticInput(); j++)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.STATIC));
		for (int j = 0; j < block.getNbDynamicInput(); j++)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.DYNAMIC));
		for (int j = 0; j < block.getNbVarArgsInput(); j++)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.VARARGS));
		if (inputs.size() != index)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.VARARGS));
		List<BlockOutput> outputs = block.getOutputs();
		index = 0;
		for (int k = 0; k < block.getNbPropertyAsOutput(); k++)
			ioDescriptors.add(new IoDescriptor(outputs.get(index), index++, IoDescriptor.PROPERTY));
		for (int k = 0; k < block.getNbStaticOutput(); k++)
			ioDescriptors.add(new IoDescriptor(outputs.get(index), index++, IoDescriptor.STATIC));
		int end = block.getNbOutput() - index;
		for (int k = 0; k < end; k++)
			ioDescriptors.add(new IoDescriptor(outputs.get(index), index++, IoDescriptor.DYNAMIC));
		return ioDescriptors.toArray(IoDescriptor[]::new);
	}

	private static IoDescriptor[] getIODescriptors(IO io) {
		return new IoDescriptor[] { new IoDescriptor(io, 0, IoDescriptor.STATIC) };
	}

	private static String getIOToolTipInfo(IO io) {
		StringBuilder sb = new StringBuilder();
		sb.append("Name: " + io.getName());
		sb.append("\nType: " + (io.getType() == null ? "not defined" : io.getType().getSimpleName()));
		sb.append("\nPath: " + (io.getType() == null ? String.valueOf((Object) null) : io.getType()));
		if (io instanceof Input) {
			sb.append("\nReaderType: " + ((Input) io).getReaderType());
			sb.append("\nMaxStackSize: " + ((Input) io).getMaxStackSize());
			sb.append("\nEraseDataIfFull: " + ((Input) io).isEraseDataIfFull());
		}
		if (io.getError() != null)
			sb.append("\nerror: " + io.getError());
		return sb.toString();
	}

	private static boolean isAssignable(Input input, Output output) {
		if (input instanceof FlowDiagramInput && output instanceof FlowDiagramOutput)
			return false;
		Class<?> inputType = input instanceof FlowDiagramInput ? null : input.getType();
		Class<?> outputType = output.getType();
		if (inputType != null) {
			if (outputType != null && !inputType.isAssignableFrom(outputType))
				return false;
			if (input instanceof BlockInput) {
				Block inputBlock = ((BlockInput) input).getBlock();
				if (((BlockInput) input).isVarArgs && inputBlock.getOperator() instanceof EvolvedVarArgsOperator) {
					// Accès à des données
					List<BlockInput> inputs = inputBlock.getInputs();
					int nbPropertyInput = inputBlock.getNbPropertyAsInput();
					Class<?>[] inputsType = new Class[inputs.size() - nbPropertyInput];
					for (int i = nbPropertyInput; i < inputs.size(); i++) {
						Link link = inputs.get(i).getLink();
						inputsType[i - nbPropertyInput] = link == null ? null : link.getOutput().getType();
					}
					return ((EvolvedVarArgsOperator) inputBlock.getOperator()).isValidInput(inputsType, outputType);
				}
			}
			return true;
		}
		return true;
	}

	private boolean iscollision(LocalisedObject selectedElement, double newX, double newY) {
		Rectangle2D rect;
		if (selectedElement instanceof Block)
			rect = ((Block) selectedElement).getRectangle();
		else {
			DrawableDiagramIO<?> fdio = (DrawableDiagramIO<?>) selectedElement;
			rect = new Rectangle2D(fdio.getPosition().getX() - this.ioFlowDiagramSize, fdio.getPosition().getY() - this.ioFlowDiagramSize, this.ioFlowDiagramSize * 2, this.ioFlowDiagramSize * 2);
		}
		rect = new Rectangle2D(newX - rect.getWidth() / 2.0, newY - rect.getHeight() / 2.0, rect.getWidth(), rect.getHeight());
		return iscollision(selectedElement, rect, newX, newY);
	}

	private boolean iscollision(LocalisedObject selectedElement, Rectangle2D rect, double newX, double newY) {
		double[] x = new double[4];
		double[] y = new double[4];
		x[0] = rect.getMinX();
		x[1] = x[0];
		x[2] = x[0] + rect.getWidth();
		x[3] = x[2];
		y[0] = rect.getMinY();
		y[1] = y[0] + rect.getHeight();
		y[2] = y[1];
		y[3] = y[0];
		ArrayList<Rectangle2D> collisionShape = new ArrayList<>();
		FlowDiagram fd = (FlowDiagram) getDrawableElement();
		for (Block block : fd.getBlocks())
			if (block != selectedElement)
				collisionShape.add(block.getRectangle());
		for (FlowDiagramInput fdi : fd.getInputs()) {
			if (selectedElement instanceof DrawableDiagramIO ? ((DrawableDiagramIO<?>) selectedElement).io == fdi : selectedElement == fdi)
				continue;
			collisionShape
					.add(new Rectangle2D(fdi.getPosition().getX() - this.ioFlowDiagramSize, fdi.getPosition().getY() - this.ioFlowDiagramSize, this.ioFlowDiagramSize * 2, this.ioFlowDiagramSize * 2));
		}
		for (FlowDiagramOutput fdo : fd.getOutputs()) {
			if (selectedElement instanceof DrawableDiagramIO ? ((DrawableDiagramIO<?>) selectedElement).io == fdo : selectedElement == fdo)
				continue;
			collisionShape
					.add(new Rectangle2D(fdo.getPosition().getX() - this.ioFlowDiagramSize, fdo.getPosition().getY() - this.ioFlowDiagramSize, this.ioFlowDiagramSize * 2, this.ioFlowDiagramSize * 2));
		}
		for (Rectangle2D rectOther : collisionShape) {
			for (int j = 0; j < x.length; j++)
				if (rectOther.getMinX() <= x[j] && rectOther.getMinY() <= y[j] && rectOther.getMaxX() >= x[j] && rectOther.getMaxY() >= y[j])
					return true;
			double[] xo = new double[4];
			double[] yo = new double[4];
			xo[0] = rectOther.getMinX();
			xo[1] = xo[0];
			xo[2] = xo[0] + rectOther.getWidth();
			xo[3] = xo[2];
			yo[0] = rectOther.getMinY();
			yo[1] = yo[0] + rectOther.getHeight();
			yo[2] = yo[1];
			yo[3] = yo[0];
			for (int j = 0; j < xo.length; j++)
				if (rect.getMinX() <= xo[j] && rect.getMinY() <= yo[j] && rect.getMaxX() >= xo[j] && rect.getMaxY() >= yo[j])
					return true;
		}
		return false;
	}

	private void removeLink(LinkDescriptor linkDesc) {
		this.drawingElementGroup.getChildren().remove(this.drawingLinks.get(linkDesc));
		this.drawingLinks.remove(linkDesc);
		if (linkDesc.input.io instanceof FlowDiagramInput) {
			IoDescriptor input = linkDesc.input;
			this.drawingInput.get(input.io).updateIOInfo();
		} else if (linkDesc.output.io instanceof FlowDiagramOutput) {
			IoDescriptor output = linkDesc.output;
			this.drawingOutput.get(output.io).updateIOInfo();
		}
	}

	private void resetDrawingLinksColor() {
		FlowDiagram fd = (FlowDiagram) getDrawableElement();
		if (fd == null || this.drawingLinks == null)
			return;
		for (CubicCurve cc : this.drawingLinks.values())
			cc.setStroke(this.link);
		// for (Block block : fd.getBlocks())
		// for (BlockInput input : block.getInputs()) {
		// Link link = input.getLink();
		// if (link != null)
		// drawingLinks.get(link).setStroke(this.link);
		// }
	}

	private void setBorderScale(Rectangle node, double scale) {
		node.setWidth(this.getScenarioWidth() * getScale() + 0.5);
		node.setHeight(this.getScenarioHeight() * getScale() + 0.5);
	}

	private void showBlockProperties(Block block) {
		Stage dia = this.visibleProperties.remove(block);
		if (dia != null) {
			dia.requestFocus();
			return;
		}
		Stage dialog = new Stage(StageStyle.DECORATED);
		dialog.initOwner(getScene().getWindow());
		dialog.setTitle(block.getName());
		final BeanManager bmo;
		if (block.isRemoteProperty()) {
			BeanManager bmTemp = null;
			try {
				BeanManager bmLocal = new BeanManager(block.operator, BeanManager.defaultDir);
				RemoteOperator remoteOperator = null;
				try {
					remoteOperator = RemoteOperator.createRemoteOperator(block, false, 1000);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				if (remoteOperator == null || !remoteOperator.isConnected()) {
					this.alert = new Alert(AlertType.ERROR, "Cannot reach " + block.getName() + " " + block.getProcessMode().toString().toLowerCase() + " bean.\nSwitch block to local process mode?",
							ButtonType.CANCEL, ButtonType.OK);
					Optional<ButtonType> result = this.alert.showAndWait();
					if (result.isPresent() && result.get() == ButtonType.OK) {
						block.setProcessMode(ProcessMode.LOCAL);
						showBlockProperties(block);
					}
					return;
				}
				bmTemp = new RMIBeanManager(remoteOperator, BeanManager.defaultDir);
				bmLocal.copyTo(bmTemp);
			} finally {
				bmo = bmTemp;
			}
		} else
			bmo = new BeanManager(block.operator, BeanManager.defaultDir);
		final BeanManager bmb = new BeanManager(block, "");
		LinkedList<PropertyDescriptor> propDesc = new LinkedList<>();
		ArrayList<Object> blockBackup = new ArrayList<>();
	
		GridPane editor = bmo.getEditor();
		addAsInputOutputMenuToEditor(block, null, bmo.getBean(), bmo, editor);
		ScrollPane sp = new ScrollPane(editor);
		sp.setFitToWidth(true);
		sp.setFitToHeight(true);
		sp.setHbarPolicy(ScrollBarPolicy.NEVER);
	
		Tab operatorPropertiesTab = new Tab("Operator properties", sp);
		operatorPropertiesTab.setClosable(false);
		Tab blockPropertiesTab = new Tab("Block properties");
		blockPropertiesTab.setClosable(false);
		TabPane tabPane = new TabPane(operatorPropertiesTab, blockPropertiesTab);
		tabPane.getSelectionModel().selectedItemProperty().addListener((obs, oldTab, newTab) -> {
			GridPane editorGridPane;
			if (newTab == blockPropertiesTab) {
				editorGridPane = bmb.getEditor();
				if (propDesc.isEmpty()) {
					propDesc.addAll(BeanManager.getPropertyDescriptors(Block.class));
					for (final PropertyDescriptor pd : propDesc)
						try {
							blockBackup.add(pd.getReadMethod().invoke(block));
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							e.printStackTrace();
						}
				}
			} else {
				editorGridPane = bmo.getEditor();
				addAsInputOutputMenuToEditor(block, null, bmo.getBean(), bmo, editorGridPane);
			}
			newTab.setContent(editorGridPane);
			oldTab.setContent(null);
			tabPane.requestLayout(); // TODO JDK-8145490
			dialog.sizeToScene();
		});
	
		BorderPane bp = new BorderPane(tabPane);
		Button okButton = new Button("Ok");
		BorderPane.setAlignment(okButton, Pos.CENTER);
		Runnable closingAction = () -> {
			Object bean = bmo.getBean();
			if (bean instanceof RemoteBean)
				((RemoteBean) bean).destroy();
			if (tabPane.getSelectionModel().getSelectedItem() == operatorPropertiesTab)
				bmo.saveIfChanged();
			else
				saveScenario();
			this.visibleProperties.remove(block);
		};
		// block.addPropertyStructChangeListener(() -> {
		//
		// System.out.println("changed");
		// });
		okButton.setOnAction(e1 -> {
			dialog.close();
			closingAction.run();
		});
	
		Button resetButton = new Button("Reset");
		resetButton.setOnAction(e1 -> {
			if (tabPane.getSelectionModel().getSelectedItem() == operatorPropertiesTab) {
				bmo.reset();
				GridPane editorGridPane = bmo.getEditor();
				operatorPropertiesTab.setContent(editorGridPane);
				addAsInputOutputMenuToEditor(block, null, bmo.getBean(), bmo, editorGridPane);
			} else {
				block.resetProperties();
				blockPropertiesTab.setContent(bmb.getEditor());
			}
			tabPane.requestLayout(); // JDK-8145490
			dialog.sizeToScene();
		});
		Button refreshButton = new Button("Refresh");
		refreshButton.setOnAction(e1 -> {
			if (tabPane.getSelectionModel().getSelectedItem() == operatorPropertiesTab) {
				GridPane editorGridPane = bmo.getEditor();
				operatorPropertiesTab.setContent(editorGridPane);
				addAsInputOutputMenuToEditor(block, null, bmo.getBean(), bmo, editorGridPane);
			} else
				blockPropertiesTab.setContent(bmb.getEditor());
			tabPane.requestLayout(); // JDK-8145490
			dialog.sizeToScene();
		});
		Button saveButton = new Button("Save");
		saveButton.setOnAction(e1 -> {
			bmo.saveIfChanged();
			if (bmb.isEdited)
				saveScenario();
		});
		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e1 -> {
			if (tabPane.getSelectionModel().getSelectedItem() == operatorPropertiesTab) {
				bmo.load();
				bmo.isEdited = false;
				BeanManager.createSubBeans(bmo.getBean(), BeanManager.defaultDir);
				operatorPropertiesTab.setContent(bmo.getEditor());
			} else {
				int j = 0;
				for (final PropertyDescriptor pd : propDesc)
					try {
						pd.getWriteMethod().invoke(block, blockBackup.get(j++));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				blockPropertiesTab.setContent(bmb.getEditor());
			}
			tabPane.requestLayout(); // JDK-8145490
			dialog.sizeToScene();
		});
		HBox vBox = new HBox(10, resetButton, refreshButton, saveButton, cancelButton, okButton);
		vBox.setAlignment(Pos.CENTER);
		vBox.setPadding(new Insets(3, 3, 3, 3));
		bp.setBottom(vBox);
		dialog.setScene(new Scene(bp, -1, -1));
		dialog.sizeToScene();
		dialog.setOnCloseRequest(e1 -> closingAction.run());
		dialog.show();
		this.visibleProperties.put(block, dialog);
		DynamicSizeEditorChangeListener scl = () -> {
			tabPane.requestLayout(); // JDK-8145490
			dialog.sizeToScene();
		};
		bmo.addSizeChangeListener(scl);
		bmb.addSizeChangeListener(scl);
		// ScenicView.show(_scene);
	}

	private void showDiagramContextMenu(ContextMenuEvent e) {
		ArrayList<DrawableIOComponent<?>> l = new ArrayList<>();
		l.addAll(this.drawingBlocks.values());
		l.addAll(this.drawingInput.values());
		l.addAll(this.drawingOutput.values());
		for (DrawableIOComponent<?> db : l) {
			Point2D p = db.core.sceneToLocal(e.getSceneX(), e.getSceneY(), true /* rootScene */);
			if (db.core.isVisible() && !db.core.isMouseTransparent() && db.core.contains(p))
				return;
		}
		if (this.contextMenu != null && this.contextMenu.isShowing()) {
			this.contextMenu.setAnchorX(e.getScreenX());
			this.contextMenu.setAnchorY(e.getScreenY());
		} else {
			createDiagramContextMenu();
			Task<Void> sleeper = new Task<>() {
				@Override
				protected Void call() throws Exception {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {}
					return null;
				}
			};
			sleeper.setOnSucceeded(event -> {
				ContextMenu contextMenu = this.contextMenu;
				if (contextMenu != null)
					contextMenu.show(DiagramDrawer.this, e.getScreenX(), e.getScreenY());
			});
			new Thread(sleeper).start();
		}
	}

	/* Renvoie la distance de p à la droite (ps, pe) au carrée */
	private static double squaredDistancePointToSegment(float[] ps, float[] pe, Point2D p) {
		if (ps[0] == pe[0] && ps[1] == pe[1])
			return Math.pow(p.getX() - ps[0], 2) + Math.pow(p.getY() - ps[1], 2);
		double sx = pe[0] - ps[0];
		double sy = pe[1] - ps[1];
		double ux = p.getX() - ps[0];
		double uy = p.getY() - ps[1];
		double dp = sx * ux + sy * uy;
		if (dp < 0)
			return Math.pow(p.getX() - ps[0], 2) + Math.pow(p.getY() - ps[1], 2);
		double sn2 = sx * sx + sy * sy;
		if (dp > sn2)
			return Math.pow(p.getX() - pe[0], 2) + Math.pow(p.getY() - pe[1], 2);
		return Math.pow(ux, 2) + Math.pow(uy, 2) - dp * dp / sn2;
	}

	private static void updateCurveControlPoint(CubicCurve cubic) {
		double y1 = cubic.getStartY();
		double y2 = cubic.getEndY();
		double x1 = cubic.getStartX();
		double x2 = cubic.getEndX();
		double deltaValueX = x2 - x1;
		if (deltaValueX < 0)
			deltaValueX = 0;
		double deltaValueY = y2 > y1 ? deltaValueX : -deltaValueX;
		double mid = (x1 + x2) / 2.0;
		cubic.setControlX1(mid - deltaValueX);
		cubic.setControlY1(y1 + deltaValueY);
		cubic.setControlX2(mid + deltaValueX);
		cubic.setControlY2(y2 - deltaValueY);
	}

	private void updateLinkIO() {
		for (DrawableBlock drawableBlock : this.drawingBlocks.values())
			drawableBlock.updateIOStroke();
		for (DrawableInput drawableInput : this.drawingInput.values())
			drawableInput.updateIOStroke();
		for (DrawableOutput drawableOutput : this.drawingOutput.values())
			drawableOutput.updateIOStroke();
	}

	private void updatePaneInfo() {
		if (!isStopped())
			stateChanged(this.filterStackState || this.filterCPUUsage || this.filterHeatLink || this.filterCount || this.filterMissedData ? SchedulerState.STARTED : SchedulerState.PRESTOP);
	}

	private void updatePosition(double newX, double newY, LocalisedObject selectedElement) {
		selectedElement.setPosition(newX, newY);
		if (selectedElement instanceof Block)
			this.drawingBlocks.get(selectedElement).updatePosition();
		else if (selectedElement instanceof DrawableInput)
			((DrawableInput) selectedElement).updatePosition();
		else
			((DrawableOutput) selectedElement).updatePosition();
	
	}

	private void updateStopAlert(List<Block> waitingBlocks, boolean phase) {
		if (this.alert != null) {
			StringBuilder sb = new StringBuilder();
			for (Iterator<Block> iterator = waitingBlocks.iterator(); iterator.hasNext();) {
				Block block = iterator.next();
				sb.append(block.getName());
				if (iterator.hasNext())
					sb.append("\n");
			}
			String waitingBlocksText = sb.toString();
			if (!waitingBlocksText.equals(this.alert.getContentText()))
				this.alert.setContentText(sb.toString());
		}
		if (this.oldAlivedBlocks != null)
			for (Block block : this.oldAlivedBlocks) {
				DrawableBlock drawingBlock = this.drawingBlocks.get(block);
				if (drawingBlock != null)
					if (!waitingBlocks.contains(block))
						drawingBlock.resetCoreStroke();
					else
						drawingBlock.updateDrawableWithStopWarning(phase, this.interruptedMode);
			}
		else
			for (Block block : waitingBlocks) {
				DrawableBlock drawingBlock = this.drawingBlocks.get(block);
				if (drawingBlock != null)
					drawingBlock.updateDrawableWithStopWarning(phase, this.interruptedMode);
			}
		this.oldAlivedBlocks = new ArrayList<>(waitingBlocks);
	}

	public Color getDiagramBackground() {
		return this.diagramBackground;
	}

	public void setDiagramBackground(Color diagramBackground) {
		var oldValue = this.diagramBackground;
		this.diagramBackground = diagramBackground;
		setBackground(new Background(new BackgroundFill(diagramBackground, null, null)));
		forceRepaint();
		pcs.firePropertyChange("diagramBackground", oldValue, this.diagramBackground);
	}

	public Color getGrid() {
		return this.grid;
	}

	public void setGrid(Color grid) {
		var oldValue = this.grid;
		this.grid = grid;
		forceRepaint();
		pcs.firePropertyChange("grid", oldValue, this.grid);
	}

	public float getGridLenght() {
		return this.gridLenght;
	}

	public void setGridLenght(float gridLenght) {
		var oldValue = this.gridLenght;
		this.gridLenght = gridLenght;
		forceRepaint();
		pcs.firePropertyChange("gridLenght", oldValue, this.gridLenght);
	}

	public Color getComponent() {
		return this.component;
	}

	public void setComponent(Color component) {
		var oldValue = this.component;
		this.component = component;
		forceRepaint();
		pcs.firePropertyChange("component", oldValue, this.component);
	}

	public Color getComponentText() {
		return componentText;
	}

	public void setComponentText(Color componentText) {
		var oldValue = this.componentText;
		this.componentText = componentText;
		forceRepaint();
		pcs.firePropertyChange("componentText", oldValue, this.componentText);
	}

	public int getComponentCornerRadius() {
		return this.componentCornerRadius;
	}

	public void setComponentCornerRadius(int componentCornerRadius) {
		var oldValue = this.componentCornerRadius;
		this.componentCornerRadius = componentCornerRadius;
		forceRepaint();
		pcs.firePropertyChange("componentCornerRadius", oldValue, this.componentCornerRadius);
	}

	public float getHeaderSize() {
		return this.headerSize;
	}

	public void setHeaderSize(float headerSize) {
		var oldValue = this.headerSize;
		this.headerSize = headerSize;
		forceRepaint();
		pcs.firePropertyChange("headerSize", oldValue, this.headerSize);
	}

	public Color getComponentHeaderLocal() {
		return this.componentHeaderLocal;
	}

	public void setComponentHeaderLocal(Color componentHeaderLocal) {
		var oldValue = this.componentHeaderLocal;
		this.componentHeaderLocal = componentHeaderLocal;
		forceRepaint();
		pcs.firePropertyChange("componentHeaderLocal", oldValue, this.componentHeaderLocal);
	}

	public Color getComponentHeaderIsolated() {
		return this.componentHeaderIsolated;
	}

	public void setComponentHeaderIsolated(Color componentHeaderIsolated) {
		var oldValue = this.componentHeaderIsolated;
		this.componentHeaderIsolated = componentHeaderIsolated;
		forceRepaint();
		pcs.firePropertyChange("componentHeaderIsolated", oldValue, this.componentHeaderIsolated);
	}

	public Color getComponentHeaderRemote() {
		return this.componentHeaderRemote;
	}

	public void setComponentHeaderRemote(Color componentHeaderRemote) {
		var oldValue = this.componentHeaderRemote;
		this.componentHeaderRemote = componentHeaderRemote;
		forceRepaint();
		pcs.firePropertyChange("componentHeaderRemote", oldValue, this.componentHeaderRemote);
	}

	public Color getComponentBorder() {
		return this.componentBorder;
	}

	public void setComponentBorder(Color componentBorder) {
		var oldValue = this.componentBorder;
		this.componentBorder = componentBorder;
		forceRepaint();
		pcs.firePropertyChange("componentBorder", oldValue, this.componentBorder);
	}

	public Color getComponentPropertyIO() {
		return this.componentPropertyIO;
	}

	public void setComponentPropertyIO(Color componentPropertyIO) {
		var oldValue = this.componentPropertyIO;
		this.componentPropertyIO = componentPropertyIO;
		forceRepaint();
		pcs.firePropertyChange("componentPropertyIO", oldValue, this.componentPropertyIO);
	}

	public Color getComponentStaticIO() {
		return this.componentStaticIO;
	}

	public void setComponentStaticIO(Color componentStaticIO) {
		var oldValue = this.componentStaticIO;
		this.componentStaticIO = componentStaticIO;
		forceRepaint();
		pcs.firePropertyChange("componentStaticIO", oldValue, this.componentStaticIO);
	}

	public Color getComponentDynamicIO() {
		return this.componentDynamicIO;
	}

	public void setComponentDynamicIO(Color componentDynamicIO) {
		var oldValue = this.componentDynamicIO;
		this.componentDynamicIO = componentDynamicIO;
		forceRepaint();
		pcs.firePropertyChange("componentDynamicIO", oldValue, this.componentDynamicIO);
	}

	public Color getComponentVarArgsIO() {
		return this.componentVarArgsIO;
	}

	public void setComponentVarArgsIO(Color componentVarArgsIO) {
		var oldValue = this.componentVarArgsIO;
		this.componentVarArgsIO = componentVarArgsIO;
		forceRepaint();
		pcs.firePropertyChange("componentVarArgsIO", oldValue, this.componentVarArgsIO);
	}

	public Color getShadow() {
		return this.shadow;
	}

	public void setShadow(Color shadow) {
		var oldValue = this.shadow;
		this.shadow = shadow;
		forceRepaint();
		pcs.firePropertyChange("shadow", oldValue, this.shadow);
	}

	public float getShadowRadius() {
		return this.shadowRadius;
	}

	public void setShadowRadius(float shadowRadius) {
		var oldValue = this.shadowRadius;
		this.shadowRadius = shadowRadius;
		forceRepaint();
		pcs.firePropertyChange("shadowRadius", oldValue, this.shadowRadius);
	}

	public Color getCountForeground() {
		return this.countForeground;
	}

	public void setCountForeground(Color countForeground) {
		var oldValue = this.countForeground;
		this.countForeground = countForeground;
		forceRepaint();
		pcs.firePropertyChange("countForeground", oldValue, this.countForeground);
	}

	public Color getCpuBarBackground() {
		return this.cpuBarBackground;
	}

	public void setCpuBarBackground(Color cpuBarBackground) {
		var oldValue = this.cpuBarBackground;
		this.cpuBarBackground = cpuBarBackground;
		forceRepaint();
		pcs.firePropertyChange("cpuBarBackground", oldValue, this.cpuBarBackground);
	}

	public Color getCpuBarForeground() {
		return this.cpuBarForeground;
	}

	public void setCpuBarForeground(Color cpuBarForeground) {
		var oldValue = this.cpuBarForeground;
		this.cpuBarForeground = cpuBarForeground;
		forceRepaint();
		pcs.firePropertyChange("cpuBarForeground", oldValue, this.cpuBarForeground);
	}

	public int getCpuBarHeight() {
		return this.cpuBarHeight;
	}

	public void setCpuBarHeight(int cpuBarHeight) {
		var oldValue = this.cpuBarHeight;
		this.cpuBarHeight = cpuBarHeight;
		forceRepaint();
		pcs.firePropertyChange("cpuBarHeight", oldValue, this.cpuBarHeight);
	}

	public int getCpuBarVerticalGap() {
		return this.cpuBarVerticalGap;
	}

	public void setCpuBarVerticalGap(int cpuBarVerticalGap) {
		var oldValue = this.cpuBarVerticalGap;
		this.cpuBarVerticalGap = cpuBarVerticalGap;
		forceRepaint();
		pcs.firePropertyChange("cpuBarVerticalGap", oldValue, this.cpuBarVerticalGap);
	}

	public int getIoFlowDiagramSize() {
		return this.ioFlowDiagramSize;
	}

	public void setIoFlowDiagramSize(int ioFlowDiagramSize) {
		var oldValue = this.ioFlowDiagramSize;
		this.ioFlowDiagramSize = ioFlowDiagramSize;
		forceRepaint();
		pcs.firePropertyChange("ioFlowDiagramSize", oldValue, this.ioFlowDiagramSize);
	}

	public int getIoRoundRadius() {
		return this.ioRoundRadius;
	}

	public void setIoRoundRadius(int ioRoundRadius) {
		var oldValue = this.ioRoundRadius;
		this.ioRoundRadius = ioRoundRadius;
		forceRepaint();
		pcs.firePropertyChange("ioRoundRadius", oldValue, this.ioRoundRadius);
	}

	public Color getIoTextColor() {
		return this.ioTextColor;
	}

	public void setIoTextColor(Color ioTextColor) {
		var oldValue = this.ioTextColor;
		this.ioTextColor = ioTextColor;
		forceRepaint();
		pcs.firePropertyChange("ioTextColor", oldValue, this.ioTextColor);
	}

	public Color getLink() {
		return this.link;
	}

	public void setLink(Color link) {
		var oldValue = this.link;
		this.link = link;
		forceRepaint();
		pcs.firePropertyChange("link", oldValue, this.link);
	}

	public float getLinkSelectionDistance() {
		return this.linkSelectionDistance;
	}

	public void setLinkSelectionDistance(float linkSelectionDistance) {
		var oldValue = this.linkSelectionDistance;
		this.linkSelectionDistance = linkSelectionDistance;
		pcs.firePropertyChange("linkSelectionDistance", oldValue, this.linkSelectionDistance);
	}

	public Color getSelectionColor() {
		return this.selectionColor;
	}

	public void setSelectionColor(Color selectionColor) {
		var oldValue = this.selectionColor;
		this.selectionColor = selectionColor;
		forceRepaint();
		pcs.firePropertyChange("selectionColor", oldValue, this.selectionColor);
	}

	public double getDynamicDiagramInfoRefreshRate() {
		return this.dynamicDiagramInfoRefreshRate;
	}

	public void setDynamicDiagramInfoRefreshRate(double dynamicDiagramInfoRefreshRate) {
		var oldValue = this.dynamicDiagramInfoRefreshRate;
		this.dynamicDiagramInfoRefreshRate = dynamicDiagramInfoRefreshRate;
		updateDynamicDiagramInfoRefreshPeriod();
		pcs.firePropertyChange("dynamicDiagramInfoRefreshRate", oldValue, this.dynamicDiagramInfoRefreshRate);
	}

	abstract class DrawableIOComponent<T extends Shape> extends Group {
		protected T core;
		protected LinkedHashMap<IO, DrawableIOComponent<? extends Shape>.DrawingIo> drawingIOs = new LinkedHashMap<>();
		protected double scale;
		private CubicCurve tempLinkCurve;
		private InvalidationListener tempLinkFocusListener;
		private Circle pickIO;
		private Point2D mousePointInScene;

		class DrawingIo {
			public Circle circle;
			public Text text;

			public DrawingIo(Circle circle, Text text) {
				this.circle = circle;
				this.text = text;
				this.circle.setViewOrder(-3);
				this.text.setViewOrder(-3);
			}
		}

		public DrawableIOComponent(double scale) {
			this.scale = scale;
		}

		private void clearTempLink() {
			DiagramDrawer.this.focusedProperty().removeListener(this.tempLinkFocusListener);
			this.tempLinkFocusListener = null;
			DiagramDrawer.this.tempLink = null;
			DiagramDrawer.this.drawingElementGroup.getChildren().remove(this.tempLinkCurve);
			clearSelectedElements();
			updateLinkIO();
			this.mousePointInScene = null;
			this.tempLinkCurve = null;
		}

		protected void createIO(IoDescriptor ioDesc) {
			Circle drawableIO = new Circle();
			drawableIO.setStroke(DiagramDrawer.this.componentBorder);
			drawableIO.setStrokeType(StrokeType.OUTSIDE);
			drawableIO.setFill(ioDesc.type == IoDescriptor.PROPERTY ? DiagramDrawer.this.componentPropertyIO
					: ioDesc.type == IoDescriptor.STATIC ? DiagramDrawer.this.componentStaticIO
							: ioDesc.type == IoDescriptor.DYNAMIC ? DiagramDrawer.this.componentDynamicIO : DiagramDrawer.this.componentVarArgsIO);
			Text drawableIOName = new Text(ioDesc.name);
			// drawableIOName.setMouseTransparent(true);
			this.drawingIOs.put(ioDesc.io, new DrawingIo(drawableIO, drawableIOName));
			drawableIO.setVisible(DiagramDrawer.this.filterIO);
			drawableIOName.setVisible(DiagramDrawer.this.filterIOName);
			updateIO(ioDesc.io);
			updateIONameStyle(ioDesc.io);
			Tooltip toolTip = new Tooltip(getIOToolTipInfo(ioDesc.io));
			Tooltip.install(drawableIO, toolTip);
			drawableIO.setOnContextMenuRequested(e -> {
				if (ioDesc.type == IoDescriptor.PROPERTY && (ioDesc.io instanceof BlockInput || ioDesc.io instanceof BlockOutput))
					if (DiagramDrawer.this.contextMenu != null && DiagramDrawer.this.contextMenu.isShowing()) {
						DiagramDrawer.this.contextMenu.setAnchorX(e.getScreenX());
						DiagramDrawer.this.contextMenu.setAnchorY(e.getScreenY());
					} else {
						CheckMenuItem menuProperties = new CheckMenuItem("Remove");
						menuProperties.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
						menuProperties.setOnAction(e1 -> {
							Block block = ioDesc.io instanceof BlockInput ? ((BlockInput) ioDesc.io).getBlock() : ((BlockOutput) ioDesc.io).getBlock();
							Object blockOperator = block.operator;
							if (blockOperator instanceof EvolvedOperator)
								((EvolvedOperator) blockOperator).removePropertyAsInput(ioDesc.name);
							else
								block.removePropertyAsInput(ioDesc.name);
							DiagramDrawer.this.selectedElements.remove(ioDesc.io);
						});
						DiagramDrawer.this.contextMenu = new ContextMenu(menuProperties);
						DiagramDrawer.this.contextMenu.show(this, e.getScreenX(), e.getScreenY());
					}
				e.consume();
			});
			drawableIO.setCursor(Cursor.HAND);
			drawableIOName.setCursor(Cursor.HAND);
			ioDesc.io.addErrorChangeListener(() -> {
				toolTip.setText(getIOToolTipInfo(ioDesc.io));
				resetStroke(ioDesc.io);
			});
			drawableIO.setOnMousePressed(e -> {
				if (e.getButton() == MouseButton.PRIMARY) {
					if (!DiagramDrawer.this.selectedElements.contains(ioDesc.io)) {
						if (!e.isControlDown())
							clearSelectedElements();
						DiagramDrawer.this.selectedElements.add(ioDesc.io);
						drawableIO.setStroke(DiagramDrawer.this.selectionColor);
					}
					e.consume();
				}
			});
			drawableIOName.setOnMousePressed(drawableIO.getOnMousePressed());
			drawableIO.setOnMouseDragged(e -> {
				if (DiagramDrawer.this.tempLink == null)
					if (e.getButton() == MouseButton.PRIMARY) {
						this.tempLinkCurve = new CubicCurve();
						DiagramDrawer.this.tempLink = ioDesc.io instanceof Input ? new Link((Input) ioDesc.io, null) : new Link(null, (Output) ioDesc.io);
						this.tempLinkFocusListener = e1 -> {
							if (!DiagramDrawer.this.isFocused() && DiagramDrawer.this.tempLink != null)
								clearTempLink();
						};
						DiagramDrawer.this.focusedProperty().addListener(this.tempLinkFocusListener);
						setPosition(ioDesc.io, this.tempLinkCurve);
						DiagramDrawer.this.drawingElementGroup.getChildren().add((DiagramDrawer.this.isFilterGrid() ? 1 : 0) + (DiagramDrawer.this.isFilterBorder() ? 1 : 0), this.tempLinkCurve);
						this.tempLinkCurve.setStroke(DiagramDrawer.this.selectionColor);
						this.tempLinkCurve.setFill(null);
						updateLinkIO();
					} else
						return;
				Point2D p = new Point2D(e.getX(), e.getY());
				Node src = (Node) e.getSource();
				this.mousePointInScene = src.localToScene(p);
				updateTempLinkWithMousePos(src.getParent().localToParent(src.localToParent(p)));
			});
			drawableIOName.setOnMouseDragged(drawableIO.getOnMouseDragged());
			drawableIO.setOnMouseReleased(e -> {
				if (e.getButton() == MouseButton.PRIMARY) {
					if (DiagramDrawer.this.tempLink != null) {
						Object element = null;
						if (this.pickIO != null)
							pickIOFound: for (int i = 0; i < 3; i++) {
								Collection<? extends DrawableIOComponent<? extends Shape>> l;
								if (i == 0)
									l = DiagramDrawer.this.drawingBlocks.values();
								else if (i == 1)
									l = DiagramDrawer.this.drawingInput.values();
								else
									l = DiagramDrawer.this.drawingOutput.values();
								for (DrawableIOComponent<? extends Shape> db : l) {
									HashMap<IO, DrawableIOComponent<? extends Shape>.DrawingIo> dio = db.drawingIOs;
									for (IO io : dio.keySet())
										if (this.pickIO == dio.get(io).circle) {
											element = io;
											break pickIOFound;
										}
								}
							}
						if (DiagramDrawer.this.tempLink.getInput() == null && element instanceof Input) {
							if (isAssignable((Input) element, DiagramDrawer.this.tempLink.getOutput()))
								DiagramDrawer.this.tempLink = new Link((Input) element, DiagramDrawer.this.tempLink.getOutput());
						} else if (DiagramDrawer.this.tempLink.getOutput() == null && element instanceof Output) {
							Output output = (Output) element;
							if (isAssignable(DiagramDrawer.this.tempLink.getInput(), output))
								DiagramDrawer.this.tempLink = new Link(DiagramDrawer.this.tempLink.getInput(), output);
						}
						if (DiagramDrawer.this.tempLink.getInput() != null && DiagramDrawer.this.tempLink.getOutput() != null)
							DiagramDrawer.this.tempLink.getInput().setLink(DiagramDrawer.this.tempLink);
						clearTempLink();
					}
					e.consume();
				}
			});
			drawableIOName.setOnMouseReleased(drawableIO.getOnMouseReleased());
			drawableIO.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY) {
					Stage dia = DiagramDrawer.this.visibleProperties.get(ioDesc.io);
					if (dia != null) {
						dia.requestFocus();
						return;
					}
					Stage dialog = new Stage(StageStyle.DECORATED);
					dialog.initOwner(getScene().getWindow());
					dialog.setTitle(ioDesc.io.getName());
					final BeanManager bm = new BeanManager(ioDesc.io, BeanManager.defaultDir);
					GridPane editor = bm.getEditor();
					editor.widthProperty().addListener(e1 -> dialog.sizeToScene());
					editor.heightProperty().addListener(e1 -> dialog.sizeToScene());
					BorderPane bp = new BorderPane(bm.getEditor());
					Button okButton = new Button("Ok");
					BorderPane.setAlignment(okButton, Pos.CENTER);
					okButton.setOnAction(e1 -> {
						bm.saveIfChanged();
						DiagramDrawer.this.visibleProperties.remove(ioDesc.io);
						dialog.close();
					});
					bp.setBottom(okButton);
					dialog.setScene(new Scene(bp, -1, -1));
					dialog.sizeToScene();
					dialog.setOnCloseRequest(e1 -> {
						bm.saveIfChanged();
						DiagramDrawer.this.visibleProperties.remove(ioDesc.io);
					});
					dialog.show();
					DiagramDrawer.this.visibleProperties.put(ioDesc.io, dialog);
					// bm.addSizeChangeListener(() -> dialog.sizeToScene());
				}
				e.consume();
			});
			drawableIOName.setOnMouseClicked(drawableIO.getOnMouseClicked());
		}

		private void updateTempLinkWithMousePos(Point2D mousePosInTheaterPane) {
			Object io = DiagramDrawer.this.tempLink.getInput() != null ? DiagramDrawer.this.tempLink.getInput() : DiagramDrawer.this.tempLink.getOutput();
			this.pickIO = null;
			Point2D pickPos = null;
			pickIOFound: for (int i = 0; i < 3; i++) {
				Collection<? extends DrawableIOComponent<?>> l;
				if (i == 0)
					l = DiagramDrawer.this.drawingBlocks.values();
				else if (i == 1)
					l = DiagramDrawer.this.drawingInput.values();
				else
					l = DiagramDrawer.this.drawingOutput.values();
				for (DrawableIOComponent<?> db : l)
					for (IO oio : db.drawingIOs.keySet()) {
						DrawableIOComponent<?>.DrawingIo di = db.drawingIOs.get(oio);
						Circle circle = di.circle;
						Point2D pCircle = db.localToParent(circle.getCenterX(), circle.getCenterY());
						Text text = di.text;
						Point2D ori = db.localToParent(text.getX() + text.getTranslateX(), text.getY() + text.getTranslateY());
						if (pCircle.distance(mousePosInTheaterPane) < circle.getRadius()
								|| new Rectangle2D(ori.getX(), ori.getY() - text.getLayoutBounds().getHeight(), text.getLayoutBounds().getWidth(), text.getLayoutBounds().getHeight())
										.contains(mousePosInTheaterPane)) {
							if (circle.getStroke() != Color.BLACK) {
								this.pickIO = circle;
								pickPos = pCircle;
							}
							break pickIOFound;
						}
					}
			}
			double x;
			double y;
			if (pickPos != null) {
				this.tempLinkCurve.setStroke(link);
				x = pickPos.getX();
				y = pickPos.getY();
			} else {
				this.tempLinkCurve.setStroke(DiagramDrawer.this.selectionColor);
				x = mousePosInTheaterPane.getX();
				y = mousePosInTheaterPane.getY();
			}
			if (io instanceof Input) {
				this.tempLinkCurve.setEndX(x);
				this.tempLinkCurve.setEndY(y);
			} else {
				this.tempLinkCurve.setStartX(x);
				this.tempLinkCurve.setStartY(y);
			}
			updateCurveControlPoint(this.tempLinkCurve);
		}

		protected abstract LocalisedObject getSeletecion();

		protected void initBlockCore(IoDescriptor[] ioDescriptors) {
			updateBlockCoreSize();
			this.core.setStrokeType(StrokeType.OUTSIDE);
			updatePosition();
			// On met les IO
			populateIO(ioDescriptors);
			if (DiagramDrawer.this.filterShadow)
				this.core.setEffect(new DropShadow(DiagramDrawer.this.shadowRadius, DiagramDrawer.this.shadow));
			InvalidationListener drawingLinksPositionUpdate = e -> updateDrawingLinksPosition();
			translateXProperty().addListener(drawingLinksPositionUpdate);
			translateYProperty().addListener(drawingLinksPositionUpdate);
			resetCoreStroke();

			this.core.setOnMousePressed(e -> {
				if (e.isConsumed())
					return;
				if (e.getButton() == MouseButton.PRIMARY) {
					if (this.core.getStroke() != DiagramDrawer.this.selectionColor)
						this.core.setStroke(DiagramDrawer.this.selectionColor);
					DiagramDrawer.this.selection = getSeletecion();
				}
			});
		}

		protected abstract void initIOPos(IoDescriptor[] ioDescriptors); // dépend de la géométrie

		public abstract void populateIO(IoDescriptor[] ioDescriptors); // Of course

		public abstract void resetCoreStroke();

		public abstract void setIOPos(IO io, Circle drawingIO);

		public void setPosition(IO io, CubicCurve cubic) {
			Circle circle = null;
			try {
				circle = this.drawingIOs.get(io).circle;
			} catch (NullPointerException e) {
				System.err.println("Cannot get drawingIO from: " + io);
				return;
			}
			if (io instanceof Input) {
				Point2D newPos = this.localToParent(new Point2D(circle.getCenterX(), circle.getCenterY()));
				cubic.setStartX((int) newPos.getX());
				cubic.setStartY((int) newPos.getY());
			} else {
				Point2D newPos = this.localToParent(new Point2D(circle.getCenterX(), circle.getCenterY()));
				cubic.setEndX((int) newPos.getX());
				cubic.setEndY((int) newPos.getY());
			}
		}

		public Runnable setScale(double scale) {
			this.scale = scale;
			updateBlockCoreSize();
			updateIOLook();
			updatePosition();
			if (this.tempLinkCurve != null && DiagramDrawer.this.tempLink != null)
				return () -> {
					IO io = DiagramDrawer.this.tempLink.getInput() != null ? DiagramDrawer.this.tempLink.getInput() : DiagramDrawer.this.tempLink.getOutput();
					setPosition(io, this.tempLinkCurve);
					updateTempLinkWithMousePos(localToParent(sceneToLocal(this.mousePointInScene)));
				};
			return null;
		}

		public void resetStroke(IO io) {
			this.drawingIOs.get(io).circle.setStroke(io.getError() == null ? DiagramDrawer.this.componentBorder : Color.RED);
		}

		protected abstract void updateBlockCoreSize();

		protected abstract void updateDrawingLinksPosition(); // dépend de la config IO

		private void updateIO(IO io) {
			Circle drawingIO = this.drawingIOs.get(io).circle;
			setIOPos(io, drawingIO);
			drawingIO.setRadius((int) (DiagramDrawer.this.ioRoundRadius * this.scale + 0.5));
		}

		protected void updateIOLook() { // Pas Thread safe, il faut le link
			for (IO io : this.drawingIOs.keySet()) {
				updateIO(io);
				updateIONameStyle(io);
			}
			updateDrawingLinksPosition();
		}

		protected abstract void updateIONameStyle(IO io); // dépend de la géométrie

		public void updateIOStroke() {
			Output output = DiagramDrawer.this.tempLink == null ? null : DiagramDrawer.this.tempLink.getOutput();
			Input input = DiagramDrawer.this.tempLink == null ? null : DiagramDrawer.this.tempLink.getInput();
			for (IO io : this.drawingIOs.keySet()) {
				Input in;
				Output out;
				boolean oio;
				if (io instanceof Input) {
					in = (Input) io;
					out = output;
					oio = output == null;
				} else {
					in = input;
					out = (Output) io;
					oio = input == null;
				}
				this.drawingIOs.get(io).circle.setStroke(DiagramDrawer.this.selectedElements.contains(io) ? DiagramDrawer.this.selectionColor
						: DiagramDrawer.this.tempLink == null ? DiagramDrawer.this.componentBorder : oio ? Color.BLACK : isAssignable(in, out) ? Color.WHITE : Color.BLACK);
			}
		}

		public void updateIOStruct(IoDescriptor[] ioDescriptors) {
			for (DrawableIOComponent<? extends Shape>.DrawingIo di : this.drawingIOs.values()) {
				getChildren().remove(di.circle);
				getChildren().remove(di.text);
			}
			this.drawingIOs.clear();
			populateIO(ioDescriptors);
			for (DrawableIOComponent<? extends Shape>.DrawingIo di : this.drawingIOs.values()) {
				getChildren().add(di.circle);
				getChildren().add(di.text);
			}
			updateDrawingLinksPosition();
		}

		public abstract void updatePosition();
	}

	class DrawableBlock extends DrawableIOComponent<Rectangle> {
		private Block block;
		private Region oldOperatorRegion = null;
		private Region operatorRegion = null;
		private Text blockName;
		private VarArgsInputChangeListener varArgsInputChangeListener;
		private RunningStateChangeListener stateChangeChangeListener;
		private PropertyChangeListener propertyChangeListener;
		private ContextMenu contextMenu;
		private Tooltip toolTip;
		private boolean isDefaulting;
		private boolean isWarning;
	
		// To watch -> warning info rectangle, input, outputs, name, defaulting, enable, processMode
		// Changer updateDrawingLinksPosition pour prendre autre chose
		public DrawableBlock(Block block, double scale, IoDescriptor[] ioDescriptors) {
			super(scale);
			this.block = block;
			// Mise en place du core
			this.core = new Rectangle();
			this.core.setViewOrder(0);
	
			// Mise en place du ToolTip
	
			String toolTipText;
			Object operator = block.operator;
			if (operator == null)
				toolTipText = null;
			else {
				toolTipText = new String("Name: " + block.getName() + "\nType: " + operator.getClass().getSimpleName() + "\nPath: " + operator.getClass());
				for (Annotation anno : operator.getClass().getAnnotations())
					if (anno instanceof BlockInfo) {
						toolTipText = toolTipText + "\nAuthor(s): " + ((BlockInfo) anno).author() + "\n" + ((BlockInfo) anno).info();
						break;
					}
				String warning = block.getWarning();
				if (warning != null)
					toolTipText += "\nWarning: " + warning;
				this.toolTip = new Tooltip(toolTipText); // Dépend de warning et de name
				this.toolTip.setShowDelay(Duration.seconds(1));
				this.toolTip.setShowDuration(Duration.minutes(1));
				if (warning != null)
					this.toolTip.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/dialog-warning.png"))));
				Tooltip.install(this.core, this.toolTip);
			}
			this.core.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop[] { new Stop(0, DiagramDrawer.this.component), new Stop(0.25, DiagramDrawer.this.component),
					new Stop(0.5, DiagramDrawer.this.component), new Stop(1, DiagramDrawer.this.component) }));
			initBlockCore(ioDescriptors);
			this.core.setOnMouseDragged(e -> e.consume()); // éviter dragg clic gauche
			this.core.setOnContextMenuRequested(e -> {
				if (DiagramDrawer.this.tempLink == null)
					if (this.contextMenu != null && this.contextMenu.isShowing()) {
						this.contextMenu.setAnchorX(e.getScreenX());
						this.contextMenu.setAnchorY(e.getScreenY());
					} else {
						boolean blockDisable = !block.isEnable();
						CheckMenuItem menuDisable = new CheckMenuItem("Disable");
						menuDisable.setSelected(blockDisable);
						menuDisable.setAccelerator(new KeyCodeCombination(KeyCode.D));
						menuDisable.setOnAction(e1 -> block.setEnable(!block.isEnable()));
						CheckMenuItem menuIsolate = new CheckMenuItem("Isolate in separated process");
						menuIsolate.setSelected(block.getProcessMode() == ProcessMode.ISOLATED);
						menuIsolate.setAccelerator(new KeyCodeCombination(KeyCode.I));
						menuIsolate.setDisable(blockDisable);
						menuIsolate.setOnAction(e1 -> block.setProcessMode(menuIsolate.isSelected() ? ProcessMode.ISOLATED : ProcessMode.LOCAL));
						CheckMenuItem menuRemote = new CheckMenuItem("Run in an remote machine");
						menuRemote.setSelected(block.getProcessMode() == ProcessMode.REMOTE);
						menuRemote.setAccelerator(new KeyCodeCombination(KeyCode.R));
						menuRemote.setDisable(blockDisable);
						menuRemote.setOnAction(e1 -> block.setProcessMode(menuRemote.isSelected() ? ProcessMode.REMOTE : ProcessMode.LOCAL));
						CheckMenuItem menuProperties = new CheckMenuItem("Properties");
						menuProperties.setAccelerator(new KeyCodeCombination(KeyCode.P));
						menuProperties.setOnAction(e1 -> showBlockProperties(block));
						this.contextMenu = new ContextMenu(menuDisable, menuIsolate, menuRemote, menuProperties);
						this.contextMenu.show(this, e.getScreenX(), e.getScreenY());
					}
				e.consume();
			});
	
			// Entete du core
			this.blockName = new Text();
			this.blockName.setViewOrder(-1);
			updateBlockName(block.getName());
			this.blockName.setVisible(DiagramDrawer.this.filterBlockName);
			this.blockName.setOnMousePressed(this.core.getOnMousePressed());
			if (block.operator instanceof LocalScenario) {
				addEventFilter(DragEvent.DRAG_OVER, e -> {
					Dragboard db = e.getDragboard();
					if (db.hasFiles() && ScenarioManager.getScenarioType(db.getFiles().get(0)) != null) {
						if (!DiagramDrawer.this.selectedElements.contains(block) || DiagramDrawer.this.selectedElements.size() != 1) {
							clearSelectedElements();
							DiagramDrawer.this.selectedElements.add(block);
							DiagramDrawer.this.drawingBlocks.get(block).core.setStroke(DiagramDrawer.this.selectionColor);
							e.acceptTransferModes(TransferMode.MOVE);
							e.consume();
						}
					} else if (DiagramDrawer.this.selectedElements.size() != 0)
						clearSelectedElements();
				});
				addEventHandler(DragEvent.DRAG_DROPPED, e -> {
					Dragboard db = e.getDragboard();
					if (db.hasFiles()) {
						File scenario = db.getFiles().get(0);
						Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(scenario);
						if (scenarioType != null) {
							e.consume();
							if (!scenarioType.equals(block.operator.getClass())) {
								showMessage("The scenario is not fit for the " + block.operator.getClass().getSimpleName() + " player", true);
								return;
							}
							new Thread(new Task<>() {
								@Override
								protected Boolean call() throws Exception {
									DiagramDrawer.this.getScheduler().stop(); // Opération potentiellement bloquante...
									LocalScenario localScenario = (LocalScenario) block.operator;
									// localScenario.synchroClose(); //Pas besoin, le setFile le fait
									// Platform.runLater(() -> localScenario.setFile(scenario)); //Pb de synchro ici
									localScenario.setFile(scenario);
									new BeanManager(localScenario, RenderPane.DRAWER_PROPERTIES_DIR).save(false);
									try {
										localScenario.birth();
										block.setError(null);
									} catch (Exception ex) {
										block.setException(ex);
										showMessage(ScenarioManager.getErrorMessage(ex), true);
									}
									return true;
								}
							}).start();
							e.setDropCompleted(true);
						}
						e.consume();
					}
				});
			}
			this.blockName.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY) {
					getChildren().remove(this.blockName);
					TextField tf = new TextField(this.blockName.getText());
					tf.setFont(this.blockName.getFont());
					tf.setAlignment(Pos.CENTER);
					tf.setBackground(new Background(new BackgroundFill(null, null, null)));
					tf.setStyle("-fx-text-fill: white;");
					tf.setPadding(new Insets(0));
					int halfArcLenghtRect = (int) (DiagramDrawer.this.componentCornerRadius * scale + 0.5);
					tf.setPrefWidth((int) this.core.getWidth() - halfArcLenghtRect / 2);
					tf.focusedProperty().addListener((ov, oldValue, newValue) -> {
						if (newValue)
							tf.selectAll();
						else {
							try {
								BeanEditor.renameBean(BeanEditor.getBeanDesc(block.getOperator()), tf.getText());
							} catch (IllegalArgumentException ex) {
								System.err.println(ex.getMessage());
							}
							getChildren().remove(tf);
							getChildren().add(this.blockName);
						}
					});
					tf.setOnKeyPressed(e1 -> {
						if (e1.getCode() == KeyCode.ENTER)
							FxUtils.traverse(); // RT-21596
					});
					tf.setTranslateX(halfArcLenghtRect / 4);
					tf.setTranslateY(this.blockName.getTranslateY() - this.blockName.getLayoutBounds().getHeight() + 1);
					getChildren().add(tf);
					tf.requestFocus();
				}
				e.consume();
			});
	
			this.core.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY)
					showBlockProperties(block);
				if (this.contextMenu != null && e.getButton() == MouseButton.PRIMARY) {
					this.contextMenu.hide();
					this.contextMenu = null;
				}
				e.consume();
			});
			getChildren().addAll(this.core, this.blockName);
			for (DrawableIOComponent<? extends Shape>.DrawingIo di : this.drawingIOs.values()) {
				getChildren().add(di.circle);
				getChildren().add(di.text);
			}
			if (this.operatorRegion != null)
				this.operatorRegion.toFront();
			layoutChildren();
			this.varArgsInputChangeListener = (indexOfInput, type) -> {
				if (type != VarArgsInputChangeListener.CHANGED) {
					IoDescriptor[] newIoDescriptors = getIODescriptors(block);
					Platform.runLater(() -> updateIOStruct(newIoDescriptors));
				}
			};
			block.addVarArgsInputChangeListener(this.varArgsInputChangeListener);
			this.stateChangeChangeListener = () -> {
				String warning = block.getWarning();
				String error = block.getError();
				Platform.runLater(() -> {
					this.isWarning = warning != null;
					this.isDefaulting = error != null;
					if (this.toolTip != null) {
						String newToolTipText = this.toolTip.getText();
						int warningIndex = newToolTipText.indexOf("\nWarning: ");
						if (warningIndex != -1)
							newToolTipText = newToolTipText.substring(0, warningIndex);
						if (warning != null)
							newToolTipText += "\nWarning: " + warning;
						int errorIndex = newToolTipText.indexOf("\nError: ");
						if (errorIndex != -1)
							newToolTipText = newToolTipText.substring(0, errorIndex);
						if (error != null)
							newToolTipText += "\nError: " + error;
						this.toolTip.setText(newToolTipText);
					}
					resetCoreStroke();
				});
			};
			block.addRunningStateChangeListener(this.stateChangeChangeListener);
			this.propertyChangeListener = (evt) -> {
				if (evt.getPropertyName().equals("enable") || evt.getPropertyName().equals("processMode")) {
					boolean enable = block.isEnable();
					ProcessMode processMode = block.getProcessMode();
					Platform.runLater(() -> updateEffect(enable, processMode));
				}
			};
			block.addPropertyChangeListener(this.propertyChangeListener);
			updateEffect(block.isEnable(), block.getProcessMode());
			DiagramDrawer.this.drawingBlocks.put(block, this);
			setVisible(DiagramDrawer.this.filterBlock);
		}
	
		public void delete() {
			this.block.removeVarArgsInputChangeListener(this.varArgsInputChangeListener);
			this.block.removeRunningStateChangeListener(this.stateChangeChangeListener);
			this.block.removePropertyChangeListener(this.propertyChangeListener);
		}
	
		private double[] getFontSize(double scale, String bName, int maxWidthAvailable, int maxHeightAvailable, int minFontSize, int maxFontSize) {
			Text t = new Text(bName);
			double sMaxFontSize = maxFontSize;
			t.setFont(new Font(Math.max(sMaxFontSize, minFontSize)));
			Bounds b = t.getLayoutBounds();
			double tw = b.getWidth();
			double th = b.getHeight();
			while ((tw > maxWidthAvailable || th > maxHeightAvailable) && sMaxFontSize > minFontSize) {
				sMaxFontSize--;
				t.setFont(new Font(sMaxFontSize));
				b = t.getLayoutBounds();
				tw = b.getWidth();
				th = b.getHeight();
			}
			return new double[] { tw, sMaxFontSize };
		}
	
		protected float getGapBetweenIO() {
			return (float) ((this.block.getRectangle().getHeight() - DiagramDrawer.this.headerSize - DiagramDrawer.this.componentCornerRadius * 2)
					/ ((double) this.block.getNbInput() + this.block.getNbOutput()));
		}
	
		@Override
		protected LocalisedObject getSeletecion() {
			return this.block;
		}
	
		@Override
		protected void initIOPos(IoDescriptor[] ioDescriptors) { // Utilise rectangle Ok inputs et outputs pas ok, set position dans inputs, outputs...
			int arcLenght = DiagramDrawer.this.componentCornerRadius * 2;
			int headerWithArcSize = (int) (arcLenght / 2.0f + DiagramDrawer.this.headerSize + 0.5);
			Rectangle2D rectangle = this.block.getRectangle();
			float gap = getGapBetweenIO();
			int nbInput = 0;
			for (; nbInput < ioDescriptors.length; nbInput++)
				if (!(ioDescriptors[nbInput].io instanceof Input))
					break;
			for (int j = 0; j < nbInput; j++)
				ioDescriptors[j].io.setPosition(new Point2D(0, headerWithArcSize + j * gap + gap / 2.0));
			for (int j = nbInput; j < ioDescriptors.length; j++)
				ioDescriptors[j].io.setPosition(new Point2D(rectangle.getWidth(), headerWithArcSize + j * gap + gap / 2.0));
		}
	
		@Override
		public void populateIO(IoDescriptor[] ioDescriptors) {
			initIOPos(ioDescriptors);
			for (IoDescriptor ioDescriptor : ioDescriptors)
				createIO(ioDescriptor);
		}
	
		@Override
		public void resetCoreStroke() {
			this.core.setStroke(this.isDefaulting ? Color.RED : this.isWarning ? Color.YELLOW : DiagramDrawer.this.componentBorder);
		}
	
		@Override
		public void setIOPos(IO io, Circle drawingIO) { // Utilise position de io...
			Point2D pos = io.getPosition();
			if (pos != null) { // Peut être null si changement asynchone
				drawingIO.setCenterX((int) (pos.getX() * this.scale + 0.5));
				drawingIO.setCenterY((int) (pos.getY() * this.scale + 0.5));
			}
		}
	
		@Override
		public Runnable setScale(double scale) {
			Runnable task = super.setScale(scale);
			updateBlockNameStyle();
			return task;
		}
	
		@Override
		protected void updateBlockCoreSize() { // Utilise bloc rectangle Ok processmode, pas ok
			Rectangle2D rectangle = this.block.getRectangle();
			this.core.setWidth(rectangle.getWidth() * this.scale);
			int arcLenght = DiagramDrawer.this.componentCornerRadius * 2;
			int headerWithArcSize = (int) (arcLenght / 2.0f + DiagramDrawer.this.headerSize + 0.5);
			double headRatioPos = headerWithArcSize / rectangle.getHeight();
			LinearGradient lg = (LinearGradient) this.core.getFill();
			List<Stop> stops = lg.getStops();
			this.core.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop[] { new Stop(0, stops.get(0).getColor()), new Stop(headRatioPos, stops.get(1).getColor()),
					new Stop(headRatioPos, stops.get(2).getColor()), new Stop(1, stops.get(3).getColor()) }));
			this.core.setHeight(rectangle.getHeight() * this.scale);
			this.core.setArcHeight((int) (arcLenght * this.scale + 0.5));
			this.core.setArcWidth((int) (arcLenght * this.scale + 0.5));
			this.operatorRegion = this.block.operator instanceof EvolvedOperator ? ((EvolvedOperator) this.block.operator).getNode() : null;
			if (this.operatorRegion != null) {
				this.operatorRegion.setOnMouseClicked(this.core.getOnMouseClicked());
				this.operatorRegion.setOnMousePressed(this.core.getOnMousePressed());
				this.operatorRegion.setPrefSize(rectangle.getWidth() * this.scale, (rectangle.getHeight() - headerWithArcSize) * this.scale);
				this.operatorRegion.setMaxSize(rectangle.getWidth() * this.scale, (rectangle.getHeight() - headerWithArcSize) * this.scale);
				this.operatorRegion.setMinSize(rectangle.getWidth() * this.scale, (rectangle.getHeight() - headerWithArcSize) * this.scale);
				this.operatorRegion.setTranslateY(headerWithArcSize * this.scale);
			}
			if (this.operatorRegion != this.oldOperatorRegion) {
				if (this.oldOperatorRegion != null)
					getChildren().remove(this.oldOperatorRegion);
				if (this.operatorRegion != null) {
					this.operatorRegion.setViewOrder(-2);
					getChildren().add(this.operatorRegion);
				}
			}
			this.oldOperatorRegion = this.operatorRegion;
		}
	
		public void updateBlockName(String name) {
			this.blockName.setText(name);
			String toolTipText = this.toolTip.getText();
			this.toolTip.setText("Name: " + name + toolTipText.substring(toolTipText.indexOf("\nType: ")));
			updateBlockNameStyle();
		}
	
		private void updateBlockNameStyle() {
			int halfArcLenghtRect = (int) (DiagramDrawer.this.componentCornerRadius * this.scale + 0.5);
			int maxWidthAvailable = (int) this.core.getWidth() - halfArcLenghtRect / 2;
			int arcLenght = DiagramDrawer.this.componentCornerRadius * 2;
			int maxHeightAvailable = (int) ((arcLenght / 2.0f + DiagramDrawer.this.headerSize) * this.scale);
			double[] textInfo = getFontSize(1, this.blockName.getText(), maxWidthAvailable, maxHeightAvailable, 9, 14);
			this.blockName.setFont(new Font(textInfo[1]));
			Bounds textBounds = this.blockName.getLayoutBounds();
			if (textBounds.getWidth() < maxWidthAvailable) {
				this.blockName.setTranslateX(this.core.getWidth() / 2 - textBounds.getWidth() / 2);
				this.blockName.setFill(componentText);
			} else {
				this.blockName.setTranslateX(halfArcLenghtRect / 4);
				// blockName.setClip(new Rectangle(0, -textBounds.getHeight(), maxWidthAvailable, textBounds.getHeight())); //Il sert a quoi, bug clip scale quand il est la
				double end = maxWidthAvailable / textBounds.getWidth();
				this.blockName.setFill(
						new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, new Stop[] { new Stop(0, componentText), new Stop(0.9 * end, componentText), new Stop(1 * end, Color.TRANSPARENT) }));
			}
			this.blockName.setTranslateY(textBounds.getHeight() / 2 + (halfArcLenghtRect / 2 + DiagramDrawer.this.headerSize * this.scale) / 2);// -textBounds.getHeight() / 2 + (halfArcLenghtRect +
																																				// headerSize * scale) / 2
		}
	
		public void updateDrawableWithStopWarning(boolean phase, boolean interruptedMode) {
			this.core.setStroke(phase ? interruptedMode ? Color.RED : Color.YELLOW : DiagramDrawer.this.componentBorder);
		}
	
		@Override
		protected void updateDrawingLinksPosition() {
			Set<LinkDescriptor> linksDesc = DiagramDrawer.this.drawingLinks.keySet();
			this.drawingIOs.keySet().stream().filter(e -> e instanceof Input).forEach(input -> {
				for (LinkDescriptor linkDesc : linksDesc)
					if (linkDesc.input.io == input)
						setPosition(input, DiagramDrawer.this.drawingLinks.get(linkDesc));
			});
			this.drawingIOs.keySet().stream().filter(e -> e instanceof Output).forEach(output -> {
				for (LinkDescriptor linkDesc : linksDesc)
					if (linkDesc.output.io == output)
						setPosition(output, DiagramDrawer.this.drawingLinks.get(linkDesc));
			});
		}
	
		private void updateEffect(boolean enable, ProcessMode processMode) {
			setEffect(enable ? null : new ColorAdjust(0, 0, 0, -0.8));
			double headRatioPos = (int) (DiagramDrawer.this.componentCornerRadius * 2 / 2.0f + DiagramDrawer.this.headerSize + 0.5) / this.block.getRectangle().getHeight();
			Color headerColor = processMode == ProcessMode.LOCAL ? DiagramDrawer.this.componentHeaderLocal
					: processMode == ProcessMode.REMOTE ? DiagramDrawer.this.componentHeaderIsolated : DiagramDrawer.this.componentHeaderRemote;
			this.core.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
					new Stop[] { new Stop(0, headerColor), new Stop(headRatioPos, headerColor), new Stop(headRatioPos, DiagramDrawer.this.component), new Stop(1, DiagramDrawer.this.component) }));
		}
	
		@Override
		protected void updateIONameStyle(IO io) {
			DrawableIOComponent<? extends Shape>.DrawingIo di = this.drawingIOs.get(io);
			Text drawingIOName = di.text;
			Circle drawingIO = di.circle;
			int marge = (int) (3 * this.scale + 0.5);
			int maxWidthAvailable = (int) (this.core.getWidth() - DiagramDrawer.this.ioRoundRadius * this.scale - marge);
			drawingIOName.setFont(new Font(getFontSize(this.scale, drawingIOName.getText(), maxWidthAvailable, (int) (getGapBetweenIO() * this.scale), 8, 12)[1]));
			Bounds textBounds = drawingIOName.getLayoutBounds();
			if (io instanceof Input)
				drawingIOName.setTranslateX(drawingIO.getCenterX() + drawingIO.getRadius() + marge);
			else
				drawingIOName.setTranslateX(
						drawingIO.getCenterX() + (textBounds.getWidth() <= maxWidthAvailable ? -(int) (textBounds.getWidth() + 0.5) - drawingIO.getRadius() - marge : marge - this.core.getWidth()));
			if (textBounds.getWidth() > maxWidthAvailable) {
				drawingIOName.setClip(new Rectangle(0, -textBounds.getHeight(), maxWidthAvailable, textBounds.getHeight()));
				double end = maxWidthAvailable / textBounds.getWidth();
				drawingIOName.setFill(
						new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, new Stop[] { new Stop(0, DiagramDrawer.this.ioTextColor), new Stop(0.9 * end, DiagramDrawer.this.ioTextColor), new Stop(1 * end, Color.TRANSPARENT) }));
			} else {
				drawingIOName.setClip(null);
				drawingIOName.setFill(DiagramDrawer.this.ioTextColor);
			}
			drawingIOName.setTranslateY((int) (drawingIO.getCenterY() + drawingIOName.getBaselineOffset() / 2));
		}
	
		@Override
		public void updateIOStruct(IoDescriptor[] ioDescriptors) {
			updateBlockCoreSize();
			super.updateIOStruct(ioDescriptors);
		}
	
		@Override
		public void updatePosition() {
			Rectangle2D rectangle = this.block.getRectangle();
			setTranslateX((int) (rectangle.getMinX() * this.scale + 0.5));
			setTranslateY((int) (rectangle.getMinY() * this.scale + 0.5));
			if (DiagramDrawer.this.canvasInfo != null)
				DiagramDrawer.this.canvasInfo.repaint();
		}
	
		@Override
		public String toString() {
			return getClass().getSimpleName() + " of " + this.block;
		}
	}

	abstract class DrawableDiagramIO<T extends IO> extends DrawableIOComponent<Polygon> implements LocalisedObject {
		protected final T io;
	
		public DrawableDiagramIO(T io, double scale, IoDescriptor[] ioDescriptors) {
			super(scale);
			this.io = io;
			this.core = new Polygon();
			updateBlockCoreSize();
			this.core.setFill(DiagramDrawer.this.componentStaticIO);
			initBlockCore(ioDescriptors);
			this.core.setOnMouseClicked(e -> e.consume());
			getChildren().add(this.core);
			for (DrawableIOComponent<? extends Shape>.DrawingIo di : this.drawingIOs.values()) {
				getChildren().add(di.circle);
				getChildren().add(di.text);
			}
		}
	
		@Override
		public Point2D getPosition() {
			return this.io.getPosition();
		}
	
		@Override
		protected LocalisedObject getSeletecion() {
			return this;
		}
	
		@Override
		protected void initIOPos(IoDescriptor[] ioDescriptors) {}
	
		protected void nameChange(IO io) {
			this.drawingIOs.get(io).text.setText(io.getName());
			updateIOInfo();
		}
	
		@Override
		public void populateIO(IoDescriptor[] ioDescriptors) {
			initIOPos(ioDescriptors);
			createIO(ioDescriptors[0]);
			Text ioText = this.drawingIOs.get(this.io).text;
			ioText.setMouseTransparent(false);
			ioText.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY) {
					Text text = this.drawingIOs.get(this.io).text;
					getChildren().remove(text);
					TextField tf = new TextField(this.io.getName());
					tf.setFont(text.getFont());
					tf.setBackground(new Background(new BackgroundFill(null, null, null)));
					tf.setStyle("-fx-text-fill: black;");
					tf.setPadding(new Insets(0));
					tf.focusedProperty().addListener((ov, oldValue, newValue) -> {
						if (newValue)
							tf.selectAll();
						else {
							getChildren().remove(tf);
							String newName = tf.getText();
							FlowDiagram fd = (FlowDiagram) getDrawableElement();
							boolean isValid = true;
							for (FlowDiagramInput fdInput : fd.getInputs()) {
								if (fdInput.getName().equals(newName))
									isValid = false;
								break;
							}
							if (isValid)
								for (FlowDiagramOutput fdOutput : fd.getOutputs())
									if (fdOutput.getName().equals(newName)) {
										isValid = false;
										break;
									}
							if (isValid)
								this.io.setName(newName);
							else
								showMessage("The name: " + newName + " is already used by an other input or output", true);
							getChildren().add(text);
						}
					});
					tf.setOnKeyPressed(e1 -> {
						if (e1.getCode() == KeyCode.ENTER)
							FxUtils.traverse();
					});
					tf.setTranslateX(text.getTranslateX());
					tf.setTranslateY(text.getTranslateY() - text.getLayoutBounds().getHeight() + 1);
					getChildren().add(tf);
					tf.requestFocus();
				}
			});
		}
	
		@Override
		public void resetCoreStroke() {
			this.core.setStroke(DiagramDrawer.this.componentBorder);
		}
	
		@Override
		public void setIOPos(IO io, Circle drawingIO) {
			drawingIO.setCenterX(0);
			drawingIO.setCenterY(0);
		}
	
		@Override
		public void setPosition(double x, double y) {
			this.io.setPosition(new Point2D(x, y));
		}
	
		public void updateDrawableStrokeWithMissedData() {
			this.core.setStroke(Color.BLACK);
		}
	
		public void updateIOInfo() {
			Tooltip.install(this.drawingIOs.get(this.io).circle, new Tooltip(getIOToolTipInfo(this.io)));
		}
	
		@Override
		protected void updateIONameStyle(IO io) {
			DrawableIOComponent<? extends Shape>.DrawingIo di = this.drawingIOs.get(io);
			Text drawingIOName = di.text;
			Circle drawingIO = di.circle;
			if (io instanceof Input) {
				drawingIOName.setTranslateX(drawingIO.getCenterX() + (DiagramDrawer.this.ioRoundRadius + 1) * this.scale);
				drawingIOName.setTranslateY((int) (drawingIO.getCenterY() + (10 + 1) * this.scale + drawingIOName.getBaselineOffset()));
			} else {
				drawingIOName.setTranslateX(drawingIO.getCenterX() - (DiagramDrawer.this.ioRoundRadius + 1 + 20) * this.scale);
				drawingIOName.setTranslateY((int) (drawingIO.getCenterY() - (10 + 1) * this.scale));
			}
		}
	
		@Override
		public void updatePosition() {
			Point2D pos = this.io.getPosition();
			setTranslateX((int) (pos.getX() * this.scale + 0.5));
			setTranslateY((int) (pos.getY() * this.scale + 0.5));
			if (DiagramDrawer.this.canvasInfo != null)
				DiagramDrawer.this.canvasInfo.repaint();
		}
	}

	class DrawableInput extends DrawableDiagramIO<FlowDiagramInput> implements LocalisedObject {
	
		public DrawableInput(FlowDiagramInput input, double scale, IoDescriptor[] ioDescriptors) {
			super(input, scale, ioDescriptors);
			DiagramDrawer.this.drawingInput.put(input, this);
		}
	
		@Override
		protected void updateBlockCoreSize() {
			this.core.getPoints().setAll((DiagramDrawer.this.ioRoundRadius + 1 + 20) * this.scale, 0.0, (DiagramDrawer.this.ioRoundRadius + 1) * this.scale, -10.0 * this.scale,
					(DiagramDrawer.this.ioRoundRadius + 1) * this.scale, 10.0 * this.scale);
		}
	
		@Override
		protected void updateDrawingLinksPosition() {
			for (LinkDescriptor ld : DiagramDrawer.this.drawingLinks.keySet())
				if (ld.input.io == this.io)
					setPosition(this.io, DiagramDrawer.this.drawingLinks.get(ld));
		}
	}

	class DrawableOutput extends DrawableDiagramIO<FlowDiagramOutput> implements LocalisedObject {

		public DrawableOutput(FlowDiagramOutput output, double scale, IoDescriptor[] ioDescriptors) {
			super(output, scale, ioDescriptors);
			DiagramDrawer.this.drawingOutput.put(output, this);
		}

		@Override
		protected void initIOPos(IoDescriptor[] ioDescriptors) {}

		@Override
		protected void updateBlockCoreSize() {
			this.core.getPoints().setAll(-(DiagramDrawer.this.ioRoundRadius + 1) * this.scale, 0.0, -(DiagramDrawer.this.ioRoundRadius + 20 + 1) * this.scale, -10.0 * this.scale,
					-(DiagramDrawer.this.ioRoundRadius + 20 + 1) * this.scale, 10.0 * this.scale);
		}

		@Override
		protected void updateDrawingLinksPosition() {
			for (LinkDescriptor linkDesc : DiagramDrawer.this.drawingLinks.keySet())
				if (linkDesc.output.io == this.io)
					setPosition(this.io, DiagramDrawer.this.drawingLinks.get(linkDesc));
		}
	}

	class CanvasInfo extends Canvas implements VisuableSchedulable {
		private final HashMap<Link, ArrayList<Tuple<Long, Integer>>> linkConsumeCpt = new HashMap<>();
		private final HashMap<Link, Integer> linkConsume = new HashMap<>();
		private final Text text = new Text();
		private long lastUpdate;
	
		public CanvasInfo() {
			FlowDiagram fd = (FlowDiagram) getDrawableElement();
			if (DiagramDrawer.this.filterHeatLink) {
				this.linkConsume.clear();
				long time = System.currentTimeMillis();
				for (Link link : fd.getAllLinks()) {
					ArrayList<Tuple<Long, Integer>> consumeStack = new ArrayList<>();
					this.linkConsumeCpt.put(link, consumeStack);
					consumeStack.add(new Tuple<>(time, 0));
					this.linkConsume.put(link, 0);
				}
			}
			updateDynamicDiagramInfoRefreshPeriod();
		}
	
		@Override
		public void paint() {
			long now = System.currentTimeMillis();
			if (now - this.lastUpdate >= DiagramDrawer.this.dynamicDiagramInfoRefreshPeriod) {
				FlowDiagram fd = (FlowDiagram) getDrawableElement();
				if (fd == null)
					return;
				GraphicsContext g = getGraphicsContext2D();
				g.setTransform(1, 0, 0, 1, 0, 0);
				g.clearRect(0, 0, getWidth(), getHeight());
				double scale = getScale();
				g.setTransform(1, 0, 0, 1, (int) getxTranslate(), (int) getyTranslate());
				if (DiagramDrawer.this.filterMissedData) {
					g.setFill(Color.RED.darker());
					g.setTextAlign(TextAlignment.RIGHT);
					g.setTextBaseline(VPos.CENTER);
					int scaleIORadius = (int) (DiagramDrawer.this.ioRoundRadius * scale);
					for (Block block : fd.getBlocks()) {
						Rectangle2D rectangle = block.getRectangle();
						double x = rectangle.getMinX();
						double y = rectangle.getMinY();
						for (BlockInput input : block.getInputs())
							if (input.getPosition() != null) {
								Point2D iPos = input.getPosition();
								int xRound = (int) ((x + iPos.getX()) * scale - scaleIORadius);
								int yRound = (int) ((y + iPos.getY()) * scale - scaleIORadius);
								long nbMissedData = input.getBuffer().getNbMissedData();
								if (nbMissedData != 0)
									g.fillText(Long.toString(input.getBuffer().getNbMissedData()), xRound - 4, yRound);
							}
					}
				}
				if (DiagramDrawer.this.filterStackState) {
					int scaleIORadius = (int) (DiagramDrawer.this.ioRoundRadius * scale);
					g.setFill(Color.BLACK);
					for (Block block : fd.getBlocks())
						if (block.isEnable()) {
							Rectangle2D rectangle = block.getRectangle();
							double x = rectangle.getMinX();
							double y = rectangle.getMinY();
							for (BlockInput input : block.getInputs())
								if (input.getPosition() != null) {
									Point2D iPos = input.getPosition();
									int xRound = (int) ((x + iPos.getX()) * scale - scaleIORadius);
									int yRound = (int) ((y + iPos.getY()) * scale - scaleIORadius);
									int stackSize = input.getBuffer().size();
									int xBegin = xRound - 4;
									if (DiagramDrawer.this.filterMissedData) {
										long nbMissedData = input.getBuffer().getNbMissedData();
										if (nbMissedData != 0) {
											this.text.setText(Long.toString(nbMissedData));
											xBegin -= (int) (this.text.getLayoutBounds().getWidth() + 8);
										}
									}
									int yBegin = yRound - 2;
									for (int m = 0; m < stackSize; m++)
										g.fillRect(xBegin - m * 5, yBegin, 4, 4);
								}
						}
				}
				if (DiagramDrawer.this.filterCPUUsage)
					if (fd.getCpuUsage() != -1) {
						Affine oldTransform = g.getTransform();
						g.setTransform(1, 0, 0, 1, 0, 0);
						int cpuWidth = (int) getWidth() / 5;
						int cpux = ((int) getWidth() - cpuWidth) / 2;
						int cpuy = (int) getHeight() - DiagramDrawer.this.cpuBarHeight - DiagramDrawer.this.cpuBarVerticalGap;
						g.setFill(DiagramDrawer.this.cpuBarBackground);
						g.fillRect(cpux, cpuy, cpuWidth, DiagramDrawer.this.cpuBarHeight);
						g.setFill(DiagramDrawer.this.cpuBarForeground);
						g.fillRect(cpux, cpuy, cpuWidth * fd.getCpuUsage(), DiagramDrawer.this.cpuBarHeight);
						g.setTransform(oldTransform);
					} else
						for (Block block : fd.getBlocks())
							if (block.isEnable()) {
								Rectangle2D rectangle = block.getRectangle();
								double x = rectangle.getMinX();
								double y = rectangle.getMinY();
								int xRect = (int) (x * scale);
								int wRect = (int) (rectangle.getWidth() * scale);
								int hRect = (int) (rectangle.getHeight() * scale);
								int yRect = (int) (y * scale) + hRect + 2;
								g.setFill(DiagramDrawer.this.cpuBarBackground);
								g.fillRect(xRect, yRect, wRect, DiagramDrawer.this.cpuBarHeight);
								float cpuUsage = block.getCpuUsage();
								if (cpuUsage < 0)
									cpuUsage = 0;
								g.setFill(DiagramDrawer.this.cpuBarForeground);
								g.fillRect(xRect, yRect, (int) (wRect * cpuUsage), DiagramDrawer.this.cpuBarHeight);
							}
				if (DiagramDrawer.this.filterCount) {
					g.setTextAlign(TextAlignment.CENTER);
					g.setTextBaseline(VPos.CENTER);
					this.text.setText("");
					double textHeight = this.text.getLayoutBounds().getHeight();
					for (Block block : fd.getBlocks())
						if (block.isEnable()) {
							long nbConsume = block.getConsume();
							Rectangle2D rectangle = block.getRectangle();
							double x = rectangle.getMinX();
							double y = rectangle.getMinY();
							int xRect = (int) (x * scale);
							int wRect = (int) (rectangle.getWidth() * scale);
							int hRect = (int) (rectangle.getHeight() * scale);
							int yRect = (int) (y * scale) + hRect + 2;
							if (DiagramDrawer.this.filterCPUUsage && fd.getCpuUsage() == -1)
								yRect += DiagramDrawer.this.cpuBarHeight + 2;
							g.setFill(DiagramDrawer.this.cpuBarBackground);
							g.fillRect(xRect, yRect, wRect, textHeight);
							g.setFill(DiagramDrawer.this.countForeground);
							g.fillText(Long.toString(nbConsume), xRect + wRect / 2, yRect + textHeight / 2);
						}
				}
				if (DiagramDrawer.this.filterHeatLink) {
					this.linkConsume.clear();
					long time = System.currentTimeMillis();
					int maxCons = Integer.MIN_VALUE;
					for (Link link : fd.getAllLinks()) {
						ArrayList<Tuple<Long, Integer>> consumeStack = this.linkConsumeCpt.get(link);
						if (consumeStack == null) {
							consumeStack = new ArrayList<>();
							this.linkConsumeCpt.put(link, consumeStack);
						}
						consumeStack.add(new Tuple<>(time, link.getConsume()));
						for (int k = 0; k < consumeStack.size(); k++)
							if (time > consumeStack.get(k).getFirst() + 1000)
								consumeStack.remove(k--);
						int sum = consumeStack.get(consumeStack.size() - 1).getSecond() - consumeStack.get(0).getSecond();
						this.linkConsume.put(link, sum);
						if (sum > maxCons)
							maxCons = sum;
					}
					float maxConsC = maxCons;
					DiagramDrawer.this.drawingLinks.forEach((ld, cc) -> {
						if (!DiagramDrawer.this.selectedElements.contains(ld)) {
							Integer consume = this.linkConsume.get(ld.link);
							if (consume != null) {
								float cons = consume / maxConsC;
								if (cons < 0)
									cons = 0;
								else if (cons > 1)
									cons = 1;
								cc.setStroke(cons == 0 ? new Color(0, 0, 0, 1) : new Color(cons, 0, 1 - cons, 1));
							}
						}
					});
				}
				this.lastUpdate = now;
			}
		}
	
		public void repaint() {
			this.lastUpdate = 0;
		}
	
		@Override
		public void setAnimated(boolean animated) {}
	}
}

class FlatteningPathIterator {
	public static final int WIND_EVEN_ODD = 0;
	public static final int WIND_NON_ZERO = 1;
	public static final int SEG_MOVETO = 0;
	public static final int SEG_LINETO = 1;
	public static final int SEG_QUADTO = 2;
	public static final int SEG_CUBICTO = 3;
	public static final int SEG_CLOSE = 4;
	static final int GROW_SIZE = 24;

	public static float ptSegDistSq(float x1, float y1, float x2, float y2, float px, float py) {
		x2 -= x1;
		y2 -= y1;
		px -= x1;
		py -= y1;
		float dotprod = px * x2 + py * y2;
		float projlenSq;
		if (dotprod <= 0f)
			projlenSq = 0f;
		else {
			px = x2 - px;
			py = y2 - py;
			dotprod = px * x2 + py * y2;
			if (dotprod <= 0f)
				projlenSq = 0f;
			else
				projlenSq = dotprod * dotprod / (x2 * x2 + y2 * y2);
		}
		float lenSq = px * px + py * py - projlenSq;
		if (lenSq < 0f)
			lenSq = 0f;
		return lenSq;
	}

	private static void subdivide(float[] src, int srcoff, int leftoff) {
		float x1 = src[srcoff];
		float y1 = src[srcoff + 1];
		float ctrlx1 = src[srcoff + 2];
		float ctrly1 = src[srcoff + 3];
		float ctrlx2 = src[srcoff + 4];
		float ctrly2 = src[srcoff + 5];
		float x2 = src[srcoff + 6];
		float y2 = src[srcoff + 7];
		src[leftoff] = x1;
		src[leftoff + 1] = y1;
		src[srcoff + 6] = x2;
		src[srcoff + 7] = y2;
		x1 = (x1 + ctrlx1) / 2f;
		y1 = (y1 + ctrly1) / 2f;
		x2 = (x2 + ctrlx2) / 2f;
		y2 = (y2 + ctrly2) / 2f;
		float centerx = (ctrlx1 + ctrlx2) / 2f;
		float centery = (ctrly1 + ctrly2) / 2f;
		ctrlx1 = (x1 + centerx) / 2f;
		ctrly1 = (y1 + centery) / 2f;
		ctrlx2 = (x2 + centerx) / 2f;
		ctrly2 = (y2 + centery) / 2f;
		centerx = (ctrlx1 + ctrlx2) / 2f;
		centery = (ctrly1 + ctrly2) / 2f;
		src[leftoff + 2] = x1;
		src[leftoff + 3] = y1;
		src[leftoff + 4] = ctrlx1;
		src[leftoff + 5] = ctrly1;
		src[leftoff + 6] = centerx;
		src[leftoff + 7] = centery;
		src[srcoff] = centerx;
		src[srcoff + 1] = centery;
		src[srcoff + 2] = ctrlx2;
		src[srcoff + 3] = ctrly2;
		src[srcoff + 4] = x2;
		src[srcoff + 5] = y2;
	}

	CubicIterator src;
	float squareflat;
	int limit;
	volatile float[] hold = new float[14];
	float curx, cury;
	float movx, movy;
	int holdType;
	int holdEnd;
	int holdIndex;
	int[] levels;

	int levelIndex;

	boolean done;

	public FlatteningPathIterator(CubicIterator src, float flatness) {
		this(src, flatness, 10);
	}

	public FlatteningPathIterator(CubicIterator src, float flatness, int limit) {
		if (flatness < 0f)
			throw new IllegalArgumentException("flatness must be >= 0");
		if (limit < 0)
			throw new IllegalArgumentException("limit must be >= 0");
		this.src = src;
		this.squareflat = flatness * flatness;
		this.limit = limit;
		this.levels = new int[limit + 1];
		// prime the first path segment
		next(false);
	}

	public int currentSegment(float[] coords) {
		if (isDone())
			throw new NoSuchElementException("flattening iterator out of bounds");
		int type = this.holdType;
		if (type != SEG_CLOSE) {
			coords[0] = this.hold[this.holdIndex];
			coords[1] = this.hold[this.holdIndex + 1];
			if (type != SEG_MOVETO)
				type = SEG_LINETO;
		}
		return type;
	}

	void ensureHoldCapacity(int want) {
		if (this.holdIndex - want < 0) {
			int have = this.hold.length - this.holdIndex;
			int newsize = this.hold.length + GROW_SIZE;
			float[] newhold = new float[newsize];
			System.arraycopy(this.hold, this.holdIndex, newhold, this.holdIndex + GROW_SIZE, have);
			this.hold = newhold;
			this.holdIndex += GROW_SIZE;
			this.holdEnd += GROW_SIZE;
		}
	}

	public float getFlatness() {
		return (float) Math.sqrt(this.squareflat);
	}

	private static float getFlatnessSq(float[] coords, int offset) {
		return Math.max(ptSegDistSq(coords[offset], coords[offset + 1], coords[offset + 6], coords[offset + 7], coords[offset + 2], coords[offset + 3]),
				ptSegDistSq(coords[offset], coords[offset + 1], coords[offset + 6], coords[offset + 7], coords[offset + 4], coords[offset + 5]));
	}

	public int getRecursionLimit() {
		return this.limit;
	}

	public int getWindingRule() {
		return this.src.getWindingRule();
	}

	public boolean isDone() {
		return this.done;
	}

	public void next() {
		next(true);
	}

	private void next(boolean doNext) {
		int level;

		if (this.holdIndex >= this.holdEnd) {
			if (doNext)
				this.src.next();
			if (this.src.isDone()) {
				this.done = true;
				return;
			}
			this.holdType = this.src.currentSegment(this.hold);
			this.levelIndex = 0;
			this.levels[0] = 0;
		}

		switch (this.holdType) {
		case SEG_MOVETO:
		case SEG_LINETO:
			this.curx = this.hold[0];
			this.cury = this.hold[1];
			if (this.holdType == SEG_MOVETO) {
				this.movx = this.curx;
				this.movy = this.cury;
			}
			this.holdIndex = 0;
			this.holdEnd = 0;
			break;
		case SEG_CLOSE:
			this.curx = this.movx;
			this.cury = this.movy;
			this.holdIndex = 0;
			this.holdEnd = 0;
			break;
		case SEG_QUADTO:
			if (this.holdIndex >= this.holdEnd) {
				this.holdIndex = this.hold.length - 6;
				this.holdEnd = this.hold.length - 2;
				this.hold[this.holdIndex] = this.curx;
				this.hold[this.holdIndex + 1] = this.cury;
				this.hold[this.holdIndex + 2] = this.hold[0];
				this.hold[this.holdIndex + 3] = this.hold[1];
				this.hold[this.holdIndex + 4] = this.curx = this.hold[2];
				this.hold[this.holdIndex + 5] = this.cury = this.hold[3];
			}

			level = this.levels[this.levelIndex];
			while (level < this.limit) {
				if (getFlatnessSq(this.hold, this.holdIndex) < this.squareflat)
					break;
				ensureHoldCapacity(4);
				subdivide(this.hold, this.holdIndex, this.holdIndex - 4);
				this.holdIndex -= 4;
				level++;
				this.levels[this.levelIndex] = level;
				this.levelIndex++;
				this.levels[this.levelIndex] = level;
			}
			this.holdIndex += 4;
			this.levelIndex--;
			break;
		case SEG_CUBICTO:
			if (this.holdIndex >= this.holdEnd) {
				this.holdIndex = this.hold.length - 8;
				this.holdEnd = this.hold.length - 2;
				this.hold[this.holdIndex] = this.curx;
				this.hold[this.holdIndex + 1] = this.cury;
				this.hold[this.holdIndex + 2] = this.hold[0];
				this.hold[this.holdIndex + 3] = this.hold[1];
				this.hold[this.holdIndex + 4] = this.hold[2];
				this.hold[this.holdIndex + 5] = this.hold[3];
				this.hold[this.holdIndex + 6] = this.curx = this.hold[4];
				this.hold[this.holdIndex + 7] = this.cury = this.hold[5];
			}

			level = this.levels[this.levelIndex];
			while (level < this.limit) {
				if (getFlatnessSq(this.hold, this.holdIndex) < this.squareflat)
					break;
				ensureHoldCapacity(6);
				subdivide(this.hold, this.holdIndex, this.holdIndex - 6);
				this.holdIndex -= 6;
				level++;
				this.levels[this.levelIndex] = level;
				this.levelIndex++;
				this.levels[this.levelIndex] = level;
			}
			this.holdIndex += 6;
			this.levelIndex--;
			break;
		default:
			throw new IllegalArgumentException("holdType: " + this.holdType + " not supported");
		}
	}
}

class IoDescriptor {
	public static final int PROPERTY = 0;
	public static final int STATIC = 1;
	public static final int DYNAMIC = 2;
	public static final int VARARGS = 3;
	public final IO io;
	public final int index;
	public final int type;
	public final String name;
	public final Class<?> dataType;

	public IoDescriptor(IO io, int index, int type) {
		this.io = io;
		this.index = index;
		this.type = type;
		this.name = io.getName();
		this.dataType = io.getType();
	}

	@Override
	public boolean equals(Object obj) {
		return this.io == ((IoDescriptor) obj).io;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.io);
	}

	@Override
	public String toString() {
		String errorMessage = this.io.getError();
		return this.io.getClass().getSimpleName() + " index: " + this.index + " type: " + this.type + " name: " + this.name + " dataType: " + this.dataType
				+ (errorMessage == null ? "" : "error: " + errorMessage);
	}
}

class LinkDescriptor {
	public final Link link;
	public final Block inputBlock;
	public final Block outputBlock;
	public final IoDescriptor input;
	public final IoDescriptor output;

	public LinkDescriptor(Link link) {
		this.link = link;
		Input in = link.getInput();
		this.input = new IoDescriptor(in, 0, 1);
		this.inputBlock = in instanceof BlockIO ? ((BlockIO) in).getBlock() : null;
		Output out = link.getOutput();
		this.output = new IoDescriptor(out, 0, 1);
		this.outputBlock = out instanceof BlockIO ? ((BlockIO) out).getBlock() : null;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof LinkDescriptor ? this.link == ((LinkDescriptor) obj).link : false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.link);
	}

	@Override
	public String toString() {
		return "input: " + this.input + " output: " + this.output;
	}
}

class CubicIterator {
	public static final int WIND_EVEN_ODD = 0;
	public static final int WIND_NON_ZERO = 1;
	public static final int SEG_MOVETO = 0;
	public static final int SEG_LINETO = 1;
	public static final int SEG_QUADTO = 2;
	public static final int SEG_CUBICTO = 3;
	public static final int SEG_CLOSE = 4;
	CubicCurve cubicCurve;
	int index;

	CubicIterator(CubicCurve cubicCurve) {
		this.cubicCurve = cubicCurve;
	}

	public int currentSegment(double[] coords) {
		if (isDone())
			throw new NoSuchElementException("cubic iterator iterator out of bounds");
		int type;
		if (this.index == 0) {
			coords[0] = this.cubicCurve.getStartX();
			coords[1] = this.cubicCurve.getStartY();
			type = SEG_MOVETO;
		} else {
			coords[0] = this.cubicCurve.getControlX1();
			coords[1] = this.cubicCurve.getControlY1();
			coords[2] = this.cubicCurve.getControlX2();
			coords[3] = this.cubicCurve.getControlY2();
			coords[4] = this.cubicCurve.getEndX();
			coords[5] = this.cubicCurve.getEndY();
			type = SEG_CUBICTO;
		}
		return type;
	}

	public int currentSegment(float[] coords) {
		if (isDone())
			throw new NoSuchElementException("cubic iterator iterator out of bounds");
		int type;
		if (this.index == 0) {
			coords[0] = (float) this.cubicCurve.getStartX();
			coords[1] = (float) this.cubicCurve.getStartY();
			type = SEG_MOVETO;
		} else {
			coords[0] = (float) this.cubicCurve.getControlX1();
			coords[1] = (float) this.cubicCurve.getControlY1();
			coords[2] = (float) this.cubicCurve.getControlX2();
			coords[3] = (float) this.cubicCurve.getControlY2();
			coords[4] = (float) this.cubicCurve.getEndX();
			coords[5] = (float) this.cubicCurve.getEndY();
			type = SEG_CUBICTO;
		}
		return type;
	}

	public int getWindingRule() {
		return WIND_NON_ZERO;
	}

	public boolean isDone() {
		return this.index > 1;
	}

	public void next() {
		this.index++;
	}
}