/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;

import org.scenarium.tools.UncheckedRunnable;
import org.scenarium.tools.UncheckedSupplier;

public class GetterPropertyHandler {
	private final UncheckedRunnable reparator;
	private UncheckedSupplier<Object> propertyConsumer;
	private MethodHandle methodHandler;
	private final Runnable destructor;

	public static void main(String[] args) throws Throwable {
		class Test {
			private Integer patate;

			public Integer getPatate() {
				return this.patate;
			}

			public void setPatate(Integer patate) {
				this.patate = patate;
			}
		}
		Test t = new Test();
		t.setPatate(7);
		GetterPropertyHandler gph = new GetterPropertyHandler(t, t.getClass().getMethod("getPatate", (Class<?>[]) null), null, null);
		t = new Test();
		t.getPatate();
		t.setPatate(5);
		gph.reBindTo(t);
		int nbIter = 100000000;
		int res = 0;
		MethodHandle mh2 = gph.methodHandler.bindTo(t);
		UncheckedSupplier<Object> a = gph.propertyConsumer;
		for (int k = 0; k < 20; k++) {
			long time = System.currentTimeMillis();
			for (int i = 0; i < nbIter; i++)
				res += (Integer) gph.get();
			System.out.println("time GetMethodHandler: " + (System.currentTimeMillis() - time) / ((double) nbIter / 1000000) + "ns " + gph.get() + " " + res);

			time = System.currentTimeMillis();
			for (int i = 0; i < nbIter; i++)
				res += (Integer) a.get();
			System.out.println("time Provider: " + (System.currentTimeMillis() - time) / ((double) nbIter / 1000000) + "ns");

			time = System.currentTimeMillis();
			for (int i = 0; i < nbIter; i++)
				res += (Integer) mh2.invokeExact();
			System.out.println("time MethodHandler: " + (System.currentTimeMillis() - time) / ((double) nbIter / 1000000) + "ns");

			time = System.currentTimeMillis();
			for (int i = 0; i < nbIter; i++)
				res += t.getPatate();
			System.out.println("time getter: " + (System.currentTimeMillis() - time) / ((double) nbIter / 1000000) + "ns");
		}
	}

	public GetterPropertyHandler(Object propertyContainer, Method method, UncheckedRunnable reparator, Runnable destructor) throws IllegalAccessException {
		this.reparator = reparator;
		method.setAccessible(true);
		this.methodHandler = MethodHandles.lookup().unreflect(method);
		reBindTo(propertyContainer);
		this.destructor = destructor;
	}

	public void setMethodHandler(Method readMethod, Object propertyContainer) throws IllegalAccessException {
		readMethod.setAccessible(true);
		this.methodHandler = MethodHandles.lookup().unreflect(readMethod);
		reBindTo(propertyContainer);
	}

	private void updatePropertyConsumer(MethodHandle methodHandler) {
		Class<?> type = methodHandler.type().returnType();
		if (type.isPrimitive()) {
			if (type == int.class)
				this.propertyConsumer = () -> methodHandler.invokeExact();
			else if (type == double.class)
				this.propertyConsumer = () -> methodHandler.invokeExact();
			else if (type == boolean.class)
				this.propertyConsumer = () -> ((boolean) methodHandler.invokeExact());
			else if (type == float.class)
				this.propertyConsumer = () -> methodHandler.invokeExact();
			else if (type == long.class)
				this.propertyConsumer = () -> methodHandler.invokeExact();
			else if (type == byte.class)
				this.propertyConsumer = () -> ((byte) methodHandler.invokeExact());
			else if (type == short.class)
				this.propertyConsumer = () -> methodHandler.invokeExact();
			else if (type == char.class)
				this.propertyConsumer = () -> ((char) methodHandler.invokeExact());
			else {
				this.propertyConsumer = null;
				new IllegalArgumentException("Unknow primitive type");
			}
		} else
			this.propertyConsumer = () -> methodHandler.invoke();
	}

	public Object get() throws Throwable {
		if (this.reparator != null)
			this.reparator.run();
		return this.propertyConsumer.get();
	}

	public void reBindTo(Object currentProperty) {
		updatePropertyConsumer(this.methodHandler.bindTo(currentProperty));
	}

	public void destroy() {
		if (this.destructor != null)
			this.destructor.run();
	}

	@Override
	public String toString() {
		return this.methodHandler.toString();
	}
}