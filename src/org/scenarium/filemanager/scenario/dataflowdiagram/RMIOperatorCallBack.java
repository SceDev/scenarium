/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.rmi.RemoteException;
import java.util.HashMap;

import org.beanmanager.BeanRenameListener;
import org.beanmanager.rmi.server.RMIBeanCallBack;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredOutputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.RMIOperatorCallBackImpl;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.RemoteOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.SerializableWrapper;
import org.scenarium.filemanager.scenariomanager.StructChangeListener;

public class RMIOperatorCallBack extends RMIBeanCallBack implements RMIOperatorCallBackImpl {
	private static final long serialVersionUID = 5L;
	private final Block block;
	private HashMap<Integer, BeanRenameListener> beanRenameListenersMap;
	private HashMap<Integer, StructChangeListener> structChangeListenersMap;
	private HashMap<Integer, VarArgsInputChangeListener> varArgsInputChangeListenersMap;
	private HashMap<Integer, InputLinksChangeListener> inputLinksChangeListenersMap;
	private HashMap<Integer, DeclaredInputChangeListener> declaredInputChangeListenersMap;
	private HashMap<Integer, DeclaredOutputChangeListener> declaredOutputChangeListenersMap;

	private HashMap<Integer, Runnable> onStartMap;
	private HashMap<Integer, Runnable> onResumeMap;
	private HashMap<Integer, Runnable> onPauseMap;
	private HashMap<Integer, Runnable> onStopMap;
	// private ArrayList<SerializableWrapper> serializableWrapperPool = new ArrayList<>();

	public RMIOperatorCallBack(Block block) throws RemoteException {
		super();
		this.block = block;
	}

	@Override
	public void addBlockNameChangeListener(int listenerId) throws RemoteException {
		BeanRenameListener brl = (oldBeanDesc, beanDesc) -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireBlockNameChanged(beanDesc.name, beanDesc.local, oldBeanDesc.name, listenerId);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.beanRenameListenersMap == null)
			this.beanRenameListenersMap = new HashMap<>();
		this.beanRenameListenersMap.put(listenerId, brl);
		((EvolvedOperator) this.block.operator).addBlockNameChangeListener(brl);
	}

	@Override
	public void addDeclaredInputChangeListener(int listenerId) throws RemoteException {
		DeclaredInputChangeListener dicl = (names, types) -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireDeclaredInputChanged(names, types);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.declaredInputChangeListenersMap == null)
			this.declaredInputChangeListenersMap = new HashMap<>();
		this.declaredInputChangeListenersMap.put(listenerId, dicl);
		((EvolvedOperator) this.block.operator).addDeclaredInputChangeListener(dicl);
	}

	@Override
	public void addDeclaredOutputChangeListener(int listenerId) throws RemoteException {
		DeclaredOutputChangeListener docl = (names, types) -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireDeclaredOutputChanged(names, types);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.declaredOutputChangeListenersMap == null)
			this.declaredOutputChangeListenersMap = new HashMap<>();
		this.declaredOutputChangeListenersMap.put(listenerId, docl);
		((EvolvedOperator) this.block.operator).addDeclaredOutputChangeListener(docl);
	}

	@Override
	public void addInputLinksChangeListener(int listenerId) throws RemoteException {
		InputLinksChangeListener ilcl = indexOfInput -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireInputLinksChangeChanged(indexOfInput);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.inputLinksChangeListenersMap == null)
			this.inputLinksChangeListenersMap = new HashMap<>();
		this.inputLinksChangeListenersMap.put(listenerId, ilcl);
		((EvolvedOperator) this.block.operator).addInputLinksChangeListener(ilcl);
	}

	@Override
	public void addStructChangeListener(int listenerId) throws RemoteException {
		StructChangeListener scl = () -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireStructChanged();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.structChangeListenersMap == null)
			this.structChangeListenersMap = new HashMap<>();
		this.structChangeListenersMap.put(listenerId, scl);
		((EvolvedOperator) this.block.operator).addStructChangeListener(scl);
	}

	@Override
	public void addVarArgsInputChangeListener(int listenerId) throws RemoteException {
		VarArgsInputChangeListener vaicl = (indexOfInput, typeOfChange) -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireVarArgsInputChanged(indexOfInput, typeOfChange);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.varArgsInputChangeListenersMap == null)
			this.varArgsInputChangeListenersMap = new HashMap<>();
		this.varArgsInputChangeListenersMap.put(listenerId, vaicl);
		((EvolvedOperator) this.block.operator).addVarArgsInputChangeListener(vaicl);
	}

	@Override
	public String getBlockName() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getBlockName();
	}

	@Override
	public int getInputIndex(String inputName) throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getInputIndex(inputName);
	}

	@Override
	public String[] getInputsName() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getInputNames();
	}

	@Override
	public long getMaxTimeStamp() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getMaxTimeStamp();
	}

	@Override
	public int getNbTriggerableInput() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getNbTriggerableInput();
	}

	@Override
	public int getNbTriggerableOutput() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getNbTriggerableOutput();
	}

	@Override
	public int getOutputIndex(String outputName) throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getOutputIndex(outputName);
	}

	@Override
	public String[] getOutputLinkToInputName() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getOutputLinkToInputNames();
	}

	@Override
	public Class<?>[] getOutputLinkToInputType() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getOutputLinkToInputTypes();
	}

	@Override
	public String[] getTriggerableOutputsName() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getTriggerableOutputsName();
	}

	@Override
	public long getTimeOfIssue(int indexOfInput) throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getTimeOfIssue(indexOfInput);
	}

	@Override
	public long getTimeStamp(int indexOfInput) throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getTimeStamp(indexOfInput);
	}

	@Override
	public String getWarning() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getWarning();
	}

	@Override
	public String getError() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).getError();
	}

	@Override
	public boolean isPropertyAsInput(String propertyName) throws RemoteException {
		return ((EvolvedOperator) this.block.operator).isPropertyAsInput(propertyName);
	}

	@Override
	public boolean isPropertyAsOutput(String propertyName) throws RemoteException {
		return ((EvolvedOperator) this.block.operator).isPropertyAsOutput(propertyName);
	}

	@Override
	public boolean isRunning() throws RemoteException {
		return ((EvolvedOperator) this.block.operator).isRunning();
	}

	@Override
	public void onStart(int listenerId) throws RemoteException {
		Runnable onStart = () -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.onStart();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.onStartMap == null)
			this.onStartMap = new HashMap<>();
		this.onStartMap.put(listenerId, onStart);
		((EvolvedOperator) this.block.operator).onStart(onStart);
	}

	@Override
	public void onResume(int listenerId) throws RemoteException {
		Runnable onResume = () -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.onResume();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.onResumeMap == null)
			this.onResumeMap = new HashMap<>();
		this.onResumeMap.put(listenerId, onResume);
		((EvolvedOperator) this.block.operator).onResume(onResume);
	}

	@Override
	public void onPause(int listenerId) throws RemoteException {
		Runnable onPause = () -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.onPause();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.onPauseMap == null)
			this.onPauseMap = new HashMap<>();
		this.onPauseMap.put(listenerId, onPause);
		((EvolvedOperator) this.block.operator).onPause(onPause);
	}

	@Override
	public void onStop(int listenerId) throws RemoteException {
		Runnable onStop = () -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.onStop();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.onStopMap == null)
			this.onStopMap = new HashMap<>();
		this.onStopMap.put(listenerId, onStop);
		((EvolvedOperator) this.block.operator).onStop(onStop);
	}

	@Override
	public void removeBlockNameChangeListener(int listenerId) throws RemoteException {
		if (this.beanRenameListenersMap == null)
			return;
		BeanRenameListener brl = this.beanRenameListenersMap.get(listenerId);
		if (brl == null)
			return;
		((EvolvedOperator) this.block.operator).removeBlockNameChangeListener(brl);
	}

	@Override
	public void removeDeclaredInputChangeListener(int listenerId) throws RemoteException {
		if (this.declaredInputChangeListenersMap == null)
			return;
		DeclaredInputChangeListener dicl = this.declaredInputChangeListenersMap.get(listenerId);
		if (dicl == null)
			return;
		((EvolvedOperator) this.block.operator).removeDeclaredInputChangeListener(dicl);
	}

	@Override
	public void removeDeclaredOutputChangeListener(int listenerId) throws RemoteException {
		if (this.declaredOutputChangeListenersMap == null)
			return;
		DeclaredOutputChangeListener docl = this.declaredOutputChangeListenersMap.get(listenerId);
		if (docl == null)
			return;
		((EvolvedOperator) this.block.operator).removeDeclaredOutputChangeListener(docl);
	}

	@Override
	public void removeInputLinksChangeListener(int listenerId) throws RemoteException {
		if (this.inputLinksChangeListenersMap == null)
			return;
		InputLinksChangeListener ilcl = this.inputLinksChangeListenersMap.get(listenerId);
		if (ilcl == null)
			return;
		((EvolvedOperator) this.block.operator).removeInputLinksChangeListener(ilcl);
	}

	@Override
	public void removeStructChangeListener(int listenerId) throws RemoteException {
		if (this.structChangeListenersMap == null)
			return;
		StructChangeListener scl = this.structChangeListenersMap.get(listenerId);
		if (scl == null)
			return;
		((EvolvedOperator) this.block.operator).removeStructChangeListener(scl);
	}

	@Override
	public void removeVarArgsInputChangeListener(int listenerId) throws RemoteException {
		if (this.varArgsInputChangeListenersMap == null)
			return;
		VarArgsInputChangeListener vaicl = this.varArgsInputChangeListenersMap.get(listenerId);
		if (vaicl == null)
			return;
		((EvolvedOperator) this.block.operator).removeVarArgsInputChangeListener(vaicl);
	}

	@Override
	public void runLater(long callingThreadId, int taskId) throws RemoteException {
		updateCallingThread(callingThreadId);
		((EvolvedOperator) this.block.operator).runLater(() -> {
			try {
				RemoteOperator remoteOp = this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.runLaterCallBack(taskId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public void setError(String error) throws RemoteException {
		((EvolvedOperator) this.block.operator).setError(error);
	}

	@Override
	public boolean addPropertyAsInput(long callingThreadId, String propertyName) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) this.block.operator).addPropertyAsInput(propertyName);
	}

	@Override
	public boolean removePropertyAsInput(long callingThreadId, String propertyName) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) this.block.operator).removePropertyAsInput(propertyName);
	}

	@Override
	public boolean addPropertyAsOutput(long callingThreadId, String propertyName) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) this.block.operator).addPropertyAsOutput(propertyName);
	}

	@Override
	public boolean removePropertyAsOutput(long callingThreadId, String propertyName) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) this.block.operator).removePropertyAsOutput(propertyName);
	}

	@Override
	public void setWarning(String warning) throws RemoteException {
		((EvolvedOperator) this.block.operator).setWarning(warning);
	}

	@Override
	public void triggerProperty(long callingThreadId, String outputPropertyName, Object outputValue) throws RemoteException {
		updateCallingThread(callingThreadId);
		((EvolvedOperator) this.block.operator).triggerProperty(outputPropertyName, outputValue instanceof SerializableWrapper ? ((SerializableWrapper) outputValue).getValue() : outputValue);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object outputValue) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) this.block.operator).triggerOutput(outputValue instanceof SerializableWrapper ? ((SerializableWrapper) outputValue).getValue() : outputValue);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object outputValue, long timeStamp) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) this.block.operator).triggerOutput(outputValue instanceof SerializableWrapper ? ((SerializableWrapper) outputValue).getValue() : outputValue, timeStamp);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object[] outputValues) throws RemoteException {
		updateCallingThread(callingThreadId);
		SerializableWrapper.deserialize(outputValues);
		return ((EvolvedOperator) this.block.operator).triggerOutput(outputValues);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object[] outputValues, long timeStamp) throws RemoteException {
		updateCallingThread(callingThreadId);
		SerializableWrapper.deserialize(outputValues);
		return ((EvolvedOperator) this.block.operator).triggerOutput(outputValues, timeStamp);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object[] outputValues, long[] timeStamps) throws RemoteException, IllegalArgumentException {
		updateCallingThread(callingThreadId);
		SerializableWrapper.deserialize(outputValues);
		return ((EvolvedOperator) this.block.operator).triggerOutput(outputValues, timeStamps);
	}

	// @Override
	// public void addIONameChangeListener(int listenerId) throws RemoteException {
	// NameChangeListener dicl = (input, indexOfInput, newName) -> {
	// try {
	// RemoteOperator remoteOp = block.getRemoteOperator();
	// if(remoteOp != null)
	// remoteOp.fireIONameChanged(input, indexOfInput, newName);
	// } catch (Throwable e) {
	// e.printStackTrace();
	// }
	// };
	// if(nameChangeChangeListenersMap == null)
	// nameChangeChangeListenersMap = new HashMap<>();
	// nameChangeChangeListenersMap.put(listenerId, dicl);
	// ((EvolvedOperator) block.operator).addIONameChangeListener(dicl);
	// }
	//
	// @Override
	// public void removeIONameChangeListener(int listenerId) throws RemoteException {
	// if(nameChangeChangeListenersMap == null)
	// return;
	// NameChangeListener ncl = nameChangeChangeListenersMap.get(listenerId);
	// if(ncl == null)
	// return;
	// ((EvolvedOperator) block.operator).removeIONameChangeListener(ncl);
	// }

	private void updateCallingThread(long callingThreadId) {
		RemoteOperator remoteOp = this.block.getRemoteOperator();
		if (remoteOp != null)
			remoteOp.setCallingThread(callingThreadId);
	}

	@Override
	public boolean updateInputs(long callingThreadId, String[] names, Class<?>[] types) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) this.block.operator).updateInputs(names, types);
	}

	@Override
	public boolean updateOutputs(long callingThreadId, String[] names, Class<?>[] types) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) this.block.operator).updateOutputs(names, types);
	}
}
