/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx.fx3D;

import java.util.List;

import javax.vecmath.Point3f;

import javafx.scene.shape.TriangleMesh;

public class TetrahedronMesh extends TriangleMesh {
	private final List<Point3f> vertices;
	private int[] facesLink;

	public TetrahedronMesh(double length, List<Point3f> v) {
		this.vertices = v;
		if (length > 0.0) {
			float[] points = new float[this.vertices.size() * 12];
			int[] faces = new int[this.vertices.size() * 24];
			this.facesLink = new int[this.vertices.size() * 4];
			float[] texCoords = new float[this.vertices.size() * 12];
			int vertexCounter = 0;
			int primitiveCounter = 0;
			int pointCounter = 0;
			int facesCounter = 0;
			int texCounter = 0;
			for (Point3f vertex : this.vertices) {
				vertex.scale(100);
				points[primitiveCounter] = vertex.x;
				points[primitiveCounter + 1] = (float) (vertex.y - length * Math.sqrt(3.0) / 3.0);
				points[primitiveCounter + 2] = -vertex.z;
				points[primitiveCounter + 3] = (float) (vertex.x + length / 2.0);
				points[primitiveCounter + 4] = (float) (vertex.y + length * Math.sqrt(3.0) / 6.0);
				points[primitiveCounter + 5] = -vertex.z;
				points[primitiveCounter + 6] = (float) (vertex.x - length / 2.0);
				points[primitiveCounter + 7] = (float) (vertex.y + length * Math.sqrt(3.0) / 6.0);
				points[primitiveCounter + 8] = -vertex.z;
				points[primitiveCounter + 9] = vertex.x;
				points[primitiveCounter + 10] = vertex.y;
				points[primitiveCounter + 11] = (float) -(vertex.z - length * Math.sqrt(2.0 / 3.0));
				faces[facesCounter] = pointCounter + 0;
				faces[facesCounter + 1] = pointCounter + 0;
				faces[facesCounter + 2] = pointCounter + 1;
				faces[facesCounter + 3] = pointCounter + 1;
				faces[facesCounter + 4] = pointCounter + 2;
				faces[facesCounter + 5] = pointCounter + 2;
				faces[facesCounter + 6] = pointCounter + 1;
				faces[facesCounter + 7] = pointCounter + 1;
				faces[facesCounter + 8] = pointCounter + 0;
				faces[facesCounter + 9] = pointCounter + 0;
				faces[facesCounter + 10] = pointCounter + 3;
				faces[facesCounter + 11] = pointCounter + 3;
				faces[facesCounter + 12] = pointCounter + 2;
				faces[facesCounter + 13] = pointCounter + 2;
				faces[facesCounter + 14] = pointCounter + 1;
				faces[facesCounter + 15] = pointCounter + 1;
				faces[facesCounter + 16] = pointCounter + 3;
				faces[facesCounter + 17] = pointCounter + 4;
				faces[facesCounter + 18] = pointCounter + 0;
				faces[facesCounter + 19] = pointCounter + 0;
				faces[facesCounter + 20] = pointCounter + 2;
				faces[facesCounter + 21] = pointCounter + 2;
				faces[facesCounter + 22] = pointCounter + 3;
				faces[facesCounter + 23] = pointCounter + 5;
				this.facesLink[pointCounter] = vertexCounter;
				this.facesLink[pointCounter + 1] = vertexCounter;
				this.facesLink[pointCounter + 2] = vertexCounter;
				this.facesLink[pointCounter + 3] = vertexCounter;
				texCoords[texCounter] = 0.5f;
				texCoords[texCounter + 1] = 1.0f;
				texCoords[texCounter + 2] = 0.75f;
				texCoords[texCounter + 3] = (float) (1.0 - Math.sqrt(3.0) / 4.0);
				texCoords[texCounter + 4] = 0.25f;
				texCoords[texCounter + 5] = (float) (1.0 - Math.sqrt(3.0) / 4.0);
				texCoords[texCounter + 6] = 1.0f;
				texCoords[texCounter + 7] = 1.0f;
				texCoords[texCounter + 8] = 0.5f;
				texCoords[texCounter + 9] = (float) (1.0 - Math.sqrt(3.0) / 2.0);
				texCoords[texCounter + 10] = 0.0f;
				texCoords[texCounter + 11] = 1.0f;
				vertexCounter++;
				primitiveCounter += 12;
				pointCounter += 4;
				facesCounter += 24;
				texCounter += 12;
			}
			getPoints().setAll(points);
			getFaces().setAll(faces);
			getTexCoords().setAll(texCoords);
		}
	}

	public Point3f getPointFromFace(int faceId) {
		return this.vertices.get(this.facesLink[faceId]);
	}
}