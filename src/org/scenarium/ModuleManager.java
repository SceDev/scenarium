/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
//import java.io.File;
import java.io.IOException;
import java.lang.module.FindException;
import java.lang.module.ModuleDescriptor;
import java.lang.module.ModuleDescriptor.Requires;
import java.lang.module.ModuleDescriptor.Version;
import java.lang.module.ModuleFinder;
import java.lang.module.ResolutionException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.LoadModuleListener;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.tools.EventListenerList;
import org.beanmanager.tools.Logger;
import org.beanmanager.tools.Tuple;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.communication.can.dbc.EncodingMessageProperties;
import org.scenarium.communication.can.dbc.MessageIdentifier;
import org.scenarium.communication.can.dbc.SignalIdentifier;
import org.scenarium.display.RenderPane;
import org.scenarium.display.drawer.DrawerManager;
import org.scenarium.display.toolbarclass.ToolBarDescriptor;
import org.scenarium.editors.CurveSeriesEditor;
import org.scenarium.editors.CurvedEditor;
import org.scenarium.editors.CurveiEditor;
import org.scenarium.editors.GeographicCoordinateEditor;
import org.scenarium.editors.GeographicalPoseEditor;
import org.scenarium.editors.NMEA0183Editor;
import org.scenarium.editors.ScenarioDescriptorEditor;
import org.scenarium.editors.ToolDescriptorEditor;
import org.scenarium.editors.can.CanTrameEditor;
import org.scenarium.editors.can.dbc.EncodingMessagePropertiesEditor;
import org.scenarium.editors.can.dbc.MessageIdentifierEditor;
import org.scenarium.editors.can.dbc.SignalIdentifierEditor;
import org.scenarium.filemanager.datastream.DataStreamManager;
import org.scenarium.filemanager.filerecorder.FileStreamRecorder;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.DiagramScheduler;
import org.scenarium.filemanager.scenariomanager.ScenarioDescriptor;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.struct.GeographicCoordinate;
import org.scenarium.struct.GeographicalPose;
import org.scenarium.struct.ScenariumProperties;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.curve.Curved;
import org.scenarium.struct.curve.Curvei;

import net.sf.marineapi.nmea.sentence.Sentence;

//version: Si jar modulaire: --module-version Sinon: nom du fichier jar (jar --update --file a.jar --module-version 1.0.1)
public class ModuleManager {
	private static boolean initialized = false;
	private static final String INTERN_PLUGIN_FOLDER_NAME = "modules";
	private static Path pluginFolder;
	private static final PathMatcher JAR_PATH_MATCHER = FileSystems.getDefault().getPathMatcher("glob:**.jar");
	private static final HashMap<String, List<SimpleModuleDescriptor>> INDEXED_MODULES = new HashMap<>();
	private static final HashMap<String, Tuple<Module, List<Path>>> LOADED_MODULES = new HashMap<>();
	private static final ModuleLayer PARENT = ModuleLayer.boot();
	private static final EventListenerList<LoadModuleListener> LISTENERS = new EventListenerList<>();
	public static final Set<String> PARENT_MODULES_NAME = PARENT.modules().stream().map(Module::getName).collect(Collectors.toUnmodifiableSet());
	private static final Kind<?>[] ALL_EVENTS = new WatchEvent.Kind<?>[] { StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY };
	private static WatchService watchService = null;
	private static final HashMap<WatchKey, Path> KEYMAP = new HashMap<>();

	public static void loadEmbeddedAndInternalModules() {
		if (initialized) // Already initialized
			return;
		initialized = true;
		// Forces static methods to be called from Scenarim module and not from another module. Otherwise, it prevents the release of module resources. See on RenderPane
		try {
			Class.forName(OperatorManager.class.getName());
			Class.forName(DiagramScheduler.class.getName());
			Class.forName(PropertyEditorManager.class.getName());
			Class.forName(DrawerManager.class.getName());
			Class.forName(RenderPane.class.getName());
			Class.forName(ScenarioManager.class.getName());
			Class.forName(DataStreamManager.class.getName());
			Class.forName(FileStreamRecorder.class.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		Logger.log(Logger.MODULE, "Load embedded modules");
		PropertyEditorManager.registerEditor(ScenarioDescriptor.class, ScenarioDescriptorEditor.class);
		PropertyEditorManager.registerEditor(ToolBarDescriptor.class, ToolDescriptorEditor.class);
		PropertyEditorManager.registerEditor(Curved.class, CurvedEditor.class);
		PropertyEditorManager.registerEditor(Curvei.class, CurveiEditor.class);
		PropertyEditorManager.registerEditor(CurveSeries.class, CurveSeriesEditor.class);
		PropertyEditorManager.registerEditor(CanTrame.class, CanTrameEditor.class);
		PropertyEditorManager.registerEditor(Sentence.class, NMEA0183Editor.class);
		PropertyEditorManager.registerEditor(SignalIdentifier.class, SignalIdentifierEditor.class);
		PropertyEditorManager.registerEditor(MessageIdentifier.class, MessageIdentifierEditor.class);
		PropertyEditorManager.registerEditor(EncodingMessageProperties[].class, EncodingMessagePropertiesEditor.class);
		PropertyEditorManager.registerEditor(GeographicCoordinate.class, GeographicCoordinateEditor.class);
		PropertyEditorManager.registerEditor(GeographicalPose.class, GeographicalPoseEditor.class);
		loadPlugins(ServiceLoader.load(PluginsSupplier.class)); // Built-in modules. 200ms for the first one, 40ms for the following

		Logger.log(Logger.MODULE, "Load Internal modules");
		pluginFolder = Paths.get(Scenarium.workspaceDirectory() + FileSystems.getDefault().getSeparator() + INTERN_PLUGIN_FOLDER_NAME);

		boolean pluginFolderExists = Files.exists(getPluginFolder());
		if (!pluginFolderExists)
			try {
				Files.createDirectories(getPluginFolder());
				pluginFolderExists = true;
			} catch (IOException e) {
				Logger.logError(Logger.MODULE, "Cannot create module directory: " + getPluginFolder() + " due to: " + e.getMessage());
			}
		if (pluginFolderExists)
			try {
				watchService = FileSystems.getDefault().newWatchService();
				addWatchService(getPluginFolder(), true, null);
				updateLoadedModules();
				Thread t = new Thread(() -> {
					try {
						WatchKey key;
						while ((key = watchService.take()) != null) {
							Path dir = KEYMAP.get(key);
							HashSet<Path> createdModulePath = new HashSet<>();
							for (WatchEvent<?> event : key.pollEvents()) {
								Kind<?> kind = event.kind();
								Path path = dir.resolve((Path) event.context());
								boolean isRegistered = isModulePathRegistered(path);
								if (kind == StandardWatchEventKinds.ENTRY_CREATE) { // Si je créer un module à coté d'un module externe, il faut pas le charger
									if (isJarFile(path)) {
										if (!isRegistered)
											addToIndex(path, false);
										else
											markDirty(path);
										createdModulePath.add(path);
									} else if (Files.isDirectory(path) && path.startsWith(getPluginFolder()))
										addWatchService(path, true, null);
								} else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
									if (isRegistered && JAR_PATH_MATCHER.matches(path)) {
										// If this path has extern module registered, need to unregister them
										INDEXED_MODULES.values().stream().filter(smd -> !smd.get(0).intern && smd.get(0).scenariumPlugin && isSameFile(smd.get(0).path, path))
												.collect(Collectors.toList()).forEach(smd -> {
													Tuple<Module, List<Path>> moduleInfo = LOADED_MODULES.get(smd.get(0).descriptor.name());
													if (moduleInfo != null)
														unregisterExternalModuleWithoutUpdate(moduleInfo.getFirst());
												});
										removeToIndex(path);
									} else
										removeWatchService(path, false, true);
								} else if (kind == StandardWatchEventKinds.ENTRY_MODIFY && isRegistered && isJarFile(path) && !createdModulePath.contains(path))
									markDirty(path);
							}
							updateLoadedModules();
							key.reset();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				});
				t.setName("ModuleWatchService");
				t.start();
			} catch (IOException ex) {
				Logger.logError(Logger.MODULE, "Cannot create watch service");
				ex.printStackTrace();
			}
	}

	private static void addWatchService(Path pathToWatch, boolean subPath, Path excludedPath) {
		if (watchService == null)
			return;
		int maxDepth = subPath ? Integer.MAX_VALUE - 1 : 0;
		try {
			Files.walk(pathToWatch, maxDepth).filter(p -> Files.isReadable(p) && Files.isDirectory(p)).forEach(path -> {
				try {
					KEYMAP.put(path.register(watchService, ALL_EVENTS), path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		try {
			Files.walk(pathToWatch, maxDepth + 1).filter(ModuleManager::isJarFile).filter(p -> excludedPath == null ? true : !isSameFile(p, excludedPath)).forEach(path -> addToIndex(path, false));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void removeWatchService(Path path, boolean oneLevel, boolean forceRemove) {
		if (forceRemove || INDEXED_MODULES.values().stream().flatMap(l -> l.stream()).filter(smd -> !smd.intern && smd.scenariumPlugin && isSameFile(smd.path.getParent(), path)).findAny().isEmpty())
			KEYMAP.entrySet().removeIf(e -> {
				if (oneLevel ? isSameFile(e.getValue(), path) : e.getValue().startsWith(path)) {
					e.getKey().cancel();
					removeAllFromDirectoryToIndex(e.getValue(), oneLevel);
					return true;
				}
				return false;
			});
	}

	private ModuleManager() {}

	// public static void main(String[] args) throws IOException {
	// Path oriTestFile = Path.of("C:\\Users\\marcr\\Workspace\\Scenarium\\Sample.jar");
	// Path copyTestFile = Path.of("C:\\Users\\marcr\\Workspace\\Scenarium\\Sample2.jar");
	// Files.copy(oriTestFile, copyTestFile, StandardCopyOption.REPLACE_EXISTING);
	// loadAndUnloadTest(copyTestFile);
	// System.gc();
	// try {
	// Files.delete(copyTestFile);
	// System.out.println("Good");
	// } catch (Exception e) {
	// System.err.println("Bad");
	// }
	// }

	// private static void loadAndUnloadTest(Path testFile) {
	// List<Module> newModules = registerModules(testFile);
	// try {
	// Class<?> c = Class.forName("org.operators.SampleOperator", true, newModules.get(0).getClassLoader());
	// Introspector.getBeanInfo(c);
	// Introspector.flushCaches();
	// Introspector.flushFromCaches(c);
	// Class<?> ci = Class.forName("com.sun.beans.introspect.ClassInfo");
	// Field cache = ci.getDeclaredField("CACHE");
	// /* Object val = */cache.get(null);
	// // cache.setAccessible(true);
	// // cache.set(null, null);
	// System.out.println(c);
	// // flushCachesWithOOME();
	// } catch (ClassNotFoundException | SecurityException | IllegalArgumentException | IntrospectionException | NoSuchFieldException | IllegalAccessException e) {
	// e.printStackTrace();
	// }
	// System.out.println(newModules);
	// newModules.forEach(ModuleManager::unregisterModule);
	// }

	static void loadExternalModules() {
		Logger.log(Logger.MODULE, "Load external modules");
		Path[] modules = ScenariumProperties.get().getExternModules();
		if (modules != null)
			registerExternalModules(modules);
	}

	public static void registerExternalModules(Path[] modulesPaths) {
		Logger.log(Logger.MODULE, "Register external module(s) from: " + Arrays.toString(modulesPaths));
		ArrayList<IllegalArgumentException> exceptions = null;
		for (Path modulesPath : modulesPaths)
			try {
				registerExternalModuleWithoutUpdate(modulesPath);
			} catch (IllegalArgumentException e) {
				if (exceptions == null)
					exceptions = new ArrayList<>();
				exceptions.add(e);
			}
		updateLoadedModules();
		if (exceptions != null)
			throw new IllegalArgumentException(exceptions.stream().map(e -> e.getMessage()).collect(Collectors.joining(System.lineSeparator())));
	}

	public static void registerExternalModules(Path modulePath) {
		Logger.log(Logger.MODULE, "Register external module from: " + modulePath);
		Scenarium.runTaskAndWait(() -> {
			registerExternalModuleWithoutUpdate(modulePath);
			updateLoadedModules();
		});
	}

	private static void registerExternalModuleWithoutUpdate(Path modulePath) {
		Path parentPath = modulePath.getParent();
		if (parentPath != null) {
			if (!KEYMAP.values().contains(parentPath))
				addWatchService(parentPath, false, modulePath);
			if (Files.exists(modulePath))
				addToIndex(modulePath, true);
			else
				removeWatchService(parentPath, true, false);
		}
	}

	public static void unregisterExternalModules(Module[] modules) {
		Logger.log(Logger.MODULE, "Unregister external module(s): " + Arrays.toString(modules));
		for (Module module : modules)
			unregisterExternalModuleWithoutUpdate(module);
		updateLoadedModules();
	}

	public static void unregisterExternalModule(Module module) {
		Logger.log(Logger.MODULE, "Unregister external module: " + module);
		unregisterExternalModuleWithoutUpdate(module);
		updateLoadedModules();
	}

	private static void unregisterExternalModuleWithoutUpdate(Module module) {
		List<SimpleModuleDescriptor> sameNameIndexedModules = INDEXED_MODULES.get(module.getName());
		SimpleModuleDescriptor smdToUnregister = INDEXED_MODULES.get(module.getName()).get(0);
		if (LOADED_MODULES.get(module.getName()) == null || !smdToUnregister.descriptor.equals(module.getDescriptor()))
			throw new IllegalArgumentException("The module: " + module.getDescriptor().toNameAndVersion() + " is not registered");
		if (sameNameIndexedModules.size() == 1)
			INDEXED_MODULES.remove(module.getName());
		else {
			sameNameIndexedModules.remove(0);
			sameNameIndexedModules.get(0).dirty = true;
		}
		// Need to Remove the directory watchers in no other register extern module has same parent
		removeWatchService(smdToUnregister.path.getParent(), true, false);
	}

	/** Reload modules from path. This method firstly unregister the intern module, then call unregisterTask, then register new modules from path
	 * @param toReload the path for the modules
	 * @param unregisterTask the task to call at the end of module removing and previous registering new modules */
	public static void reloadModules(Module[] moduleToReload, Runnable unregisterTask) {
		for (Module module : moduleToReload)
			if (LOADED_MODULES.containsKey(module.getName()))
				throw new IllegalArgumentException("The module: " + module.getName() + "is not loaded");
		Scenarium.runTaskAndWait(() -> safelyReloadModules(Arrays.stream(moduleToReload).map(mtr -> INDEXED_MODULES.get(mtr.getName()).get(0)).collect(Collectors.toList()), unregisterTask));
	}

	public static String getModulePath(Module module) {
		SimpleModuleDescriptor desc = getModuleDescription(module);
		return desc.intern ? "intern module" : desc.path.toString();
	}

	public static boolean isInternalModule(Module module) {
		SimpleModuleDescriptor desc = getModuleDescription(module);
		return desc == null ? false : desc.intern;
	}
	
	public static boolean isBuiltInModule(Module module) {
		return module.getClassLoader().equals(ModuleManager.class.getClassLoader());
	}

	public static boolean isScenariumPlugin(ModuleDescriptor descriptor) {
		return descriptor.uses().contains(PluginsSupplier.class.getName()) && descriptor.provides().stream().anyMatch(p -> p.service().equals(PluginsSupplier.class.getName()));
	}

	public static boolean isJarFile(Path path) {
		return Files.isRegularFile(path) && JAR_PATH_MATCHER.matches(path);
	}

	public static int compare(Optional<Version> v1, Optional<Version> v2) {
		return v1.isPresent() && v2.isPresent() ? -v1.get().compareTo(v2.get()) : v1.isPresent() ? -1 : v2.isPresent() ? 1 : 0;
	}

	private static boolean isModulePathRegistered(Path path) {
		return INDEXED_MODULES.values().stream().flatMap(l -> l.stream()).anyMatch(smd -> isSameFile(smd.path, path));
	}

	public static void addLoadModuleListener(LoadModuleListener listener) {
		LISTENERS.addStrongRef(listener);
	}

	public static void removeLoadModuleListener(LoadModuleListener listener) {
		LISTENERS.removeStrongRef(listener);
	}

	private static void fireModuleLoaded(Module module) {
		LISTENERS.forEach(l -> l.loaded(module));
	}

	private static Stream<Runnable> fireModuleModified(Module module) {
		ArrayList<Runnable> tasks = new ArrayList<>();
		LISTENERS.forEach(l -> {
			Runnable runnable = l.modified(module);
			if (runnable != null)
				tasks.add(runnable);
		});
		return tasks.stream();
	}

	private static void fireModuleUnloaded(Module module) {
		LISTENERS.forEach(l -> l.unloaded(module));
	}

	private static Optional<Module> registerModule(SimpleModuleDescriptor smd) {
		Logger.log(Logger.MODULE, "Register " + (smd.intern ? "intern" : "extern") + " module: " + smd.descriptor.toNameAndVersion() + " from: " + smd.path);
		String moduleName = smd.descriptor.name();
		if (smd.scenariumPlugin) {
			if (!BeanManager.isModuleRegistered(moduleName)) {
				ModuleFinder finder;
				try {
					ArrayList<Path> requiresModulesPath = findRequiredModules(smd.descriptor, new HashSet<>(PARENT_MODULES_NAME), smd.intern ? null : smd.path.getParent());
					requiresModulesPath.add(smd.path);
					finder = ModuleFinder.of(requiresModulesPath.toArray(Path[]::new));
					ModuleLayer moduleLayer = PARENT.defineModulesWithOneLoader(PARENT.configuration().resolve(finder, ModuleFinder.of(), List.of(moduleName)), ClassLoader.getSystemClassLoader());
					ClassLoader classLoader = moduleLayer.findLoader(moduleName);
					ServiceLoader<PluginsSupplier> sl = ServiceLoader.load(PluginsSupplier.class, classLoader);
					if (!sl.findFirst().isEmpty()) {
						Optional<Module> om = moduleLayer.modules().stream().filter(m -> m.getName().equals(smd.descriptor.name())).findFirst();
						if (om.isPresent()) {
							loadPlugins(sl);
							sl.reload();
							Module module = om.get();
							BeanManager.registerModule(module, classLoader);
							requiresModulesPath.remove(requiresModulesPath.size() - 1);
							LOADED_MODULES.put(moduleName, new Tuple<>(module, requiresModulesPath));
							smd.dirty = false;
							ModuleManager.fireModuleLoaded(module);
						}
						return om;
					}
				} catch (FindException | ResolutionException | SecurityException | IllegalArgumentException | IllegalStateException | Error | LayerInstantiationException e) {
					Logger.logError(Logger.MODULE, "Cannot register module: " + moduleName + " due to: " + e.getClass().getSimpleName() + ": " + e.getMessage());
				}
			} else {
				SimpleModuleDescriptor desc = INDEXED_MODULES.get(moduleName).get(0);
				Logger.logError(Logger.MODULE, "Cannot load module: " + moduleName + ", another " + (desc.intern ? "intern" : "extern") + " located at: " + desc.path + " have the same name");
			}
		} else
			Logger.logError(Logger.MODULE,
					"Cannot load module: " + moduleName + ", this is not a Scenarium plugin. A Scenarium plugin uses " + PluginsSupplier.class.getName() + " and provides at least one plugin");
		return Optional.empty();
	}

	private static ArrayList<Path> findRequiredModules(ModuleDescriptor descriptor, Set<String> parentModulesname, Path additionalModulePath) throws FindException {
		ArrayList<Path> requiresModulesPath = new ArrayList<>();
		String moduleName = descriptor.name();
		for (Requires requireModule : descriptor.requires()) {
			String requireModuleName = requireModule.name();
			if (!parentModulesname.contains(requireModuleName)) {
				List<SimpleModuleDescriptor> requiredModules = getModulesDescription(requireModuleName);
				Optional<SimpleModuleDescriptor> requiredModuleDescriptor;
				if (requiredModules != null
						&& (requiredModuleDescriptor = requiredModules.stream().filter(p -> p.intern || (additionalModulePath != null && isSameFile(p.path.getParent(), additionalModulePath))).findFirst())
								.isPresent()) {
					requiresModulesPath.add(requiredModuleDescriptor.get().path);
					parentModulesname.add(requireModuleName);
					requiresModulesPath.addAll(findRequiredModules(requiredModuleDescriptor.get().descriptor, parentModulesname, additionalModulePath));
				} else
					throw new FindException("Cannot register module: " + moduleName + ". Module: " + requireModuleName + " not found, required by " + moduleName);
			}
		}
		return requiresModulesPath;
	}

	private static void loadPlugins(ServiceLoader<PluginsSupplier> serviceLoader) {
		serviceLoader.forEach(pl -> {
			pl.populateOperators(OperatorManager::addOperator);
			pl.populateCloners(new ClonerConsumer());
			pl.populateEditors(new EditorConsumer());
			pl.populateDrawers(DrawerManager::registerDrawer);
			pl.populateToolBars(RenderPane::addToolBarDescs); // Leak here JDK-8116412 workaround: weakReference in RenderFrame
			pl.populateScenarios(ScenarioManager::registerScenario);
			pl.populateDataStream(new DataStreamConsumer());
			pl.populateFileRecorder(FileStreamRecorder::registerStreamRecorder);
			pl.populateClassNameRedirection(BeanManager::registerClassNameRedirection);
		});
	}

	private static synchronized void unregisterModule(Module module) {
		Logger.log(Logger.MODULE, "Unregister module: " + module.getDescriptor().toNameAndVersion());
		// I) Operators
		OperatorManager.purgeOperators(module);
		BeanManager.unregisterModule(module);
		// II) Cloners
		DiagramScheduler.purgeCloners(module);
		// III) Editors
		PropertyEditorManager.purgeEditors(module);
		// V) ToolBars
		RenderPane.purgeToolBars(module);
		// IV) Drawers
		DrawerManager.purgeDrawers(module);
		// VI) Scenarios
		ScenarioManager.purgeScenarios(module);
		// VII) DataStreams
		DataStreamManager.purgeDataStream(module);
		// VIII) StreamRecorders
		FileStreamRecorder.purgeStreamRecorder(module);
		LOADED_MODULES.remove(module.getName());
		ModuleManager.fireModuleUnloaded(module);
	}

	private static void safelyReloadModules(List<SimpleModuleDescriptor> modules, Runnable unregisterTask) {
		Logger.log(Logger.MODULE, "Reload modules: " + modules.stream().map(m -> m.descriptor.toNameAndVersion()).collect(Collectors.joining(",")));
		List<Runnable> tasks = modules.stream().flatMap(smd -> ModuleManager.fireModuleModified(LOADED_MODULES.get(smd.descriptor.name()).getFirst())).collect(Collectors.toList());
		// Records all beans that's going to be removed
		class BeanInfo {
			String className;
			String name;
			String local;
			byte[] data;

			public BeanInfo(String className, String name, String local, byte[] data) {
				this.className = className;
				this.name = name;
				this.local = local;
				this.data = data;
			}
		}
		HashMap<String, List<BeanInfo>> beansToRemoveInfo = new HashMap<>();
		for (SimpleModuleDescriptor smd : modules) {
			Module module = LOADED_MODULES.get(smd.descriptor.name()).getFirst();
			for (BeanDesc<?> beanDesc : BeanEditor.getAllBeans())
				if (beanDesc.bean.getClass().getModule().equals(module)) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					try {
						new BeanManager(beanDesc.bean, BeanManager.defaultDir).save(baos, "");
						String beanClassName = BeanManager.getDescriptorFromClass(beanDesc.bean.getClass());
						List<BeanInfo> sameTypeBeansToRemove = beansToRemoveInfo.get(beanClassName);
						if (sameTypeBeansToRemove == null) {
							sameTypeBeansToRemove = new ArrayList<>();
							beansToRemoveInfo.put(beanClassName, sameTypeBeansToRemove);
						}
						sameTypeBeansToRemove.add(new BeanInfo(beanClassName, beanDesc.name, beanDesc.local, baos.toByteArray()));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		}
		modules.forEach(smd -> unregisterModule(LOADED_MODULES.get(smd.descriptor.name()).getFirst()));
		if (unregisterTask != null)
			unregisterTask.run();
		// Reload and recreate all removed beans
		BiFunction<Object, File, Boolean> oldLoadBeanMethod = BeanManager.loadBeanMethod;
		BeanManager.loadBeanMethod = (bean, file) -> {
			String subBeanName = file.getAbsolutePath();
			subBeanName = subBeanName.substring(subBeanName.lastIndexOf(File.separator) + 1, subBeanName.length() - BeanManager.SERIALIZE_EXT.length());
			subBeanName = subBeanName.substring(subBeanName.indexOf(BeanDesc.SEPARATOR) + 1, subBeanName.length());
			List<BeanInfo> sameTypeBeansRemoved = beansToRemoveInfo.get(BeanManager.getDescriptorFromClass(bean.getClass()));
			if (sameTypeBeansRemoved != null)
				for (Iterator<BeanInfo> iterator = sameTypeBeansRemoved.iterator(); iterator.hasNext();) {
					BeanInfo beanRemovedInfo = iterator.next();
					if (beanRemovedInfo.name.equals(subBeanName)) {
						try {
							new BeanManager(bean, beanRemovedInfo.local).load(new ByteArrayInputStream(beanRemovedInfo.data), beanRemovedInfo.local);
							iterator.remove();
						} catch (IOException e) {
							e.printStackTrace();
						}
						return true;
					}
				}
			try {
				return oldLoadBeanMethod != null ? oldLoadBeanMethod.apply(bean, file) : new BeanManager(bean, "").load(new FileInputStream(file), file.getParent() + File.separator);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		};
		modules.forEach(smd -> registerModule(smd).isPresent());
		for (List<BeanInfo> beanRemovedInfos : beansToRemoveInfo.values())
			for (Iterator<BeanInfo> iterator = beanRemovedInfos.iterator(); iterator.hasNext();) {
				BeanInfo beanRemovedInfo = iterator.next();
				try {
					Class<?> c = BeanManager.getClassFromDescriptor(beanRemovedInfo.className);
					BeanDesc<?> beanDesc = BeanEditor.getRegisterBean(c, beanRemovedInfo.name);
					if (beanDesc != null)
						new BeanManager(beanDesc.bean, beanDesc.local).load(new ByteArrayInputStream(beanRemovedInfo.data), beanDesc.local);
					else {
						Object newBean = c.getConstructor().newInstance();
						new BeanManager(newBean, beanRemovedInfo.local).load(new ByteArrayInputStream(beanRemovedInfo.data), beanRemovedInfo.local);
						BeanEditor.registerBean(newBean, beanRemovedInfo.name, beanRemovedInfo.local);
					}
					iterator.remove();
				} catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
						| SecurityException e) {
					Logger.logError(Logger.MODULE, "Cannot reload bean: " + beanRemovedInfo + " due to: " + e.getClass().getSimpleName() + ": " + e.getMessage());
				}
			}
		BeanManager.loadBeanMethod = oldLoadBeanMethod;
		tasks.forEach(Runnable::run);
	}

	private static void addToIndex(Path path, boolean registerExternModule) {
		// System.out.println("add path: " + path + " as registerExternModule: " + registerExternModule);
		if (registerExternModule && path.startsWith(ModuleManager.getPluginFolder()))
			throw new IllegalArgumentException("Cannot register internal module path: " + path + " as an external module path");
		if (isModulePathRegistered(path))
			throw new IllegalArgumentException("The module path: " + path + " is already registerd");
		List<String> errorMessages = null;
		try {
			for (Iterator<ModuleDescriptor> iterator = ModuleFinder.of(path).findAll().stream().map(mr -> mr.descriptor()).iterator(); iterator.hasNext();) {
				ModuleDescriptor descriptor = iterator.next();
				SimpleModuleDescriptor smd = new SimpleModuleDescriptor(path, descriptor, registerExternModule);
				List<SimpleModuleDescriptor> moduleList = INDEXED_MODULES.get(descriptor.name());
				if (registerExternModule && !smd.intern && moduleList != null && moduleList.get(0).compareTo(smd) < 0) {
					SimpleModuleDescriptor otherModule = moduleList.get(0);
					String errorMessage = "Cannot register external module: " + smd.descriptor.toNameAndVersion() + System.lineSeparator() + "Another more recent "
							+ (otherModule.intern ? "intern" : "extern") + " module is already registered: " + otherModule.descriptor.toNameAndVersion()
							+ (otherModule.intern ? "" : " at: " + otherModule.path) + System.lineSeparator() + "Remove this module before registering this new module.";
					Logger.logError(Logger.MODULE, errorMessage);
					if (errorMessages == null)
						errorMessages = new ArrayList<>();
					errorMessages.add(errorMessage);
				} else {
					if (moduleList == null) {
						moduleList = new ArrayList<>();
						INDEXED_MODULES.put(descriptor.name(), moduleList);
					}
					moduleList.add(smd);
					Collections.sort(moduleList);
				}
			}
		} catch (FindException e) {
			if (registerExternModule || path.startsWith(ModuleManager.getPluginFolder()))
				errorMessages = List.of(e.getClass().getSimpleName() + ": " + e.getCause());
		}
		if (errorMessages != null)
			throw new IllegalArgumentException(errorMessages.stream().collect(Collectors.joining(System.lineSeparator())));
	}

	private static void removeToIndex(Path path) {
		markDirty(path); // To mark as dirty all depedencies
		INDEXED_MODULES.values().removeIf(l -> {
			boolean markDirty = isSameFile(l.get(0).path, path);
			if (markDirty)
				markDirty(l.get(0).path);
			l.removeIf(md -> isSameFile(md.path, path));
			if (l.isEmpty())
				return true;
			if (markDirty)
				markDirty(l.get(0).path);
			return false;
		});
	}

	private static void removeAllFromDirectoryToIndex(Path path, boolean onlyFather) {
		INDEXED_MODULES.values().removeIf(l -> {
			boolean markDirty = l.get(0).path.startsWith(path);
			if (markDirty)
				markDirty(l.get(0).path);
			l.removeIf(md -> onlyFather ? isSameFile(md.path.getParent(), path) : md.path.startsWith(path));
			if (l.isEmpty())
				return true;
			if (markDirty)
				markDirty(l.get(0).path);
			return false;
		});
	}

	private static void markDirty(Path path) {
		INDEXED_MODULES.values().stream().flatMap(l -> l.stream()).filter(smd -> {
			if (isSameFile(path, smd.path))
				return true;
			Tuple<Module, List<Path>> loadedModule = LOADED_MODULES.get(smd.descriptor.name());
			return loadedModule == null ? false : loadedModule.getSecond().contains(path);
		}).forEach(smd -> smd.dirty = true);
	}

	private static void updateLoadedModules() {
		Scenarium.runTaskAndWait(() -> {
			ArrayList<SimpleModuleDescriptor> toReload = new ArrayList<>();
			HashSet<String> verifiedModules = new HashSet<>();
			INDEXED_MODULES.values().stream().map(l -> l.get(0)).filter(smd -> smd.scenariumPlugin).forEach(smd -> {
				if (smd.dirty)
					if (LOADED_MODULES.containsKey(smd.descriptor.name()))
						toReload.add(smd);
					else
						registerModule(smd);
				verifiedModules.add(smd.descriptor.name());
			});
			if (!toReload.isEmpty())
				safelyReloadModules(toReload, null);
			// Need to collect and then iterate on each of them to avoid ConcurrentModificationException
			LOADED_MODULES.keySet().stream().filter(loadedModuleName -> !verifiedModules.contains(loadedModuleName)).collect(Collectors.toList())
					.forEach(toRemoveModule -> unregisterModule(LOADED_MODULES.get(toRemoveModule).getFirst()));
			ArrayList<Path> externalModulesLoaded = new ArrayList<>();
			for (String loadedModuleName : LOADED_MODULES.keySet()) {
				SimpleModuleDescriptor moduleDescriptor = INDEXED_MODULES.get(loadedModuleName).get(0);
				if (!moduleDescriptor.intern)
					externalModulesLoaded.add(moduleDescriptor.path);
			}
			ScenariumProperties.get().setExternModules(externalModulesLoaded.toArray(Path[]::new));
			// System.out.println(indexedModules.values());
		});
	}

	private static List<SimpleModuleDescriptor> getModulesDescription(String name) {
		return INDEXED_MODULES.get(name);
	}

	private static SimpleModuleDescriptor getModuleDescription(Module module) {
		return INDEXED_MODULES.get(module.getName()).get(0);

	}

	private static boolean isSameFile(Path path, Path path2) {
		try {
			return Files.isSameFile(path, path2);
		} catch (IOException e) {
			return false;
		}
	}

	public static Path getPluginFolder() {
		return pluginFolder;
	}
}

class SimpleModuleDescriptor implements Comparable<SimpleModuleDescriptor> { // Record
	public final Path path;
	public final ModuleDescriptor descriptor;
	public final boolean scenariumPlugin;
	public final boolean intern;
	public boolean dirty;

	public SimpleModuleDescriptor(Path path, ModuleDescriptor descriptor, boolean registerExternModule) {
		this.path = path;
		this.descriptor = descriptor;
		this.intern = path.startsWith(ModuleManager.getPluginFolder());
		this.scenariumPlugin = ModuleManager.isScenariumPlugin(descriptor) && (this.intern || registerExternModule);
		this.dirty = true;
	}

	@Override
	public int compareTo(SimpleModuleDescriptor o) {
		int type = -Boolean.compare(this.scenariumPlugin, o.scenariumPlugin);
		return type != 0 ? type : ModuleManager.compare(this.descriptor.version(), o.descriptor.version());
	}

	@Override
	public String toString() {
		return "SMD [p=" + this.path + ", n=" + this.descriptor.toNameAndVersion() + ", sp=" + this.scenariumPlugin + ", im=" + this.intern + ", dirty=" + this.dirty + "]";
	}
}
