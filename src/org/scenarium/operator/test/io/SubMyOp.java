/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test.io;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;

public class SubMyOp {
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	private int vache = 1;
	private SubMyOp subProp;

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		if (Arrays.asList(this.pcs.getPropertyChangeListeners()).contains(listener))
			System.out.println("remove ok");
		this.pcs.removePropertyChangeListener(listener);
	}

	public int getVache() {
		return this.vache;
	}

	public void setVache(int vache) {
		int oldVache = this.vache;
		this.vache = vache;
		this.pcs.firePropertyChange("vache", oldVache, vache);
	}

	public SubMyOp getSubProp() {
		return this.subProp;
	}

	public void setSubProp(SubMyOp subProp) {
		SubMyOp oldValue = this.subProp;
		this.subProp = subProp;
		this.pcs.firePropertyChange("subProp", oldValue, subProp);
	}
}
