/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import java.io.File;
import java.util.Arrays;
import java.util.stream.Stream;

import org.beanmanager.tools.EventListenerList;
import org.scenarium.filemanager.DataLoader;
import org.scenarium.struct.ScenariumProperties;

import javafx.application.Application;

public class UserAgentStylesheetManager {
	private static final String USER_AGENT_STYLESHEET_FOLDER = "/theme";
	private static final EventListenerList<UserAgentStylesheetChange> LISTENERS = new EventListenerList<UserAgentStylesheetChange>();
	
	private UserAgentStylesheetManager() {}
	
	public static String[] getAvailableUserAgentStylesheets() {
		return Stream.concat(Stream.of(Application.STYLESHEET_CASPIAN, Application.STYLESHEET_MODENA),
				Arrays.stream(new File(DataLoader.class.getResource(USER_AGENT_STYLESHEET_FOLDER).getPath()).listFiles()).map(File::getName).filter(n -> n.endsWith(".css")).map(n -> n.substring(0, n.length() - 4)))
				.toArray(String[]::new);
	}
	
	public static void loadUserAgentStylesheet(String userAgentStylesheet) {
		String currentUserAgentStylesheet = Application.getUserAgentStylesheet();
		if(currentUserAgentStylesheet == null){
			if(userAgentStylesheet.equals(Application.STYLESHEET_MODENA))
				return;
		}else if(currentUserAgentStylesheet.equals(userAgentStylesheet))
			return;
		Arrays.stream(getAvailableUserAgentStylesheets()).filter(usa -> usa.equals(userAgentStylesheet)).findAny().ifPresent(newUserAgentStylesheet -> {
			ScenariumProperties.get().setLookAndFeel(newUserAgentStylesheet);
			if (!newUserAgentStylesheet.equals(Application.STYLESHEET_CASPIAN) && !newUserAgentStylesheet.equals(Application.STYLESHEET_MODENA))
				newUserAgentStylesheet = DataLoader.class.getResource(USER_AGENT_STYLESHEET_FOLDER + "/" + newUserAgentStylesheet + ".css").toString();
			Application.setUserAgentStylesheet(newUserAgentStylesheet);
			LISTENERS.forEach(UserAgentStylesheetChange::userAgentStylesheetChange);
		});
	}
	
	public static void addUserAgentStylesheetChangeListener(UserAgentStylesheetChange listener) {
		LISTENERS.addStrongRef(listener);
	}

	public static void removeUserAgentStylesheetChangeListener(UserAgentStylesheetChange listener) {
		LISTENERS.removeStrongRef(listener);
	}
}
