/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.bugSize;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLWarning {

	private XMLWarning() {}

	public static void main(String[] args) {
		try {
			Document dom = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Element rootEle = dom.createElement("doc");
			Element e = dom.createElement("val1");
			e.appendChild(dom.createTextNode("val2"));
			rootEle.appendChild(e);
			dom.appendChild(rootEle);
			TransformerFactory.newInstance().newTransformer().transform(new DOMSource(dom), new StreamResult(new FileOutputStream("Test.xml")));
		} catch (ParserConfigurationException | FileNotFoundException | TransformerException | TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
	}
}
