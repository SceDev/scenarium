/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.output;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.scenarium.struct.raster.DoubleRaster;

public class DoubleRasterOutputStream extends DataFlowOutputStream<DoubleRaster> {
	private byte[] tempData;

	@Override
	public void push(DoubleRaster raster) throws IOException {
		this.dataOutput.writeInt(raster.getWidth());
		this.dataOutput.writeInt(raster.getHeight());
		this.dataOutput.writeInt(raster.getType());
		double[] pixels = raster.getData();
		if (this.tempData == null || this.tempData.length != pixels.length * Double.BYTES)
			this.tempData = new byte[pixels.length * Double.BYTES];
		ByteBuffer.wrap(this.tempData).asDoubleBuffer().put(pixels);
		this.dataOutput.write(this.tempData);
	}
}
