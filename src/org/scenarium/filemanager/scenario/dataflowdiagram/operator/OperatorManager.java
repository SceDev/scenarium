/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.WeakHashMap;

import org.beanmanager.testbean.TypeTest;
import org.scenarium.Scenarium;
import org.scenarium.operator.dataprocessing.DataAnalyzer;
import org.scenarium.operator.dataprocessing.DataGenerator;
import org.scenarium.operator.dataprocessing.Expression;
import org.scenarium.operator.dataprocessing.PerlinNoise;
import org.scenarium.operator.dataprocessing.ReSampler;
import org.scenarium.operator.dataprocessing.Smoothing;
import org.scenarium.operator.dataprocessing.Synchronizer;
import org.scenarium.operator.dataprocessing.image.Convolution;
import org.scenarium.operator.dataprocessing.image.Crop;
import org.scenarium.operator.dataprocessing.image.Gain;
import org.scenarium.operator.dataprocessing.image.ImageMuxer;
import org.scenarium.operator.dataprocessing.image.ImageToVideo;
import org.scenarium.operator.dataprocessing.image.Noiser;
import org.scenarium.operator.dataprocessing.image.Rescale;
import org.scenarium.operator.dataprocessing.image.Reverse;
import org.scenarium.operator.dataprocessing.image.Threshold;
import org.scenarium.operator.dataprocessing.image.conversion.ImgDecoder;
import org.scenarium.operator.dataprocessing.image.conversion.ImgEncoder;
import org.scenarium.operator.dataprocessing.image.conversion.TypeConverter;
import org.scenarium.operator.dataprocessing.vector.Devectorizer;
import org.scenarium.operator.dataprocessing.vector.Vectorizer;
import org.scenarium.operator.network.Controller;
import org.scenarium.operator.network.MjpegStreamer;
import org.scenarium.operator.network.bus.can.CAN;
import org.scenarium.operator.network.bus.ethernet.SocketTCP;
import org.scenarium.operator.network.bus.ethernet.SocketUDP;
import org.scenarium.operator.network.bus.serial.RXTXSerial;
import org.scenarium.operator.network.decoder.CanDBCDecoder;
import org.scenarium.operator.network.decoder.NMEA0183Decoder;
import org.scenarium.operator.network.encoder.CanDBCEncoder;
import org.scenarium.operator.recorder.CSVRecorder;
import org.scenarium.operator.recorder.ImageStackRecorder;
import org.scenarium.operator.recorder.Mp4Recorder;
import org.scenarium.operator.recorder.Recorder;
import org.scenarium.operator.speaker.Beep;
import org.scenarium.operator.test.ArrayGenerator;
import org.scenarium.operator.test.B1;
import org.scenarium.operator.test.B2;
import org.scenarium.operator.test.CompA;
import org.scenarium.operator.test.CompB;
import org.scenarium.operator.test.CompC;
import org.scenarium.operator.test.CompD;
import org.scenarium.operator.test.CompE;
import org.scenarium.operator.test.DoubleConsumer;
import org.scenarium.operator.test.LifeGame;
import org.scenarium.operator.test.ListenerTest;
import org.scenarium.operator.test.MyOperator;
import org.scenarium.operator.test.PCTest;
import org.scenarium.operator.test.PersonBean;
import org.scenarium.operator.test.RasterGenerator;
import org.scenarium.operator.test.RmiOperatorTest;
import org.scenarium.operator.test.io.EvolvedIOTest;
import org.scenarium.operator.test.io.SimpleIOTest;
import org.scenarium.operator.viewer.FlowViewer;
import org.scenarium.operator.viewer.Histogram;
import org.scenarium.operator.viewer.Oscilloscope;
import org.scenarium.operator.viewer.Viewer;
import org.scenarium.operator.viewer.conversion.ToCrossMarker;
import org.scenarium.operator.wrapper.COrCpp;
import org.scenarium.operator.wrapper.Simulink;

public class OperatorManager {
	private static WeakHashMap<Class<?>, Boolean> operatorListCache = new WeakHashMap<>();
	private static final ArrayList<Class<?>> OPERATORS = new ArrayList<>();

	static {
		// addInternOperator(Patate.class);
		// addInternOperator(SubClass.class);
		addOperator(Threshold.class);
		addOperator(Rescale.class);
		addOperator(Mp4Recorder.class);
		addOperator(MjpegStreamer.class);
		addOperator(Controller.class);
		addOperator(Gain.class);
		addOperator(ImageMuxer.class);
		addOperator(Convolution.class);
		addOperator(Crop.class);
		addOperator(Histogram.class);
		addOperator(ImageStackRecorder.class);
		addOperator(ImageToVideo.class);
		addOperator(ImgEncoder.class);
		addOperator(ImgDecoder.class);
		addOperator(Reverse.class);
		addOperator(TypeConverter.class);
		addOperator(Noiser.class);
		addOperator(CAN.class);
		addOperator(CanDBCDecoder.class);
		addOperator(CanDBCEncoder.class);
		addOperator(SocketUDP.class);
		addOperator(SocketTCP.class);
		addOperator(NMEA0183Decoder.class);
		addOperator(RXTXSerial.class);
		addOperator(ToCrossMarker.class);
		// addInternOperator(JSerialComm.class);
		addOperator(Oscilloscope.class);
		addOperator(PerlinNoise.class);
		addOperator(DataAnalyzer.class);
		addOperator(Beep.class);
		addOperator(Viewer.class);
		addOperator(FlowViewer.class);
		addOperator(Recorder.class);
		addOperator(Synchronizer.class);
		addOperator(ReSampler.class);
		addOperator(CSVRecorder.class);
		addOperator(Vectorizer.class);
		addOperator(Devectorizer.class);
		addOperator(Expression.class);
		addOperator(Simulink.class);
		addOperator(COrCpp.class);
		addOperator(DataGenerator.class);

		if (Scenarium.DEBUG_MODE) {
			addOperator(TypeTest.class);
			addOperator(LifeGame.class);
			addOperator(MyOperator.class);
			addOperator(PCTest.class);
			addOperator(PersonBean.class);
			addOperator(RmiOperatorTest.class);
			addOperator(ArrayGenerator.class);
			addOperator(EvolvedIOTest.class);
			addOperator(SimpleIOTest.class);
			addOperator(CompA.class);
			addOperator(CompB.class);
			addOperator(CompC.class);
			addOperator(CompD.class);
			addOperator(CompE.class);
			addOperator(DoubleConsumer.class);
			addOperator(B1.class);
			addOperator(B2.class);
			addOperator(Smoothing.class);
			addOperator(RasterGenerator.class);
			addOperator(ListenerTest.class);
		}
	}

	private OperatorManager() {}

	/** Add an operator to ScenariumFx. The operator must have a birth, a death method without parameters and without return value and a process method to be accepted.
	 *
	 * @param operator the operator to add to ScenariumFx */
	public static void addOperator(Class<?> operator) {
		if (OperatorManager.isOperator(operator))
			OPERATORS.add(operator);
		else
			System.err.println(operator + " is not an operator, an operator have no contructor, a public birth and death method without argument and with no return, and a public process method");
	}

	public static void purgeOperators(Module module) {
		OPERATORS.removeIf(op -> op.getModule().equals(module));
	}

	/** Return a copy of the list of all operators of ScenariumFx.
	 *
	 * @return a copy of the list of all operators */
	public static ArrayList<Class<?>> getOperators() {
		return new ArrayList<>(OPERATORS);
	}

	/** Tell if the operator class is an ScenariumFx valid operator. A valid operator must have a birth, a death method without parameters and without return value and a process method to be accepted.
	 *
	 * @param operatorClass the class to test
	 * @return true if the operatorClass is a valid ScenariumFx operator */
	public static boolean isOperator(Object operatorClass) {
		if (operatorClass == null)
			return false;
		Class<?> type = operatorClass instanceof Class<?> ? (Class<?>) operatorClass : operatorClass.getClass();
		Boolean isOperator = operatorListCache.get(type);
		if (isOperator != null)
			return isOperator;
		boolean isBirthMethod = false;
		boolean isProcessMethod = false;
		boolean isDeathMethod = false;
		for (Method method : type.getMethods()) {
			String methoName = method.getName();
			if (methoName.equals("birth") && method.getParameterTypes().length == 0)
				isBirthMethod = true;
			else if (methoName.equals("process"))
				isProcessMethod = true;
			else if (methoName.equals("death") && method.getParameterTypes().length == 0)
				isDeathMethod = true;
			if (isBirthMethod && isProcessMethod && isDeathMethod) {
				operatorListCache.put(type, true);
				return true;
			}
		}
		operatorListCache.put(type, false);
		return false;
	}

	public static void invokeBirth(Object operator) throws IllegalAccessException, Throwable {
		invokeLifeMethod(operator, true);
	}

	public static void invokeDeath(Object operator) throws IllegalAccessException, Throwable {
		invokeLifeMethod(operator, false);
	}
	
	private static void invokeLifeMethod(Object operator, boolean birth) throws IllegalAccessException, Throwable {
		Method declaredMethod = operator.getClass().getMethod(birth ? "birth" : "death", new Class<?>[0]);
		declaredMethod.setAccessible(true);
		MethodHandles.lookup().unreflect(declaredMethod).invoke(operator);
	}
}
