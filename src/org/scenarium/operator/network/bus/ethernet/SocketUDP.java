/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.network.bus.ethernet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.scenarium.operator.network.bus.DataType;

public class SocketUDP extends AbstractEthernet implements DynamicEnableBean {
	@PropertyInfo(index = 0, nullable = false, info = "The desired ethernet adresse to use\nIf no ip is specified, this block will be considered as the client")
	private InetSocketAddress address = new InetSocketAddress(0);

	private byte[] data = new byte[this.receiveBufferSize];
	private byte[] dpData = new byte[1];

	private DatagramSocket datagramSocket;

	public SocketUDP() {
		try {
			this.address = new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 0);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public SocketUDP(DataType dataTypeIn, DataType dataTypeOut, boolean connectionFlag, boolean verbose, int timeOut, int bufferSize, InetSocketAddress inetSocketAddress) {
		super(dataTypeIn, dataTypeOut, connectionFlag, verbose, timeOut, bufferSize);
		if (inetSocketAddress == null)
			throw new IllegalArgumentException("the adress cannot be null");
		this.address = inetSocketAddress;
	}

	public InetSocketAddress getAddress() {
		return this.address;
	}

	@Override
	protected boolean isInputAvailable() {
		try {
			return this.address.getAddress() == null ? false : !this.address.getAddress().equals(InetAddress.getByName("0.0.0.0"));
		} catch (UnknownHostException e) {
			return false;
		}
	}

	@Override
	protected boolean isOutputAvailable() {
		try {
			return this.address.getAddress() == null ? false : this.address.getAddress().equals(InetAddress.getByName("0.0.0.0"));
		} catch (UnknownHostException e) {
			return false;
		}
	}

	public void process() {
		Object in = getAdditionalInputs()[0];
		DatagramSocket datagramSocket = this.datagramSocket;
		if (datagramSocket != null) {
			try {
				if (this.address.getAddress().equals(InetAddress.getByName("0.0.0.0")))
					if (this.verbose)
						System.err.println(getBlockName() + ": cannot send data in client mode");
			} catch (UnknownHostException ex) {
				return;
			}
			try {
				byte[] sendBuf = null;
				if (this.inDataType == DataType.OBJECT) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					try (ObjectOutputStream os = new ObjectOutputStream(baos)) {
						os.writeObject(in);
						os.flush();
						sendBuf = baos.toByteArray();
					}
				} else if (in instanceof byte[] && this.inDataType == DataType.BYTE)
					sendBuf = (byte[]) in;
				else if (in instanceof String && this.inDataType == DataType.STRING)
					sendBuf = ((String) in).getBytes();
				if (sendBuf != null) {
					DatagramPacket pack = new DatagramPacket(sendBuf, sendBuf.length, this.address);
					pack.setData(sendBuf);
					datagramSocket.send(pack);
				}
			} catch (IOException e) {
				if (this.verbose)
					System.err.println(getBlockName() + ": " + e.getMessage());
				start();
			}
		}
	}

	public void setAddress(InetSocketAddress address) {
		if (address != null) {
			this.address = address;
			setEnable();
			restartAndReloadStructLater();
		}
	}

	@Override
	public void setEnable() {
		try {
			boolean isClient = this.address.getAddress() == null ? true : this.address.getAddress().equals(InetAddress.getByName("0.0.0.0"));
			fireSetPropertyEnable(this, "inDataType", this.address == null ? false : !isClient);
			fireSetPropertyEnable(this, "outDataType", this.address == null ? false : isClient);
		} catch (UnknownHostException e) {}
	}

	@Override
	protected synchronized void start() {
		death();
		this.ethernetThread = new Thread(() -> {
			this.data = new byte[this.receiveBufferSize];
			while (isAlive()) {
				if (Thread.currentThread().isInterrupted()) {
					stop();
					return;
				}
				do {
					try {
						if (this.datagramSocket == null) {
							this.datagramSocket = this.address.getAddress().equals(InetAddress.getByName("0.0.0.0")) ? new DatagramSocket(this.address.getPort()) : new DatagramSocket();
							this.datagramSocket.setSoTimeout(this.timeOut);
							connectionEstablished(true);
						}
						if (this.outDataType == DataType.DISABLE)
							return;
					} catch (IOException e) {
						if (this.verbose)
							System.err.println(getBlockName() + ": " + e.getMessage());
						if (!isAlive())
							return;
						if (this.datagramSocket == null)
							try {
								Thread.sleep(this.timeOut);
							} catch (InterruptedException ex) {}
					}
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
				} while (this.datagramSocket == null && isAlive());
				while (isAlive() && this.datagramSocket != null) {
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
					try {
						DatagramPacket dp = new DatagramPacket(this.data, this.data.length);
						this.datagramSocket.receive(dp);
						if (this.dpData.length != dp.getLength())
							this.dpData = new byte[dp.getLength()];
						System.arraycopy(dp.getData(), dp.getOffset(), this.dpData, 0, this.dpData.length);
						if (this.outDataType == DataType.STRING)
							triggerData(new String(this.dpData));
						else if (this.outDataType == DataType.BYTE)
							triggerData(this.dpData);
						else if (this.outDataType == DataType.OBJECT)
							try (ObjectInputStream iStream = new ObjectInputStream(new ByteArrayInputStream(this.dpData))) {
								Object data = iStream.readObject();
								triggerData(data);
							}
					} catch (IOException | NullPointerException | ClassNotFoundException e) {
						if (isAlive()) {
							if (this.verbose)
								System.err.println(getBlockName() + ": " + e.getMessage());
							if (!e.getClass().equals(SocketTimeoutException.class))
								stop();
						}
					}
				}
			}
		});
		this.ethernetThread.start();
	}

	@Override
	protected synchronized void stop() {
		this.dpData = new byte[1];
		if (this.datagramSocket != null) {
			this.datagramSocket.close();
			connectionEstablished(false);
			this.datagramSocket = null;
		}
	}
}
