Scenarium
=========

Scenarium is a platform to create, edit and execute flow diagrams.
Scenarium permit to interconnect the functionals blocks with a simple and efficient GUI.
It includes a collection of tools for data acquisition, data recording, data visualization and data processing.

This program is developed by ```Marc Revilloud``` and free (license MPL-2)

Scenarium can work on:
  - Windows
  - MacOS
  - Linux

Dependencies:
=============

Scenarium depend on the last version of java. The development cycle is Rolling-release only.

Java
----

Install the last Java with JavaFX: https://bell-sw.com/pages/java-14 with LibericaFX

Eclipse
-------

Install the last version: https://www.eclipse.org/downloads/

Install Sources
===============

[Installation process](doc/download_project_gui.md)

Tutorials:
==========

[global overview](doc/overview_system.md)
[contribution guideline](doc/contribution_guideline.md)

practices:
  - 01: [First run](doc/tutorial_01_first_run.md)
  - 02: [Dynamic parameter](doc/tutorial_02_dynamic_parameter.md)
  - 03: [Read Video and process data](doc/tutorial_03_video_processing.md)
  - 04: [Record and replay](doc/tutorial_04_replay.md)
  - 05: [Create a plug-in library](doc/tutorial_05_create_a_library_to_create_your_operators.md)
  - 06: [Write a block](doc/tutorial_06_write_a_new_block.md)
  - 07: [JNI Bridge](doc/tutorial_07_JNI_bridge.md)



