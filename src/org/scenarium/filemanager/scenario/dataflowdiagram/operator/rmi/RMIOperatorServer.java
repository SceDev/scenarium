/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.rmi.RemoteException;

import org.beanmanager.rmi.server.RMIBean;
import org.beanmanager.rmi.server.RMIBeanCallBackImpl;
import org.beanmanager.rmi.server.RMIBeanServer;
import org.scenarium.ModuleManager;
import org.scenarium.Scenarium;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.timescheduler.VisuableSchedulable;

public class RMIOperatorServer extends RMIBeanServer {
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) throws RemoteException {
		if (args.length < 4) {
			System.err.println(
					"RMIOperatorServer must be launch with at least 4 arguments:\n-port: the port for the remote bean\n-terminateIfParentTerminate: tell if the RMIBeanServer must terminate if the father process terminate\n-verbose: Explicitly displays in the console all operations performed\n-localDir: The local directory for Scenarium");
			System.exit(-1);
		}
		Scenarium.initScenarium(args[3]);
		ModuleManager.loadEmbeddedAndInternalModules();
		if(args.length > 4 && args[4] != null)
			ModuleManager.registerExternalModules(Path.of(args[4]));
		RMIBeanServer.launch(new String[] { args[0], args[1], args[2] }, new RMIOperatorServer());
	}

	protected RMIOperatorServer() throws RemoteException {
		super();
	}

	@Override
	protected RMIBean getRMIObject(Class<?> type, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		if (VisuableSchedulable.class.isAssignableFrom(type))
			return new RMIVisuableSchedulableOperator(type, identifier, callBack);
		else if (Scenario.class.isAssignableFrom(type))
			return new RMIScenarioOperator(type, identifier, callBack);
		else
			return new RMIOperator(type, identifier, callBack);
	}

	@Override
	protected RMIBeanServer getRMIServer() throws RemoteException {
		return new RMIOperatorServer();
	}
}
