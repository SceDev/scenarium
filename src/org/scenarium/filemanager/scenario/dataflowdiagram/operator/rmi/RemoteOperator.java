/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.awt.image.BufferedImage;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import org.beanmanager.BeanManager;
import org.beanmanager.rmi.client.RemoteBean;
import org.scenarium.ModuleManager;
import org.scenarium.Scenarium;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.ProcessMode;
import org.scenarium.filemanager.scenario.dataflowdiagram.RMIOperatorCallBack;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.operator.test.DoubleConsumer;
import org.scenarium.timescheduler.VisuableSchedulable;

// Bench
// 1920x1080: In 32.945s send: 124416000000 octets 607.072393382911fps 3601.5281150402184 mo/s
// 960x540: In 10.307s send: 31104000000 octets 1940.4288347724846fps 2877.95536407296 mo/s
// 480x270: In 3.496s send: 7776000000 octets 5720.823798627002fps 2121.2160996495995 mo/s
// 240x135: In 3.068s send: 1944000000 octets 6518.904823989569fps 604.283856288706 mo/s
// 120x67: In 2.143s send: 482400000 octets 9332.711152589829fps 214.67685031935372 mo/s
// 60x33: In 2.425s send: 118800000 octets 8247.422680412372fps 46.720209809922686 mo/s
// 30x16: In 1.964s send: 28800000 octets 10183.299389002037fps 13.984633560336048 mo/s
// 15x8: In 1.867s send: 7200000 octets 10712.372790573112fps 3.677801327329941 mo/s
// 10 thread:
// 1920x1080: In 38.68s send: 12441600000 octets 51.70630816959669fps 306.75373254912097 mo/s
// 960x540: In 15.33s send: 3110400000 octets 130.4631441617743fps 193.4969728473581 mo/s
// 480x270: In 5.645s send: 777600000 octets 354.295837023915fps 131.36884826173605 mo/s
// 240x135: In 3.571s send: 194400000 octets 560.0672080649678fps 51.91663038627135 mo/s
// 120x67: In 2.433s send: 48240000 octets 822.0304151253597fps 18.90885697634094 mo/s
// 60x33: In 2.059s send: 11880000 octets 971.3453132588635fps 5.502501641042374 mo/s
// 30x16: In 1.924s send: 2880000 octets 1039.5010395010395fps 1.4275374382796258 mo/s
// 15x8: In 1.577s send: 720000 octets 1268.2308180088776fps 0.43541249702758406 mo/s
// 40 thread:
// 1920x1080: In 167.263s send: 12441600000 octets 11.957217077297429fps 70.93759154744325 mo/s
// 960x540: In 54.066s send: 3110400000 octets 36.991824806717716fps 54.86458391133059 mo/s
// 480x270: In 22.717s send: 777600000 octets 88.03979398688207fps 32.644149686908484 mo/s
// 240x135: In 14.901s send: 194400000 octets 134.2191799208107fps 12.441734588911817 mo/s
// 120x67: In 10.239s send: 48240000 octets 195.33157534915517fps 4.493138883039115 mo/s
// 60x33: In 9.221s send: 11880000 octets 216.89621516104543fps 1.2286791973653888 mo/s
// 30x16: In 7.846s send: 2880000 octets 254.90695895997962fps 0.3500614365600306 mo/s
// 15x8: In 7.35s send: 720000 octets 272.108843537415fps 0.09342115752551021 mo/s

public class RemoteOperator extends RemoteBean {
	public static RemoteOperator createRemoteOperator(Block block, boolean eraseCallBackIfPresent) throws Exception {
		return createRemoteOperator(block, eraseCallBackIfPresent, -1);
	}

	public static RemoteOperator createRemoteOperator(Block block, boolean eraseCallBackIfPresent, int timeout) throws Exception {
		Object op = block.getOperator();
		String hostName;
		int port;
		if (block.getProcessMode() == ProcessMode.ISOLATED) {
			hostName = InetAddress.getLocalHost().getHostName();
			port = block.getIsolatePort();
		} else {
			InetSocketAddress remoteIp = block.getRemoteIp();
			hostName = remoteIp.getHostName();
			port = remoteIp.getPort();
		}
		boolean createRemoteOperator = block.getProcessMode() == ProcessMode.ISOLATED;
		RMIOperatorCallBack cb = new RMIOperatorCallBack(block);
		RemoteOperator remoteOperator;
		if (op instanceof VisuableSchedulable)
			remoteOperator = new VisuableSchedulableRemoteOperator(op.getClass(), block.getName(), hostName, port, createRemoteOperator, cb, eraseCallBackIfPresent, timeout);
		else if (op instanceof Scenario)
			remoteOperator = new ScenarioRemoteOperator(op.getClass(), block.getName(), hostName, port, createRemoteOperator, cb, eraseCallBackIfPresent, timeout);
		else
			remoteOperator = new RemoteOperator(op.getClass(), block.getName(), hostName, port, createRemoteOperator, cb, eraseCallBackIfPresent, timeout);
		return remoteOperator;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			int id = i;
			// new Thread(() -> {
			try {
				RMIOperatorCallBack cb = new RMIOperatorCallBack(null);
				System.out.println("create");
				RemoteOperator ro;
				try {
					ro = new RemoteOperator(DoubleConsumer.class, null, InetAddress.getLocalHost().getHostName(), 1100 + id, false, cb, false);
					System.out.println("create ok");
					try {
						ro.birth();
						int nbImage = 1000;
						int width = 1920 / 128;
						int height = 1080 / 128;
						Object[] data = new Object[] { new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR) };
						boolean[] link = new boolean[] { true };
						long time = System.currentTimeMillis();
						for (int j = 0; j < nbImage; j++)
							// System.out.println(id + " " + j);
							ro.process(data, null, link);// new double[960*600*3]}
						nbImage *= 2;
						double timeSpend = (System.currentTimeMillis() - time) / 1000.0;
						long imageSize = width * height * 3;
						System.out.println(width + "x" + height + ": In " + timeSpend + "s send: " + imageSize * nbImage + " octets " + nbImage / timeSpend + "fps "
								+ imageSize * nbImage / timeSpend / (1024 * 1024) + " mo/s");
						ro.death();
					} catch (Throwable e) {
						e.printStackTrace();
					}
					ro.destroy();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (IllegalArgumentException | IOException e) {
				e.printStackTrace();
			}
			System.out.println("end of thread");
			// }).start();
		}
	}

	private final ConcurrentHashMap<Thread, Long> callingThreadId = new ConcurrentHashMap<>();
	private final ArrayList<SerializableWrapper> processSerializableWrapperPool = new ArrayList<>();

	private int processSerializableWrapperPoolCpt = 0;

	private Class<?>[] inputsClass = new Class<?>[0];

	private Class<?>[] additionalInputsClass = new Class<?>[0];

	public RemoteOperator(Class<?> operatorClass, String identifier, String hostName, int port, boolean createRemoteOperator, RMIOperatorCallBack callBack, boolean eraseCallBackIfPresent)
			throws Exception {
		this(operatorClass, identifier, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent, -1);
	}

	public RemoteOperator(Class<?> operatorClass, String identifier, String hostName, int port, boolean createRemoteOperator, RMIOperatorCallBack callBack, boolean eraseCallBackIfPresent, int timeout)
			throws Exception {
		super(operatorClass, identifier, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent, timeout, getAdditionalArgs(operatorClass));
		if (this.remoteBean != null)
			((RMIOperatorImpl) this.remoteBean).setMainThreadId(Thread.currentThread().getId());
	}

	private static String[] getAdditionalArgs(Class<?> operatorClass) {
		ArrayList<String> args = new ArrayList<>();
		args.add(Scenarium.workspaceDirectory());
		Module module = operatorClass.getModule();
		if (!ModuleManager.isBuiltInModule(module) && !ModuleManager.isInternalModule(module))
			args.add(ModuleManager.getModulePath(module));
		return args.toArray(String[]::new);
	}

	private static String[] asStringNameArray(Class<?>[] inputsClass) {
		String[] stringNameArray = new String[inputsClass.length];
		for (int i = 0; i < inputsClass.length; i++)
			if (inputsClass[i] != null)
				stringNameArray[i] = BeanManager.getDescriptorFromClass(inputsClass[i]);
		return stringNameArray;
	}

	public void birth() throws Throwable {
		((RMIOperatorImpl) this.remoteBean).birth(Thread.currentThread().getId());
	}

	public void death() throws Throwable {
		((RMIOperatorImpl) this.remoteBean).death(Thread.currentThread().getId());
	}

	public void fireBlockNameChanged(String beanDescName, String beanDescLocal, String oldName, int id) throws Throwable {
		((RMIOperatorImpl) this.remoteBean).fireBlockNameChanged(Thread.currentThread().getId(), beanDescName, beanDescLocal, oldName, id);
	}

	public void fireDeclaredInputChanged(String[] names, Class<?>[] types) throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		((RMIOperatorImpl) this.remoteBean).fireDeclaredInputChanged(Thread.currentThread().getId(), names, types);
	}

	public void fireDeclaredOutputChanged(String[] names, Class<?>[] types)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		((RMIOperatorImpl) this.remoteBean).fireDeclaredOutputChanged(Thread.currentThread().getId(), names, types);
	}

	public void fireInputLinksChangeChanged(int indexOfInput) throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		((RMIOperatorImpl) this.remoteBean).fireInputLinksChanged(Thread.currentThread().getId(), indexOfInput);
	}

	public void fireStructChanged() throws RemoteException {
		((RMIOperatorImpl) this.remoteBean).fireStructChanged(Thread.currentThread().getId());
	}

	public void fireVarArgsInputChanged(int indexOfInput, int typeOfChange)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		((RMIOperatorImpl) this.remoteBean).fireVarArgsInputChanged(Thread.currentThread().getId(), indexOfInput, typeOfChange);
	}

	@Override
	protected Class<?> getServer() {
		return RMIOperatorServer.class;
	}

	public boolean isFromThreadedBlock(long threadedBlockId) {
		return this.callingThreadId.getOrDefault(Thread.currentThread(), -1L) == threadedBlockId;
	}

	public boolean needToBeSaved() throws RemoteException {
		if (this.remoteBean == null)
			return false;
		return ((RMIOperatorImpl) this.remoteBean).needToBeSaved(Thread.currentThread().getId());
	}

	public void onStart() throws RemoteException {
		((RMIOperatorImpl) this.remoteBean).onStartCallBack(Thread.currentThread().getId());
	}

	public void onResume() throws RemoteException {
		((RMIOperatorImpl) this.remoteBean).onResumeCallBack(Thread.currentThread().getId());
	}

	public void onPause() throws RemoteException {
		((RMIOperatorImpl) this.remoteBean).onPauseCallBack(Thread.currentThread().getId());
	}

	public void onStop() throws RemoteException {
		((RMIOperatorImpl) this.remoteBean).onStopCallBack(Thread.currentThread().getId());
	}

	public Object[] process(Object[] inputs, Object[] additionalInputs, boolean[] linkedStaticOutput) throws Throwable {
		this.processSerializableWrapperPoolCpt = 0;
		boolean inputChanged = false;
		Class<?>[] newInputsClass = updateClassIfChanged(this.inputsClass, inputs);
		if (newInputsClass != null) {
			this.inputsClass = newInputsClass;
			inputChanged = true;
		}
		try {
			SerializableWrapper.serialize(inputs, this.processSerializableWrapperPool, this.processSerializableWrapperPoolCpt);
			if (additionalInputs != null) {
				newInputsClass = updateClassIfChanged(this.additionalInputsClass, additionalInputs);
				if (newInputsClass != null) {
					this.additionalInputsClass = newInputsClass;
					inputChanged = true;
				}
				SerializableWrapper.serialize(additionalInputs, this.processSerializableWrapperPool, this.processSerializableWrapperPoolCpt);
			} else if (this.additionalInputsClass.length != 0) {
				this.additionalInputsClass = new Class<?>[0];
				inputChanged = true;
			}
			try {
				Object[] staticOutputsArray = inputChanged
						? ((RMIOperatorImpl) this.remoteBean).processWithNewInputs(Thread.currentThread().getId(), asStringNameArray(this.inputsClass), inputs,
								asStringNameArray(this.additionalInputsClass), additionalInputs, linkedStaticOutput)
						: ((RMIOperatorImpl) this.remoteBean).process(Thread.currentThread().getId(), inputs, additionalInputs, linkedStaticOutput);
				SerializableWrapper.deserialize(staticOutputsArray);
				return staticOutputsArray;
			} catch (SocketException | RemoteException e) {
				System.err.println(this.remoteBean.remoteToString() + " cannot send data to remote operator, caused by: " + e.getMessage());
				// e.printStackTrace();
				return null;
			}
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	public void runLaterCallBack(int taskId) throws RemoteException {
		((RMIOperatorImpl) this.remoteBean).runLaterCallBack(Thread.currentThread().getId(), taskId);
	}

	public void setCallingThread(long callingThreadId) {
		this.callingThreadId.put(Thread.currentThread(), callingThreadId);
	}

	private static Class<?>[] updateClassIfChanged(Class<?>[] inputsClass, Object[] inputs) {
		boolean inputChanged = false;
		if (inputsClass.length != inputs.length) {
			inputsClass = new Class<?>[inputs.length];
			inputChanged = true;
		}
		for (int i = 0; i < inputs.length; i++) {
			Object input = inputs[i];
			if (input != null && input.getClass() != inputsClass[i]) {
				inputsClass[i] = input.getClass();
				inputChanged = true;
			}
		}
		return inputChanged ? inputsClass : null;
	}
}
