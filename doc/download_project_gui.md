Configure the workspace
=======================

Auto refresh the project with file system update:

Window -> Preferences: General/Workspace: **and check the option **‘Refresh using native hooks or polling’.

With this, the worktree will update when you modify the project file with an other application.

Download project:
=================

GUI: in Eclipse
---------------

  - Run Eclipse (select a new workspace)
  - File -> Import
      - Git -> Project From Git (With Smart Import)
      - ```==> Next```
      - Clone URI ```https://gitlab.com/SceDev/scenarium.git```
      - ```==> Next```
      - Select ```master```
      - ```==> Next```
      - Set destination in ```YOUR_WORKSPACE/scenarium```
      - Set ```Clone submodules``` to true
      - ```==> Next```
      - ```==> Finish```


In Console (Linux/Mac)
----------------------

```{.bash}
export MY_WORKSPACE="workspace"
mkdir ${MY_WORKSPACE}
cd ${MY_WORKSPACE}
git clone https://gitlab.com/SceDev/scenarium.git
cd scenarium
git submodule init
git submodule update
cd ..
```

Run eclipse and open new workspace ${MY_WORKSPACE} folder

**Note:** You need to add the beanmanager project with adding a local project in eclipse ```scenarium/beanmanager```

