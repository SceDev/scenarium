
/******************************************************************************* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors: Revilloud Marc - initial API and implementation ******************************************************************************/
import org.scenarium.PluginsSupplier;

/** @author revilloud */
open module Scenarium {
	uses PluginsSupplier;

	exports org.scenarium.test.versionconverter;
	exports org.scenarium.math;
	exports org.scenarium;
	exports org.scenarium.operator.network.bus;
	exports org.scenarium.display.toolBar;
	exports org.scenarium.tools;
	exports org.scenarium.filemanager.scenario.dataflowdiagram.operator;
	exports org.scenarium.operator.network;
	exports org.scenarium.updater;
	exports org.scenarium.communication.can.dbc;
	exports org.scenarium.filemanager.scenario.dataflowdiagram;
	exports org.scenarium.operator.dataprocessing;
	exports org.scenarium.math.thresholding;
	exports org.scenarium.display.toolbarclass;
	exports org.scenarium.filemanager;
	exports org.scenarium.operator.network.bus.serial;
	exports org.scenarium.editors.can;
	exports org.scenarium.editors;
	exports org.scenarium.tools.dynamiccompilation;
	exports org.scenarium.operator.network.bus.ethernet;
	exports org.scenarium.communication.can;
	exports org.scenarium.display.drawer.camera3D;
	exports org.scenarium.filemanager.datastream.input;
	exports org.scenarium.operator.network.encoder;
	exports org.scenarium.display.toolBar.receditor;
	exports org.scenarium.filemanager.datastream.output;
	exports org.scenarium.timescheduler;
	exports org.scenarium.struct;
	exports org.scenarium.display.drawer.geo;
	exports org.scenarium.struct.raster;
	exports org.scenarium.operator.test;
	exports org.scenarium.display.drawer.imagedrawers;
	exports org.scenarium.editors.can.dbc;
	exports org.scenarium.operator.dataprocessing.vector;
	exports org.scenarium.operator.network.decoder;
	exports org.scenarium.operator.dataprocessing.image.conversion;
	exports org.scenarium.filemanager.scenariomanager;
	exports org.scenarium.operator.viewer;
	exports org.scenarium.operator.recorder;
	exports org.scenarium.struct.curve;
	exports org.scenarium.operator.wrapper;
	exports org.scenarium.filemanager.datastream;
	exports org.scenarium.filemanager.scenario;
	exports org.scenarium.display.drawer;
	exports org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;
	exports org.scenarium.operator.dataprocessing.image;
	exports org.scenarium.operator.network.bus.can;
	exports org.scenarium.math.association;
	exports org.scenarium.operator.test.io;
	exports org.scenarium.math.noise;
	exports org.scenarium.filemanager.filerecorder;
	exports org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;
	exports org.scenarium.filemanager.playlist;
	exports org.scenarium.display;

	// Project
	requires transitive BeanManager;
	requires transitive CANPeak2016;
	requires Jcodec;

	// Default Java module
	requires transitive java.compiler;
	requires java.desktop;
	requires java.management;
	requires java.rmi;
	requires java.xml;
	requires transitive javafx.base;
	requires transitive javafx.controls;
	requires transitive javafx.graphics;
	requires transitive javafx.media;
	requires transitive javafx.swing;
	requires transitive javafx.web;
	requires java.base;
	requires jdk.httpserver;

	// Libraries compliant with Java module system
	requires transitive org.objectweb.asm;
	requires transitive com.sun.jna;
	requires com.sun.jna.platform;
	requires org.scenicview.scenicview;
	requires transitive vecmath;
	requires transitive JRxTx;
	// requires com.fazecast.jSerialComm;

	// Libraries not compliant with Java module system
	requires jimObjModelImporterJFX;
	requires transitive jogamp.fat;
	requires transitive marineapi;
	requires exp4j;
	requires ejml.core;
	requires ejml.ddense;
	requires jinput;
	// requires transitive RXTXcomm;
}