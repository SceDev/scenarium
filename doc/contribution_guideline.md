

Contribute to the development of Scenarium 
=======================

The 'master' branch is managed exclusively by Marc Revilloud. The 'dev' branch is managed by 'developers'. If you want to modify scenarium, you can fork it and then modify it.

If you want to propose bug fixes or improvements, you can make a 'merge request' into the dev branch.

If you want to get more involved, you can request to get the 'developer' status.

Coding Standards and Guidelines 
=======================

The scenarium project uses coding standards. It's recommended to respect these standards when you propose a 'merge request'. To follow these standards, you can configure Eclipse as described below.

- Formatter : The Eclipse formatter configuration file ('Formatter.xml') is located at the root of the project. You can import the formatter in Eclipse (Right click on the the scenarium projet, 'Properties' -> 'Java Code Style' -> 'Formatter' -> Import...)
- Cleaner : The Eclipse formatter configuration file ('CleanUp.xml') is located at the root of the project. You can import the cleaner in Eclipse (Right click on the the scenarium projet, 'Properties' -> 'Java Code Style' -> 'Clean up' -> Import...)
- CheckStyle : This part is made by an external Eclipse plugin (https://marketplace.eclipse.org/content/checkstyle-plug). You can install this plugin from the Eclipse marketplace, then import the CheckStyle configuration file in Eclipse ('Window' -> 'Preferences' -> 'Checkstyle' -> 'New...' -> 'Import...'), and finally, set it for your project (Right click on the the scenarium projet, 'Properties' -> 'Checkstyle' -> choose your checkstyle configuration) . The checkStyle configuration file 'Checkstyle.XML' is located at the root of the project.

Don't forget to specify your name and email address when you propose a 'merge request'. For Eclipse: 

1. Click **Window > Preferences > Team > Git > Configuration**

2. Click **Add Entry** and enter the key value pairs:

   - Key:  **user.name**

   - Value: *YourUsernameHere*

     And

   - Key:  **user.email**

   - Value: *YourEmailHere*