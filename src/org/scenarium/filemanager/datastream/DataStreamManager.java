/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream;

import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.scenarium.filemanager.datastream.input.BufferedImageInputStream;
import org.scenarium.filemanager.datastream.input.ByteRasterInputStream;
import org.scenarium.filemanager.datastream.input.DataFlowInputStream;
import org.scenarium.filemanager.datastream.input.IntegerRasterInputStream;
import org.scenarium.filemanager.datastream.input.PropertyInputStream;
import org.scenarium.filemanager.datastream.input.ShortRasterInputStream;
import org.scenarium.filemanager.datastream.output.BufferedImageOutputStream;
import org.scenarium.filemanager.datastream.output.ByteRasterOutputStream;
import org.scenarium.filemanager.datastream.output.DataFlowOutputStream;
import org.scenarium.filemanager.datastream.output.DoubleRasterOutputStream;
import org.scenarium.filemanager.datastream.output.FloatRasterOutputStream;
import org.scenarium.filemanager.datastream.output.IntegerRasterOutputStream;
import org.scenarium.filemanager.datastream.output.PropertyOutputStream;
import org.scenarium.filemanager.datastream.output.ShortRasterOutputStream;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.struct.raster.DoubleRaster;
import org.scenarium.struct.raster.FloatRaster;
import org.scenarium.struct.raster.IntegerRaster;
import org.scenarium.struct.raster.ShortRaster;

public final class DataStreamManager {
	private static final ConcurrentHashMap<Long, DataFlowInputMap> DATA_INPUT_STREAM_MAP = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<Long, DataFlowOutputMap> DATA_OUTPUT_STREAM_MAP = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<Class<?>, Class<? extends DataFlowInputStream<?>>> INPUT_STREAM_MAP = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<Class<?>, Class<? extends DataFlowOutputStream<?>>> OUTPUT_STREAM_MAP = new ConcurrentHashMap<>();
	private static HashMap<Class<?>, Boolean> hasOutputStream = new HashMap<>();

	private DataStreamManager() {}

	static {
		registerDataStream(BufferedImage.class, BufferedImageInputStream.class, BufferedImageOutputStream.class);
		registerDataStream(ByteRaster.class, ByteRasterInputStream.class, ByteRasterOutputStream.class);
		registerDataStream(ShortRaster.class, ShortRasterInputStream.class, ShortRasterOutputStream.class);
		registerDataStream(IntegerRaster.class, IntegerRasterInputStream.class, IntegerRasterOutputStream.class);
		registerDataStream(FloatRaster.class, FloatRasterInputStream.class, FloatRasterOutputStream.class);
		registerDataStream(DoubleRaster.class, DoubleRasterInputStream.class, DoubleRasterOutputStream.class);
	}

	public static <T> DataFlowInputStream<T> getInputStream(Class<T> type) {
		DataFlowInputMap threadDataInputStreamMap = DATA_INPUT_STREAM_MAP.get(Thread.currentThread().getId());
		if (threadDataInputStreamMap == null) {
			threadDataInputStreamMap = new DataFlowInputMap();
			DATA_INPUT_STREAM_MAP.put(Thread.currentThread().getId(), threadDataInputStreamMap);
		}
		DataFlowInputStream<T> inputStream = threadDataInputStreamMap.get(type);
		if (inputStream != null)
			return inputStream;
		@SuppressWarnings("unchecked")
		Class<? extends DataFlowInputStream<T>> inputStreamClass = (Class<? extends DataFlowInputStream<T>>) INPUT_STREAM_MAP.get(type);
		if (inputStreamClass != null)
			try {
				inputStream = inputStreamClass.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				return null;
			}
		if (inputStream == null) {
			DataFlowOutputMap threadDataOutputStreamMap = DATA_OUTPUT_STREAM_MAP.get(Thread.currentThread().getId());
			if (threadDataOutputStreamMap == null) {
				threadDataOutputStreamMap = new DataFlowOutputMap();
				DATA_OUTPUT_STREAM_MAP.put(Thread.currentThread().getId(), threadDataOutputStreamMap);
			}
			DataFlowOutputStream<T> dos = threadDataOutputStreamMap.get(type);
			if (dos != null && dos instanceof PropertyOutputStream)
				inputStream = new PropertyInputStream<>(((PropertyOutputStream<T>) dos).getEditor());
			else {
				PropertyEditor<T> pe = PropertyEditorManager.findEditor(type, "");
				if (pe != null)
					inputStream = new PropertyInputStream<>(pe);
			}
		}
		if (inputStream != null)
			threadDataInputStreamMap.put(type, inputStream);
		return inputStream;
	}

	public static <T> DataFlowOutputStream<T> getOutputStream(Class<T> type) {
		DataFlowOutputMap threadDataOutputStreamMap = DATA_OUTPUT_STREAM_MAP.get(Thread.currentThread().getId());
		if (threadDataOutputStreamMap == null) {
			threadDataOutputStreamMap = new DataFlowOutputMap();
			DATA_OUTPUT_STREAM_MAP.put(Thread.currentThread().getId(), threadDataOutputStreamMap);
		}
		DataFlowOutputStream<T> outputStream = threadDataOutputStreamMap.get(type);
		if (outputStream != null)
			return outputStream;
		@SuppressWarnings("unchecked")
		Class<? extends DataFlowOutputStream<T>> outputStreamClass = (Class<? extends DataFlowOutputStream<T>>) OUTPUT_STREAM_MAP.get(type);
		if (outputStreamClass != null)
			try {
				outputStream = outputStreamClass.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				System.err.println("Cannot construct DataFlowOutputStream class: " + outputStreamClass + " due to: " + e.getMessage());
				return null;
			}
		if (outputStream == null) {
			DataFlowInputMap threadDataInputStreamMap = DATA_INPUT_STREAM_MAP.get(Thread.currentThread().getId());
			if (threadDataInputStreamMap == null) {
				threadDataInputStreamMap = new DataFlowInputMap();
				DATA_INPUT_STREAM_MAP.put(Thread.currentThread().getId(), threadDataInputStreamMap);
			}
			DataFlowInputStream<T> dis = threadDataInputStreamMap.get(type);
			if (dis != null && dis instanceof PropertyInputStream)
				outputStream = new PropertyOutputStream<>(((PropertyInputStream<T>) dis).getEditor());
			else {
				PropertyEditor<T> pe = PropertyEditorManager.findEditor(type, "");
				if (pe != null)
					outputStream = new PropertyOutputStream<>(pe);
			}
		}
		if (outputStream != null)
			threadDataOutputStreamMap.put(type, outputStream);
		return outputStream;
	}

	public static boolean hasOutputStream(Class<? extends Object> type) {
		Boolean isProperty = hasOutputStream.get(type);
		if (isProperty == null) {
			isProperty = DataStreamManager.getOutputStream(type) != null;
			hasOutputStream.put(type, isProperty);
		}
		return isProperty;
	}

	public static <T> void registerDataStream(Class<T> type, Class<? extends DataFlowInputStream<T>> inputStream, Class<? extends DataFlowOutputStream<T>> outputStream) {
		if (type == null || inputStream == null || outputStream == null)
			throw new IllegalArgumentException("Type, inputStream and outputStream cannot be null");
		INPUT_STREAM_MAP.put(type, inputStream);
		OUTPUT_STREAM_MAP.put(type, outputStream);
	}

	public static void unregisterDataStream(Class<?> type) {
		INPUT_STREAM_MAP.remove(type);
		OUTPUT_STREAM_MAP.remove(type);
	}

	public static void purgeDataStream(Module module) {
		INPUT_STREAM_MAP.keySet().removeIf(c -> c.getModule().equals(module));
		OUTPUT_STREAM_MAP.keySet().removeIf(c -> c.getModule().equals(module));
	}
}

class DataFlowInputMap {
	private final HashMap<Class<?>, DataFlowInputStream<?>> map = new HashMap<>();

	@SuppressWarnings("unchecked")
	public <T> DataFlowInputStream<T> get(Class<T> key) {
		return (DataFlowInputStream<T>) this.map.get(key);
	}

	public <T> void put(Class<T> key, DataFlowInputStream<T> editor) {
		this.map.put(key, editor);
	}
}

class DataFlowOutputMap {
	private final HashMap<Class<?>, DataFlowOutputStream<?>> map = new HashMap<>();

	@SuppressWarnings("unchecked")
	public <T> DataFlowOutputStream<T> get(Class<T> key) {
		return (DataFlowOutputStream<T>) this.map.get(key);
	}

	public <T> void put(Class<T> key, DataFlowOutputStream<T> editor) {
		this.map.put(key, editor);
	}
}