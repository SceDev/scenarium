/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.TransientProperty;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.beanmanager.struct.TreeRoot;
import org.beanmanager.tools.FxUtils;
import org.scenarium.display.AnimationTimerConsumer;
import org.scenarium.display.FilterChangeListener;
import org.scenarium.display.PaintListener;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.display.TheaterFilterPropertyChange;
import org.scenarium.display.TheaterFilterStructChange;
import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.display.toolbarclass.ToolBoxItem;
import org.scenarium.operator.viewer.Viewer;
import org.scenarium.struct.BufferedStrategy;
import org.scenarium.timescheduler.Scheduler;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Dimension2D;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.Window;

@BeanPropertiesInheritanceLimit
public abstract class TheaterPanel extends Pane implements FilterChangeListener {
	private final EventListenerList listeners = new EventListenerList();
	protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	@TransientProperty
	private Object drawableElement;
	private Object[] additionalDrawableElement;
	private ScenariumContainer container;
	private Dimension2D oldDataSize;
	@TransientProperty
	private int theaterEditorSelection;
	@TransientProperty
	private Scheduler scheduler;
	private int scenarioWidth; // largeur du scenario
	private int scenarioHeight; // hauteur du scenario
	@PropertyInfo(hidden = true)
	private TreeRoot<BooleanProperty> theaterFilter;
	private boolean ignoreRepaint = true;
	private boolean animated = false;
	private boolean needToBeRefresh;

	public void initStruct(Viewer viewer) {}

	public void initialize(ScenariumContainer container, Scheduler scheduler, Object drawableElement, boolean autoFitIfResize) {
			if (drawableElement == null)
				throw new IllegalArgumentException("drawableElement must be non null");
			setDrawableElement(drawableElement);
			this.scheduler = scheduler;
			this.container = container;
			setFocusTraversable(true);
			// Redraw canvas when size changes.
			ChangeListener<Number> es = (a, b, c) -> {
				if (autoFitIfResize)
					fitDocument();
				else
					repaint(false);
			};
			widthProperty().addListener(es);
			heightProperty().addListener(es);
			addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
				if (container != null && container.isStatusBar())
					container.updateStatusBar(getStatusBarInfo());
			});
			addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
				if (!e.isConsumed())
					requestFocus();
			}); // Bug de focus sur rename block sinon -> Handler pour corriger, Bug de focus info chartdrawer si pas
			addEventFilter(ScrollEvent.SCROLL, e -> {
				if (e.getDeltaY() != 0)
					if (this.getScheduler() != null)
						if (e.isShiftDown()) {
							this.getScheduler().jump(e.getDeltaY() < 0);
							e.consume();
						} else if (!e.isControlDown()) {
							this.getScheduler().moveSpeed(e.getDeltaY() > 0);
							e.consume();
						}
			});
			addEventHandler(KeyEvent.KEY_PRESSED, e -> {
				if (e.isConsumed())
					return;
				KeyCode keyCode = e.getCode();
				switch (keyCode) {
				case SPACE:
					if (this.getScheduler() != null)
						if (e.isControlDown())
							new Thread(() -> TheaterPanel.this.getScheduler().stop()).start();
						else if (this.getScheduler().isRunning())
							this.getScheduler().pause();
						else
							new Thread(() -> TheaterPanel.this.getScheduler().start()).start();
					// this.scheduler.setRunning(e.isControlDown() ? false : !this.scheduler.isRunning(), e.isControlDown());
					break;
				case ADD:
					if (this.getScheduler() != null)
						this.getScheduler().moveSpeed(true);
					break;
				case SUBTRACT:
					if (this.getScheduler() != null)
						this.getScheduler().moveSpeed(false);
					break;
				case ENTER:
					if (e.isAltDown())
						changeFullScreen();
					break;
				default:
					return;
				}
				e.consume();
			});
	//		this.roi[2] = 50;
	//		this.roi[3] = 50;
			if (this.theaterFilter == null) {
				this.theaterFilter = new TreeRoot<>(BooleanProperty.class);
				populateTheaterFilter(this.theaterFilter);
				updateTheaterFilter();
			}
		}

	public void death() {
		this.container = null;
		this.scheduler = null;
		this.drawableElement = null;
		getChildren().clear();
		// widthProperty().removeListener(listener);
		// heightProperty().removeListener(listener);
	}

	public abstract void fitDocument();

	public abstract Dimension2D getDimension();

	public String geticonName() {// TODO faire les icones pour les autres
		return "/scenarium_icon_viewer.png";
	}

	public Dimension2D getPreferredSize() {
		return new Dimension2D(getWidth(), getHeight());
	}

	public abstract String[] getStatusBarInfo();

	public int getTheaterEditorSelection() {
		return this.theaterEditorSelection;
	}

	public ArrayList<ToolBoxItem> getToolBoxItems() {
		return new ArrayList<>();
	}

	public boolean hasTheaterFilterStructChanged() {
		return this.listeners.getListeners(TheaterFilterStructChange.class).length != 0;
	}

	public void populateExclusiveFilter(ArrayList<List<String>> exclusiveFilters) {}

	public void repaint(boolean autoFitSize) {
		if (this.ignoreRepaint)
			return;
		if (this.animated) {
			this.needToBeRefresh = true;
			return;
		}
		paintImmediately(autoFitSize);
	}

	public void paintImmediately(boolean autoFitSize) { // only for viewer
		this.needToBeRefresh = false;
		Dimension2D dim = getDimension();
		if (this.oldDataSize == null || !this.oldDataSize.equals(dim)) {
			this.oldDataSize = dim;
			if (autoFitSize)
				this.container.adaptSizeToDrawableElement();
			else
				fitDocument();
		}
		Object drawableElement = this.drawableElement;
		if (drawableElement == null)
			return;
		if (!(drawableElement instanceof BufferedStrategy)) // ATTENTION rajout de synchro aurel qui correspont au cas sans flip sur la synchro data de update scenario
			synchronized (drawableElement) {
				FxUtils.runLaterIfNeeded(() -> paint(drawableElement));
			}
		else
			FxUtils.runLaterIfNeeded(() -> paint(drawableElement));
		firePaint();
	}

	public void setAdditionalDrawableElement(Object[] additionalDrawableElement) {
		this.additionalDrawableElement = additionalDrawableElement;
	}

	public void setAnimated(boolean animated) {
		this.animated = animated;
	}
	
	public void setAnimationTimerConsumer(AnimationTimerConsumer animationTimerConsumer) {}

	public void showMessage(String message, boolean error) {
		this.container.showMessage(message, error);
	}

	@Override
	public boolean isResizable() {
		return true;
	}

	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
	}

	public final void updateFilterWithPath(String[] path, boolean value, boolean updateTheater) {
		if (updateTheater)
			filterChanged(path, value);
		if (!updateFilterWithPath(path, value))
			return;
		fireTheaterFilterPropertyChanged(path);
	}

	@Override
	public void filterChanged(String[] path, boolean value) {
		TreeNode<BooleanProperty> node = this.theaterFilter;
		int i = 0;
		boolean sucess = false;
		while (i != path.length) {
			sucess = false;
			for (TreeNode<BooleanProperty> son : node.getChildren())
				if (son.getValue().name.equals(path[i])) {
					node = son;
					i++;
					sucess = true;
					break;
				}
			if (!sucess)
				break;
		}
		if (sucess)
			node.getValue().value = value;
	}

	public void updateTheaterFilter() {
		if (this.theaterFilter.getChildren() == null)
			return;
		LinkedList<TreeNode<BooleanProperty>> todoList = new LinkedList<>();
		LinkedList<ArrayList<String>> todoListPath = new LinkedList<>();
		todoList.add(this.theaterFilter);
		todoListPath.add(new ArrayList<>());
		while (!todoList.isEmpty()) {
			TreeNode<BooleanProperty> node = todoList.pop();
			ArrayList<String> nodePath = todoListPath.pop();
			for (TreeNode<BooleanProperty> treeNode : node.getChildren())
				if (treeNode.getChildren() != null) {
					ArrayList<String> path = new ArrayList<>(nodePath);
					path.add(treeNode.getValue().name);
					todoList.push(treeNode);
					todoListPath.push(path);
				} else {
					ArrayList<String> path = new ArrayList<>(nodePath);
					path.add(treeNode.getValue().name);
					updateFilterWithPath(path.toArray(String[]::new), treeNode.getValue().value, false);
				}
		}
	}

	// protected void updateToolView(int toolId, boolean isVisible) {
	// container.updateToolView(toolId, isVisible);
	// }

	public void setIgnoreRepaint(boolean ignoreRepaint) {
		this.ignoreRepaint = ignoreRepaint;
	}

	public boolean isNeedToBeRefresh() {
		return needToBeRefresh;
	}

	public void setTheaterEditorSelection(int theaterEditorSelection) {
		this.theaterEditorSelection = theaterEditorSelection;
	}

	public TreeRoot<BooleanProperty> getTheaterFilter() {
		return this.theaterFilter;
	}

	public void setTheaterFilter(TreeRoot<BooleanProperty> theaterFilter) {
		if (this.theaterFilter == null) {
			this.theaterFilter = new TreeRoot<>(BooleanProperty.class);
			populateTheaterFilter(this.theaterFilter);
	
			if (theaterFilter != null && theaterFilter.getChildren() != null) { // Chargement
				LinkedList<TreeNode<BooleanProperty>> todoListOri = new LinkedList<>();
				LinkedList<TreeNode<BooleanProperty>> todoListSave = new LinkedList<>();
				todoListOri.push(this.theaterFilter);
				todoListSave.push(theaterFilter);
				while (!todoListOri.isEmpty()) {
					TreeNode<BooleanProperty> oriNode = todoListOri.pop();
					TreeNode<BooleanProperty> saveNode = todoListSave.pop();
					if (oriNode.getChildren() == null && saveNode.getChildren() == null && saveNode.getValue() != null)
						oriNode.setValue(saveNode.getValue());
					else if (oriNode.getChildren() != null && saveNode.getChildren() != null)
						for (TreeNode<BooleanProperty> oriTreeNode : oriNode.getChildren()) {
							String oriSonName = oriTreeNode.getValue().name;
							TreeNode<BooleanProperty> correspondingSaveNode = null;
							for (TreeNode<BooleanProperty> seekNode : saveNode.getChildren())
								if (oriSonName.equals(seekNode.getValue().name)) {
									correspondingSaveNode = seekNode;
									break;
								}
							if (correspondingSaveNode != null) {
								todoListOri.push(oriTreeNode);
								todoListSave.push(correspondingSaveNode);
							}
						}
				}
			}
			updateTheaterFilter();
		}
	}

	public Object getDrawableElement() {
		return this.drawableElement;
	}

	public void setDrawableElement(Object drawableElement) {
		this.drawableElement = drawableElement;
		Dimension2D dim = getDimension();
		if (dim != null) {
			this.scenarioWidth = (int) dim.getWidth();
			this.scenarioHeight = (int) dim.getHeight();
			if (this.scenarioHeight == -1)
				getDimension();
		} else {
			this.scenarioWidth = 800;
			this.scenarioHeight = 600;
		}
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public void addPaintListener(PaintListener listener) {
		this.listeners.add(PaintListener.class, listener);
	}

	public void removePaintListener(PaintListener listener) {
		this.listeners.remove(PaintListener.class, listener);
	}

	public void addTheaterFilterPropertyChangeListener(TheaterFilterPropertyChange listener) {
		this.listeners.add(TheaterFilterPropertyChange.class, listener);
	}

	public void removeTheaterFilterPropertyChangeListener(TheaterFilterPropertyChange listener) {
		this.listeners.remove(TheaterFilterPropertyChange.class, listener);
	}

	public void addTheaterFilterStructChangeListener(TheaterFilterStructChange listener) {
		this.listeners.add(TheaterFilterStructChange.class, listener);
	}

	public void removeTheaterFilterStructChangeListener(TheaterFilterStructChange listener) {
		this.listeners.remove(TheaterFilterStructChange.class, listener);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	protected int getScenarioWidth() {
		return scenarioWidth;
	}

	protected int getScenarioHeight() {
		return scenarioHeight;
	}

	protected void setScenarioSize(int width, int height) {
		scenarioWidth = width;
		scenarioHeight = height;
	}

	protected void changeFullScreen() {
		Window windows = getScene().getWindow();
		if (windows instanceof Stage) {
			Stage stage = (Stage) windows;
			stage.setFullScreen(!stage.isFullScreen());
		}
	}

	protected Object[] getAdditionalDrawableElement() {
		return this.additionalDrawableElement;
	}

	protected TreeNode<BooleanProperty> getTheaterSon(TreeNode<BooleanProperty> father, String propName) {
		for (TreeNode<BooleanProperty> son : father.getChildren())
			if (son.getValue().name.equals(propName))
				return son;
		return null;
	}

	protected boolean isRunning() {
		return this.getScheduler() != null && this.getScheduler().isRunning();
	}

	protected boolean isStopped() {
		return this.getScheduler() == null || this.getScheduler().isStopped();
	}

	protected abstract void paint(Object dataElement);

	protected void populateTheaterFilter(TreeRoot<BooleanProperty> theaterFilter) {}

	protected void saveScenario() {
		this.container.saveScenario();
	}

	protected void showTool(Class<? extends Tool> toolClass) {
		this.container.showTool(toolClass);
	}

	protected abstract boolean updateFilterWithPath(String[] filterPath, boolean value);

	protected void fireTheaterFilterStructChanged() {
		for (TheaterFilterStructChange listener : this.listeners.getListeners(TheaterFilterStructChange.class))
			listener.theaterFilterStructChanged();
	}

	private void fireTheaterFilterPropertyChanged(String[] path) {
		for (TheaterFilterPropertyChange listener : this.listeners.getListeners(TheaterFilterPropertyChange.class))
			listener.theaterFilterPropertyChanged(path);
	}

	private void firePaint() {
		for (PaintListener listener : this.listeners.getListeners(PaintListener.class))
			listener.paint(this);
	}

	// protected void updateToolView(int toolId, boolean isVisible) {
	// container.updateToolView(toolId, isVisible);
	// }
	
	public static boolean canAddInputToRenderer(Class<? extends StackableDrawer> type, Class<?>[] class1) {
		try {
			return type.getConstructor().newInstance().canAddInputToRenderer(class1);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			// System.err.println("No default protected constructor for the class: " + type.getSimpleName() + "\nCannot check add input possibilities");
			System.err.println(e.getClass().getSimpleName() + ": " + e.getMessage());
			return false;
		}
	}

	public static Object getImageFromData(Object scenarioData) {
		return null;
	}

	public static boolean isValidAdditionalInput(Class<? extends StackableDrawer> type, Class<?>[] inputsType, Class<?> additionalInput) {
		try {
			return additionalInput == null ? false : type.getConstructor().newInstance().isValidAdditionalInput(inputsType, additionalInput);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			// System.err.println("No default protected constructor for the class: " + type.getSimpleName() + "\nCannot check input validities");
			System.err.println(e.getClass().getSimpleName() + ": " + e.getMessage());
			return false;
		}
	}
}
