Tutorial 4: Record and replay a flow
====================================

We consider now that you are able to create alone a Scenario.
Then create the next simple scénario.

![basic scenario](tutorial_04/basic_flow.png)

_**Note:**_ We use the first perlin nose to generate automatic datas.

Record:
-------

Now we will record the flow:
  - Add operator: ```Basic/Recorder```
  - Connect the output of the block ```PerlinNoise-0```

![add recorder](tutorial_04/add_record.png)

We need to select an output of the recorder (recorder property):

![configure recorder](tutorial_04/configure_recorder.png)

Now your can save your scenario to open it later ```File->Save Scenario As...``` like "xxx_record.pdfd"

At every time you play the scenario a new entry of record is generated.

_**Note:**_ The record is in row, then pay attention whan you record image stream.


Replay:
-------

Simply do:
  - Record your scenario as a new scenario file ==> permit to switch between record and replay. ```File->Save Scenario As...``` like "xxx_replay.pdfd"
  - Remove the recorder operator
  - Remove the Data generator operator
  - Remove the PerlinNoise-0 operator
  - Add the operator ```Scenario/ScenariumScheduler```

![regenerate scenario](tutorial_04/regenerate.png)

_**Note:**_ The scheduler does not have output data ==> need to select the record to generate output elements

Configure your scheduler (operator property):

![configure scheduler](tutorial_04/configure_scheduler.png)

Now link your recorded stream to the old operators

All is ready ro replay.

  - Open the player console and enjoy

![player console](tutorial_04/player.png)

  - **1**: Play pause button
  - **2**: Stop simulation
  - **3**: Play reverse or forward
  - **4**: Previous/next Record in the playlist
  - **5**: Slowly/Faster: Change the speed of the simulation
  - **6**: **In pause only:** Play the previous/next frame of data
  - **7**: Automatic replay of the simulation
  - **8**: Configure cycle for replay data (marker A/B can be change with a drag and drop)

