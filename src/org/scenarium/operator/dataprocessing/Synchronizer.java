/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.dataprocessing;

import java.util.Arrays;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.LinkChangeListener;
import org.scenarium.operator.test.TimeStampSynchronizer;

public class Synchronizer extends EvolvedOperator implements LinkChangeListener {
	private int nbData;
	@PropertyInfo(index = 0, info = "Choose the type of the synchronizer.\nTIMESTAMP : Timestamp synchronisation trigger out the data of the same timeStamp.\nALL_AND_LAST_ARRIVED trigger output when data arrived on every inputs.")
	private SynchronizerType synchronizerType;
	@PropertyInfo(index = 1, nullable = false, info = "Nombre de données maximale stocké par input pour la synchronisation temporel.")
	private int[] nbDataMax = new int[0];
	@PropertyInfo(index = 2, nullable = false, info = "Difference de timestamp maximale pour faire une association. Ne devrais pas être supérieur à la moitié de la période minimal des inputs.")
	private int nMaxDelta = 0;


	private Object[] objectToSend;
	private long[] timeStamps; // Buffer uses for input or output timeStamps

	private TimeStampSynchronizer timeStampSync;
	private boolean full;

	@Override
	public void initStruct() {
		String[] oltin = getOutputLinkToInputNames();
		Class<?>[] oltit = getOutputLinkToInputTypes();
		this.nbData = oltin.length - 1;
		String[] names = new String[this.nbData];
		Class<?>[] types = new Class<?>[this.nbData];
		for (int i = 0; i < types.length; i++) {
			names[i] = oltin[i] + "-" + i;
			types[i] = oltit[i];
			if (types[i] == null)
				return;
		}
		updateOutputs(names, types);
		this.objectToSend = new Object[this.nbData];
		this.timeStamps = new long[this.objectToSend.length];
	}

	@Override
	public void linkChanged(int indexOfInput) {
		initStruct();
	}

	@Override
	public void birth() {
		this.timeStampSync = this.synchronizerType == SynchronizerType.TIMESTAMP
				? new TimeStampSynchronizer(this.nbData, this::getTimeStamp, this.nbDataMax,this.nMaxDelta)
				: null;
	}

	public void process(Object... data) {
		this.full = false;
		if (this.timeStampSync != null) {
			this.timeStampSync.feed((objects, ts) -> {
				this.objectToSend = objects;
				Arrays.fill(this.timeStamps, ts);
			}, data);
			if (this.objectToSend != null)
				this.full = true;
		} else {
			this.full = true;
			for (int i = 0; i < data.length; i++) {
				if (data[i] != null)
					this.objectToSend[i] = data[i];
				if (this.objectToSend[i] == null)
					this.full = false;
			}
			Arrays.fill(this.timeStamps, getMaxTimeStamp());
		}
		if (this.full) {
			triggerOutput(this.objectToSend, this.timeStamps);
			Arrays.fill(this.objectToSend, null);
			Arrays.fill(this.timeStamps, getMaxTimeStamp());
		}
	}

	public SynchronizerType getSynchronizerType() {
		return this.synchronizerType;
	}

	public void setSynchronizerType(SynchronizerType synchronizerType) {
		var oldValue = this.synchronizerType;
		this.synchronizerType = synchronizerType;
		restartLater();
		this.pcs.firePropertyChange("synchronizerType", oldValue, synchronizerType);
	}

	public int[] getNbDataMax() {
		return this.nbDataMax;
	}

	public void setNbDataMax(int[] nbDataMax) {
		var oldValue = this.nbDataMax;
		this.nbDataMax = nbDataMax;
		restartLater();
		this.pcs.firePropertyChange("nbDataMax", oldValue, nbDataMax);
	}

	@Override
	public void death() {
	}
	
	public int getnMaxDelta() {
		return nMaxDelta;
	}

	public void setnMaxDelta(int nMaxDelta) {
		var oldValue = this.nMaxDelta;
		this.nMaxDelta = nMaxDelta;
		restartLater();
		this.pcs.firePropertyChange("nMaxDelta", oldValue, nMaxDelta);
	}

}
