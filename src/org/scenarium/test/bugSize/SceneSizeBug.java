/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.bugSize;

import org.scenicview.ScenicView;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class SceneSizeBug extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Button button = new Button("test");
		button.setOnAction(e -> {
			button.setPrefSize(2000, 2000);
			primaryStage.sizeToScene();
		});
		button.setPrefSize(1280, 1024);
		Scene s = new Scene(button, -1, -1);
		primaryStage.setX(0);
		primaryStage.setY(0);
		primaryStage.setScene(s);
		ScenicView.show(s);
		primaryStage.show();
	}

}
