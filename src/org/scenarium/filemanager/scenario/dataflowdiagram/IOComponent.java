/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.util.List;

public interface IOComponent {
	public boolean canTriggerOrBeTriggered();

	public IOLinks[] getIndex();

	public List<? extends Input> getInputs();

	public Object[] getOutputBuff();

	public long[] getOutputBuffTs();

	public List<? extends Output> getOutputs();

	public List<? extends Output> getTriggerableOutputs();

	public boolean isReady();
}
