/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

public class CycleMapTileSource implements TileSource {

	@Override
	public int getMaxZoom() {
		return 22;
	}

	@Override
	public int getMinZoom() {
		return 0;
	}

	@Override
	public String getPrefix() {
		return "CycleMap";
	}

	@Override
	public String getTileUrl(int zoom, int tilex, int tiley) {
		// System.out.println("file:///home/revilloud/workspace/MagicHat/cache/riegl/" + zoom + "_" + tilex + "_" + tiley + ".png");
		// return "file:///home/revilloud/workspace/MagicHat/cache/riegl/" + zoom + "_" + tilex + "_" + tiley + ".png";//
		return "http://a.tile.opencyclemap.org/cycle/" + zoom + "/" + tilex + "/" + tiley + ".png";
	}
}
