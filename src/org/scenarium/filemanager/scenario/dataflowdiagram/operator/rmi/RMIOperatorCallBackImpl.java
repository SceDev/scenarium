/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIOperatorCallBackImpl extends Remote {
	/**** Listeners methods *******/
	void addBlockNameChangeListener(int listenerId) throws RemoteException;

	void addDeclaredInputChangeListener(int listenerId) throws RemoteException;

	void addDeclaredOutputChangeListener(int listenerId) throws RemoteException;

	void addInputLinksChangeListener(int listenerId) throws RemoteException;

	void addStructChangeListener(int listenerId) throws RemoteException;

	void addVarArgsInputChangeListener(int listenerId) throws RemoteException;

	String getBlockName() throws RemoteException;

	int getInputIndex(String inputName) throws RemoteException;

	String[] getInputsName() throws RemoteException;

	/****** RS ***/
	long getMaxTimeStamp() throws RemoteException;

	int getNbTriggerableInput() throws RemoteException;

	int getNbTriggerableOutput() throws RemoteException;

	/****** RS info ******/
	int getOutputIndex(String outputName) throws RemoteException;

	String[] getOutputLinkToInputName() throws RemoteException;

	Class<?>[] getOutputLinkToInputType() throws RemoteException;

	String[] getTriggerableOutputsName() throws RemoteException;

	long getTimeOfIssue(int indexOfInput) throws RemoteException;

	long getTimeStamp(int indexOfInput) throws RemoteException;

	String getWarning() throws RemoteException;

	String getError() throws RemoteException;

	boolean isPropertyAsInput(String propertyName) throws RemoteException;

	boolean isPropertyAsOutput(String propertyName) throws RemoteException;

	boolean isRunning() throws RemoteException;

	void onStart(int listenerId) throws RemoteException;

	void onResume(int listenerId) throws RemoteException;

	void onPause(int listenerId) throws RemoteException;

	void onStop(int listenerId) throws RemoteException;

	void removeBlockNameChangeListener(int listenerId) throws RemoteException;

	void removeDeclaredInputChangeListener(int listenerId) throws RemoteException;

	void removeDeclaredOutputChangeListener(int listenerId) throws RemoteException;

	void removeInputLinksChangeListener(int listenerId) throws RemoteException;

	void removeStructChangeListener(int listenerId) throws RemoteException;

	void removeVarArgsInputChangeListener(int listenerId) throws RemoteException;

	void runLater(long callingThreadId, int taskId) throws RemoteException;

	void setError(String error) throws RemoteException;

	boolean addPropertyAsInput(long callingThreadId, String propertyName) throws RemoteException;

	boolean removePropertyAsInput(long callingThreadId, String propertyName) throws RemoteException;

	boolean addPropertyAsOutput(long callingThreadId, String propertyName) throws RemoteException;

	boolean removePropertyAsOutput(long callingThreadId, String propertyName) throws RemoteException;

	void setWarning(String warning) throws RemoteException;

	/****** RS trigger ******/
	void triggerProperty(long callingThreadId, String outputPropertyName, Object outputValue) throws RemoteException;

	boolean triggerOutput(long callingThreadId, Object outputValue) throws RemoteException;

	boolean triggerOutput(long callingThreadId, Object outputValue, long timeStamp) throws RemoteException;

	boolean triggerOutput(long callingThreadId, Object[] outputValues) throws RemoteException;

	boolean triggerOutput(long callingThreadId, Object[] outputValues, long timeStamp) throws RemoteException;

	boolean triggerOutput(long callingThreadId, Object[] outputValues, long[] timeStamps) throws RemoteException;

	/***** Struct changed *****/
	boolean updateInputs(long callingThreadId, String[] names, Class<?>[] types) throws RemoteException;

	boolean updateOutputs(long callingThreadId, String[] names, Class<?>[] types) throws RemoteException;
}
