package org.scenarium.operator.test;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.struct.OldObject3D;

public class PCTest extends EvolvedOperator{
	public void birth() {
		onStart(() -> triggerOutput(new OldObject3D(OldObject3D.POINTS, new float[] {5, 0, 1}, 3)));
	}
	
	public OldObject3D process() {
		return null;
	}
	
	public void death() {}
}
