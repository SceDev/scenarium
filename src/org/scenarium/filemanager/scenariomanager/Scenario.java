/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenariomanager;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.editors.DynamicVisibleBean;
import org.beanmanager.editors.TransientProperty;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.struct.BufferedStrategy;
import org.scenarium.timescheduler.Schedulable;
import org.scenarium.timescheduler.SchedulerInterface;

import javafx.beans.InvalidationListener;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

@BeanPropertiesInheritanceLimit
public abstract class Scenario extends EvolvedOperator implements Schedulable, DynamicVisibleBean {
	public static final String MINUTE_PATTERN = "ss.SSS";
	public static final String HOUR_PATTERN = "mm:ss.SSS";
	public static final String DAY_PATTERN = "HH:mm:ss.SSS";
	public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final int LOADED = 0;
	public static final int SCHEDULED = 1;
	protected ReadOnlyDoubleProperty progressProperty;
	private final EventListenerList listeners = new EventListenerList();
	protected Object scenarioData;
	@TransientProperty
	protected SchedulerInterface schedulerInterface;
	private TheaterPanel theaterPanel;
	private Polygon polygon;

	public void addLoadListener(LoadListener listener) {
		this.listeners.add(LoadListener.class, listener);
	}

	public void addScheduleChangeListener(ScheduleChangeListener listener) {
		this.listeners.add(ScheduleChangeListener.class, listener);
	}

	public void addSourceChangeListener(SourceChangeListener listener) {
		for (Object list : this.listeners.getListenerList())
			if (listener == list)
				return;
		this.listeners.add(SourceChangeListener.class, listener);
	}

	public void addSourceChangeListenerIfNotPresent(SourceChangeListener listener) {
		for (Object list : this.listeners.getListenerList())
			if (listener == list)
				return;
		addSourceChangeListener(listener);
	}

	public boolean canModulateSpeed() {
		return true;
	}

	public boolean canReverse() {
		return true;
	}

	protected abstract boolean close();

	@Override
	public void death() {
		close();
		this.scenarioData = null;
	}

	protected void fireLoadChanged() {
		for (LoadListener listener : this.listeners.getListeners(LoadListener.class))
			listener.scenarioLoaded();
	}

	protected void fireScheduleChanged() {
		for (ScheduleChangeListener listener : this.listeners.getListeners(ScheduleChangeListener.class))
			listener.scheduleChange();
	}

	protected void fireSourceChanged() {
		for (SourceChangeListener listener : this.listeners.getListeners(SourceChangeListener.class))
			listener.sourceChanged();
	}

	public abstract long getBeginningTime();

	public abstract long getEndTime();

	public abstract Date getStartTime();

	public abstract Date getStopTime();

	public abstract Class<?> getDataType();

	public LinkedHashMap<String, String> getInfo() throws IOException {
		LinkedHashMap<String, String> info = new LinkedHashMap<>();
		getInfo(info);
		return info;
	}

	public abstract void getInfo(LinkedHashMap<String, String> info) throws IOException;

	@Override
	public Region getNode() {
		this.polygon = new Polygon();
		this.polygon.setFill(Color.TRANSPARENT);
		this.polygon.setStrokeLineCap(StrokeLineCap.ROUND);
		this.polygon.setStrokeLineJoin(StrokeLineJoin.ROUND);
		StackPane sp = new StackPane(this.polygon);
		sp.setMouseTransparent(true);
		sp.setFocusTraversable(false);
		InvalidationListener il = e -> updatePolyPoints(this.polygon, sp.getWidth(), sp.getHeight());
		sp.heightProperty().addListener(il);
		sp.widthProperty().addListener(il);
		updateColor();
		return sp;
	}

	protected void updateColor() {
		if (this.polygon != null) {
			Color color = getColor();
			Stop[] stops = new Stop[] { new Stop(0, new Color(color.getRed(), color.getGreen(), color.getBlue(), 130 / 255.0)),
					new Stop(1, new Color(color.getRed(), color.getGreen(), color.getBlue(), 30 / 255.0)) };
			LinearGradient lg = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
			this.polygon.setStroke(lg);
		}
	}

	protected Color getColor() {
		return new Color(0, 0.7, 0, 0);
	}

	public ReadOnlyDoubleProperty getProgressProperty() {
		return this.progressProperty;
	}

	protected abstract String[] getReaderFormatNames();

	public Object getScenarioData() {
		return this.scenarioData;
	}

	public SchedulerInterface getSchedulerInterface() {
		return this.schedulerInterface;
	}

	public abstract int getSchedulerType();

	@TransientProperty
	public abstract Object getSource();

	public Schedulable getTaskFromId(int id) {
		return this;
	}

	public int getTaskId(Schedulable task) {
		return 0;
	}

	public abstract void initOrdo();

	@Override
	public void initStruct() throws Exception {
		Class<?> type = getDataType();
		if (type != null)
			updateOutputs(new String[] { type.getSimpleName() }, new Class<?>[] { type });
		else
			updateOutputs(new String[] {}, new Class<?>[] {});
	}

	public boolean isPaintableProperties() {
		return false;
	}

	public boolean isStreamScenario() {
		return this instanceof StreamScenario;
	}

	// protected abstract boolean isTimeRepresentation();

	public abstract void load(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException;

	public abstract void populateInfo(LinkedHashMap<String, String> info) throws IOException;

	public abstract void process(Long timePointer) throws Exception;

	protected void reload() {
		try {
			synchroLoad(getSource(), false);
			// fireLoaded();
		} catch (IOException | ScenarioException e) {
			e.printStackTrace();
		}
	}

	public void removeLoadListener(LoadListener listener) {
		this.listeners.remove(LoadListener.class, listener);
	}

	public void removeScheduleChangeListener(ScheduleChangeListener listener) {
		this.listeners.remove(ScheduleChangeListener.class, listener);
	}

	public void removeSourceChangeListener(SourceChangeListener listener) {
		this.listeners.remove(SourceChangeListener.class, listener);
	}

	public abstract void save(File file) throws IOException;

	public void setScheduler(SchedulerInterface schedulerInterface) {
		this.schedulerInterface = schedulerInterface;
		if (schedulerInterface != null)
			initOrdo();
	}

	public abstract void setSource(Object source);

	// public Scheduler getScheduler() {
	// return scheduler;
	// }

	public void setTheaterPanel(TheaterPanel theaterPanel) {
		this.theaterPanel = theaterPanel;
	}

	public boolean stopAndWait() {
		return this.schedulerInterface.stopAndWait();
	}

	public void synchroClose() {
		Callable<Boolean> closeTask = () -> {
			synchronized (this) {
				return close();
			}
		};
		try {
			if (this.schedulerInterface != null) {
				this.schedulerInterface.synchronisedCall(closeTask); // Il ne faut pas que le scheduler s'arrête pendant le rechargement
				this.schedulerInterface.removeScheduleElement(this);
			} else
				closeTask.call();
		} catch (Exception e) {} // Impossible normalement

	}

	public void synchroLoad(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException {
		Callable<Boolean> loadTask = () -> {
			synchronized (this) {
				load(scenarioSource, backgroundLoading);
				return true;
			}
		};
		try {
			if (this.schedulerInterface != null)
				this.schedulerInterface.synchronisedCall(loadTask);
			else
				loadTask.call();
		} catch (IOException | ScenarioException e) {
			throw e;
		} catch (Exception e) {} // Impossible normalement

	}

	private boolean synchroUpdate(long timePointer) {
		Object oldScenarioData = this.scenarioData;
		try {
			process(timePointer);
			if (this.scenarioData instanceof BufferedStrategy)
				((BufferedStrategy<?>) this.scenarioData).flip();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		if (this.theaterPanel != null) {
			if (oldScenarioData != this.scenarioData)
				this.theaterPanel.setDrawableElement(this.scenarioData);
			this.theaterPanel.repaint(true);
		}
		return true;
	}

	@Override
	public synchronized void update(long timePointer) {
		if (this.scenarioData == null) {
			System.err.println("no scenario data for " + this);
			return;
		}
		boolean success;
		if (this.scenarioData instanceof BufferedStrategy<?>) {
			BufferedStrategy<?> rasterStrategy = (BufferedStrategy<?>) this.scenarioData;
			if (rasterStrategy.isPageFlipping())
				success = synchroUpdate(timePointer);
			else
				synchronized (rasterStrategy.getDrawElement()) {
					success = synchroUpdate(timePointer);
				}
		} else
			synchronized (this.scenarioData) {
				success = synchroUpdate(timePointer);
			}
		if (success && isAlive())
			triggerOutput(new Object[] { this.scenarioData instanceof BufferedStrategy<?> ? ((BufferedStrategy<?>) this.scenarioData).getDrawElement() : this.scenarioData },
					this.schedulerInterface.getTimeStamp());
	}

	private static void updatePolyPoints(Polygon polygon, double width, double height) {
		ObservableList<Double> points = polygon.getPoints();
		double gapx = 0.85f;
		double gapy = 0.95f;
		double roundSize = width / 4;
		double minX = (1 - gapx) * width + roundSize;
		points.clear();
		points.addAll(minX, (1 - gapy) * width + roundSize, width - roundSize, width / 2, minX, gapy * width - roundSize);
		polygon.setStrokeWidth(roundSize);
	}

	public String getTimePattern() {
		LocalDateTime endDate = Instant.ofEpochMilli(getEndTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
		LocalDateTime beginningDate = Instant.ofEpochMilli(getBeginningTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
		if (!endDate.truncatedTo(ChronoUnit.DAYS).equals(beginningDate.truncatedTo(ChronoUnit.DAYS)))
			return DATE_PATTERN;
		return getEndTime() < 1000 * 60 ? MINUTE_PATTERN : getEndTime() < 1000 * 60 * 60 ? HOUR_PATTERN : DAY_PATTERN;
	}

	public boolean canRecord() {
		return false;
	}

	public boolean isRecording() {
		return false;
	}

	public void setRecording(boolean recording) {}

	@Override
	public void setVisible() {
		fireSetPropertyVisible(this, "recording", canRecord());
	}

	public void addRecordingListener(RecordingListener listener) {
		this.listeners.add(RecordingListener.class, listener);
	}

	public void removeRecordingListener(RecordingListener listener) {
		this.listeners.remove(RecordingListener.class, listener);
	}

	protected void fireRecordingChanged(boolean recording) {
		for (RecordingListener listener : this.listeners.getListeners(RecordingListener.class))
			listener.recordingPropertyChanged(recording);
	}

	@Override
	public boolean canTrigger(long time) {
		return this.scenarioData != null;
	};
}
