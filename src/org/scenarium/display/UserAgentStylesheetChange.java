package org.scenarium.display;

import java.util.EventListener;

@FunctionalInterface
public interface UserAgentStylesheetChange extends EventListener{
	void userAgentStylesheetChange();
}
