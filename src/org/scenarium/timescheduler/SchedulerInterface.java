package org.scenarium.timescheduler;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.function.BiConsumer;

import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.ScenarioRemoteOperator;
import org.scenarium.filemanager.scenariomanager.LocalScenario;

public class SchedulerInterface {
	private final Scheduler scheduler;
	private Block remoteBlock;

	public SchedulerInterface(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public SchedulerInterface(Scheduler scheduler, Block remoteBlock) {
		this(scheduler);
		this.remoteBlock = remoteBlock;
	}

	public void addPropertyChangeListener(SchedulerPropertyChangeListener schedulerPropertyChangeListener) {
		this.scheduler.addPropertyChangeListener(schedulerPropertyChangeListener);
	}

	public void addTasks(ArrayList<ScheduleTask> schedulableTasks, BiConsumer<Long, Long> minMaxSupplier) {
		if (this.remoteBlock != null)
			for (int i = 0; i < schedulableTasks.size(); i++) {
				ScheduleTask st = schedulableTasks.get(i);
				schedulableTasks.set(i, new ScheduleTask(st.timeofIssue, st.timeStamp, new Schedulable() {

					@Override
					public void update(long timePointer) {
						ScenarioRemoteOperator remoteOp = (ScenarioRemoteOperator) SchedulerInterface.this.remoteBlock.getRemoteOperator();
						if (remoteOp != null)
							try {
								remoteOp.update(((LocalScenario) SchedulerInterface.this.remoteBlock.getOperator()).getTaskId(st.task), timePointer, SchedulerInterface.this.scheduler.getTimeStamp());
							} catch (RemoteException e) {
								e.printStackTrace();
							}
					}

					@Override
					public boolean canTrigger(long time) {
						ScenarioRemoteOperator remoteOp = (ScenarioRemoteOperator) SchedulerInterface.this.remoteBlock.getRemoteOperator();
						if (remoteOp != null)
							try {
								return remoteOp.canTrigger(time);
							} catch (RemoteException e) {
								e.printStackTrace();
							}
						return false;
					}
				}, st.seekIndex));
			}
		if (this.scheduler instanceof EvenementScheduler)
			((EvenementScheduler) this.scheduler).addTasks(schedulableTasks, minMaxSupplier);
	}

	public void clean() {
		this.scheduler.clean();
	}

	public SchedulerInterface derivateAsRemote(Block remoteBlock) {
		return new SchedulerInterface(this.scheduler, remoteBlock);
	}

	public long getBeginTime() {
		return this.scheduler.getBeginningTime();
	}

	public long getTimeStamp() {
		return this.scheduler.getTimeStamp();
	}

	public long getEndTime() {
		return this.scheduler.getEndTime();
	}

	public void removePropertyChangeListener(SchedulerPropertyChangeListener schedulerPropertyChangeListener) {
		this.scheduler.addPropertyChangeListener(schedulerPropertyChangeListener);
	}

	public void removeScheduleElement(Schedulable schedulable) {
		if (this.scheduler instanceof TimerScheduler)
			((TimerScheduler) this.scheduler).removeScheduleElement(schedulable);
	}

	public void setStartLock(CountDownLatch startLock) {
		this.scheduler.setStartLock(startLock);
	}

	public void setTriggerMode(TriggerMode triggerMode) {
		if (this.scheduler instanceof EvenementScheduler)
			((EvenementScheduler) this.scheduler).setTriggerMode(triggerMode);
	}

	public boolean stop() {
		return this.scheduler.stop();
	}

	public boolean stopAndWait() {
		return this.scheduler.stopAndWait();
	}

	public void setStartTime(long startTime) {
		this.scheduler.setStartTime(startTime);
	}

	public void setStopTime(long stopTime) {
		this.scheduler.setStopTime(stopTime);
	}

	public <T> T synchronisedCall(Callable<T> callable) throws Exception {
		synchronized (this.scheduler) {
			return callable.call();
		}
	}

	public boolean isRunning() {
		return this.scheduler.isRunning();
	}
}