/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.filerecorder;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.container.BeanEditor;
import org.scenarium.struct.raster.BufferedImageStrategy;

public abstract class FileStreamRecorder {
	private static HashMap<Class<?>, Class<? extends FileStreamRecorder>> streamMap = new HashMap<>();
	static {
		streamMap.put(BufferedImage.class, RawStreamRecorder.class);
		streamMap.put(BufferedImageStrategy.class, RawStreamRecorder.class);
	}

	public static FileStreamRecorder getRecorder(Class<?> typeClass) {
		Class<? extends FileStreamRecorder> recorder = streamMap.get(typeClass);
		if (recorder != null)
			try {
				return recorder.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				return null;
			}
		PropertyEditor<?> pe = PropertyEditorManager.findEditor(typeClass, "");
		if(pe instanceof BeanEditor<?>)
			pe = null;
		if (pe == null && Serializable.class.isAssignableFrom(typeClass))
			return new SerializableObjectStreamRecorder();
		if (pe != null)
			return new PropertyStreamRecorder(pe);
		return null;
	}

	public static boolean hasStreamRecorder(Class<?> typeClass) {
		return streamMap.get(typeClass) != null ? true : PropertyEditorManager.findEditor(typeClass, "") != null ? true : Serializable.class.isAssignableFrom(typeClass);
	}

	public static void registerStreamRecorder(Class<?> typeClass, Class<? extends FileStreamRecorder> streamClass) {
		streamMap.put(typeClass, streamClass);
	}

	public static void unregisterStreamRecorder(Class<?> typeClass) {
		streamMap.remove(typeClass);
	}

	public static void purgeStreamRecorder(Module module) {
		streamMap.keySet().removeIf(c -> c.getModule().equals(module));
	}

	protected String recordPath;

	protected String recordName;

	protected int timePointer = 0;

	public abstract void close() throws IOException;

	public abstract File getFile();

	public int getTimePointer() {
		return this.timePointer;
	}

	public abstract void push(Object object) throws IOException, FileNotFoundException;

	public void setRecordName(String recordName) {
		this.recordName = recordName;
	}

	public void setRecordPath(String recordPath) {
		this.recordPath = recordPath;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " -> " + this.recordName;
	}
}
