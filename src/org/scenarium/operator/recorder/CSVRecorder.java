/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.recorder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.primitive.number.NumberEditor;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;

public class CSVRecorder extends AbstractRecorder implements VarArgsInputChangeListener, EvolvedVarArgsOperator {
	private String[] outputLinkToInputName;
	private Class<?>[] outputLinkToInputType;
	private RecordDescriptor[] records;
	private Runnable varArgsInputChangedTask;
	private int recordPt;

	@Override
	public void birth() {
		if (!isRecording())
			return;
		this.outputLinkToInputName = getOutputLinkToInputNames();
		HashMap<String, ArrayList<Integer>> similar = new HashMap<>();
		for (int i = 0; i < this.outputLinkToInputName.length; i++) {
			String name = this.outputLinkToInputName[i];
			if (name != null) {
				ArrayList<Integer> groupName = similar.get(name);
				if (groupName == null) {
					groupName = new ArrayList<>();
					similar.put(name, groupName);
				}
				groupName.add(i);
			}
		}
		for (ArrayList<Integer> groupName : similar.values())
			if (groupName.size() != 1) {
				int i = 0;
				for (Integer id : groupName)
					this.outputLinkToInputName[id] += "_" + i++;
			}
		this.outputLinkToInputType = getOutputLinkToInputTypes();
		int nbRecorder = 0;
		for (Class<?> type : this.outputLinkToInputType)
			if (type != null)
				nbRecorder++;
		File rd = getRecordDirectory();
		if (rd == null || !rd.exists()) {
			setWarning(rd == null || rd.getName().isEmpty() ? "Empty record directory" : "The record directory: " + rd + " does not exists");
			return;
		}
		this.records = new RecordDescriptor[nbRecorder];
		this.recordPt = 0;
		for (int i = 0; i < nbRecorder; i++) {
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(this.outputLinkToInputType[i], null);
			if (editor == null)
				continue;
			try {
				this.records[i] = new RecordDescriptor(editor, rd, this.recordPt++ + "_" + this.outputLinkToInputName[i]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		addVarArgsInputChangeListener(this);
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return true;
	}

	@Override
	public void death() {
		removeVarArgsInputChangeListener(this);
		if (this.records != null) {
			for (RecordDescriptor rd : this.records)
				try {
					rd.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			this.records = null;
		}
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return PropertyEditorManager.findEditor(additionalInput, "") != null;
	}

	@ParamInfo(in = "r")
	public void process(final Object... objs) {
		if (this.records == null)
			return;
		if (this.varArgsInputChangedTask != null) {
			this.varArgsInputChangedTask.run();
			this.varArgsInputChangedTask = null;
		}
		for (int i = 0; i < objs.length; i++) {
			Object obj = objs[i];
			if (obj != null)
				try {
					this.records[i].pop(getTimeOfIssue(i), getTimeStamp(i), obj);
				} catch (IOException | ArrayIndexOutOfBoundsException e) {
					System.err.println(this.records.length);
					e.printStackTrace();
				}
		}
	}

	@Override
	public void varArgsInputChanged(int indexOfInput, int type) {
		this.outputLinkToInputName = getOutputLinkToInputNames();
		this.outputLinkToInputType = getOutputLinkToInputTypes();
		if (type == VarArgsInputChangeListener.CHANGED) {
			try {
				this.records[indexOfInput].close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(this.outputLinkToInputType[indexOfInput], null);
			try {
				this.records[indexOfInput] = editor == null ? null : new RecordDescriptor(editor, getRecordDirectory(), this.recordPt++ + this.outputLinkToInputName[indexOfInput]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (type == VarArgsInputChangeListener.NEW) {
			RecordDescriptor[] newRecords = new RecordDescriptor[indexOfInput];
			System.arraycopy(this.records, 0, newRecords, 0, this.records.length);
			int indexOfNewInput = newRecords.length - 1;
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(this.outputLinkToInputType[indexOfNewInput], null);
			try {
				newRecords[indexOfNewInput] = editor == null ? null : new RecordDescriptor(editor, getRecordDirectory(), this.recordPt++ + "_" + this.outputLinkToInputName[indexOfNewInput]);
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.records = newRecords;
		} else if (type == VarArgsInputChangeListener.REMOVE) {
			try {
				this.records[indexOfInput].close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			RecordDescriptor[] newRecords = new RecordDescriptor[this.records.length - 1];
			System.arraycopy(this.records, 0, newRecords, 0, newRecords.length);
			for (int i = indexOfInput + 1; i < this.records.length; i++)
				newRecords[i - 1] = this.records[i];
			this.records = newRecords;
		}
	}
}

class RecordDescriptor {
	private final boolean needQuotationMark;
	private final PropertyEditor<?> editor;
	private final FileWriter writter;

	public RecordDescriptor(PropertyEditor<?> editor, File recordDirectory, String name) throws IOException {
		this.needQuotationMark = !(editor instanceof NumberEditor);
		this.editor = editor;
		this.writter = new FileWriter(recordDirectory.toPath().resolve(name + ".csv").toFile()); // new RandomAccessFile(recordFile, "rw");//
																									// Files.newBufferedWriter(recordDirectory.toPath().resolve(name + ".csv"), CHARSET,
																									// StandardOpenOption.SYNC, StandardOpenOption.CREATE);
		this.writter.write("TimeOfIssue,TimeStamp," + name + "\n");
	}

	public void close() throws IOException {
		this.writter.close();
	}

	public void pop(long timeOfIssue, long timeStamp, Object obj) throws IOException {
		this.editor.setValueFromObj(obj);
		this.writter.write(timeOfIssue + "," + timeStamp + (this.needQuotationMark ? ",\"" + this.editor.getAsText() + "\"\n" : "," + this.editor.getAsText() + "\n"));
	}
}
