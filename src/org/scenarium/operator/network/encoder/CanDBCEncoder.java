/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.network.encoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.beanmanager.editors.DynamicChoiceBox;
import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.UpdatableViewBean;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.struct.Selection;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.communication.can.LoadedListener;
import org.scenarium.communication.can.dbc.CanDBC;
import org.scenarium.communication.can.dbc.DBCMessage;
import org.scenarium.communication.can.dbc.DBCSignal;
import org.scenarium.communication.can.dbc.EncodingMessageProperties;
import org.scenarium.communication.can.dbc.EncodingSignalProperties;
import org.scenarium.communication.can.dbc.MessageIdentifier;
import org.scenarium.editors.NotChangeableAtRuntime;
import org.scenarium.editors.can.SendMode;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class CanDBCEncoder extends EvolvedOperator implements LoadedListener, DynamicEnableBean, UpdatableViewBean {
	// TODO prend pas en compte le changement de timer temps réel d'un signal ou le changement de filters, rien du tout en faite
	@BeanInfo(alwaysExtend = true)
	@PropertyInfo(index = 0, info = "DBC database for encoding datas")
	@NotChangeableAtRuntime
	private CanDBC canDBC;
	@PropertyInfo(index = 1, info = "Input filters based on available signal in the DBC database")
	@DynamicChoiceBox(possibleChoicesMethod = "getVariablesIdentifier")
	private Selection<MessageIdentifier> filters;
	@PropertyInfo(index = 2, info = "Input enum as string or as double value")
	private boolean enumAsString;
	@PropertyInfo(index = 3, info = "Properties for CAN encoding and triggering")
	@DynamicChoiceBox(possibleChoicesMethod = "getPossibleMessages")
	@NotChangeableAtRuntime
	private EncodingMessageProperties[] messagesProperties = null;
	private final HashMap<String, int[]> triggerMap = new HashMap<>();
	private final HashMap<Integer, InputData> inputOnNewDataMap = new HashMap<>(); // Map index entré vers InputData
	private final HashMap<Integer, byte[]> dataMap = new HashMap<>(); // Map id vers data pour external trigget et timer

	private ArrayList<CanTrame> canTramesToTrigger = new ArrayList<>();
	private MessageTimer signalTimer;
	private final ArrayList<String> triggers = new ArrayList<>();

	private EncodingMessageProperties[] oldMessagesProperties;

	private void addMessage(EncodingMessageProperties prop) {
		if (prop.getSendMode() == SendMode.TIMER) {
			if (this.signalTimer == null) {
				this.signalTimer = new MessageTimer();
				this.signalTimer.add(prop);
				this.signalTimer.start();
			} else
				this.signalTimer.add(prop);
		} else if (prop.getSendMode() == SendMode.ONEXTERNALTRIGGER)
			addToTriggerMap(prop);
	}

	private void addToTriggerMap(EncodingMessageProperties messagesPropertie) {
		String name = messagesPropertie.getAdditionalInfo();
		if (name != null && !name.isEmpty()) {
			int[] triggerList = this.triggerMap.get(name);
			if (triggerList == null)
				triggerList = new int[] { messagesPropertie.getId() };
			else {
				int[] newTriggerList = new int[triggerList.length + 1];
				System.arraycopy(triggerList, 0, newTriggerList, 0, triggerList.length);
				newTriggerList[triggerList.length] = messagesPropertie.getId();
				triggerList = newTriggerList;
			}
			this.triggerMap.put(name, triggerList);
		}
		this.triggerMap.forEach((triggerName, ids) -> System.out.println(triggerName + ": " + Arrays.toString(ids)));
	}

	@Override
	public void birth() {
		launch();
	}

	@Override
	public void death() {
		if (this.signalTimer != null) {
			this.signalTimer.stop();
			this.signalTimer = null;
		}
		this.dataMap.clear();
		this.triggerMap.clear();
	}

	public CanDBC getCanDBC() {
		return this.canDBC;
	}

	public Selection<MessageIdentifier> getFilters() {
		return this.filters;
	}

	public EncodingMessageProperties[] getMessagesProperties() {
		return this.messagesProperties;
	}

	public MessageIdentifier[] getPossibleMessages() {
		return this.filters == null ? null : this.filters.getSelected().toArray(MessageIdentifier[]::new);
	}

	public Object[] getVariablesIdentifier() {
		return this.canDBC == null ? new MessageIdentifier[0] : this.canDBC.getMessagesIdentifier().toArray();
	}

	@Override
	public void initStruct() {
		this.inputOnNewDataMap.clear();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		this.triggers.clear();
		int inputMapIndex = 0;
		if (this.canDBC != null && this.messagesProperties != null && this.messagesProperties.length != 0) {
			this.canDBC.loadIfNeeded();
			ArrayList<EncodingMessageProperties> selectedOutputs = new ArrayList<>(Arrays.asList(this.messagesProperties));
			selectedOutputs.sort((a, b) -> Integer.compare(a.getId(), b.getId()));
			for (EncodingMessageProperties emp : selectedOutputs) {
				if (emp.getSendMode() == SendMode.ONEXTERNALTRIGGER && emp.getAdditionalInfo() != null && !this.triggers.contains(emp.getAdditionalInfo()))
					this.triggers.add(emp.getAdditionalInfo());
				DBCMessage message = this.canDBC.getPropById(emp.getId());
				if (message == null || !message.getName().equals(emp.getName()))
					continue;
				boolean[] updatedData = null;
				if (emp.getSendMode() == SendMode.ONNEWSETOFDATAS)
					updatedData = new boolean[message.getSignals().size()];
				for (EncodingSignalProperties esp : emp.getSignalProperties())
					if (esp.isAsInput()) {
						DBCSignal signal = message.getSignal(esp.getName());
						names.add(emp.getId() + "_" + signal.name);
						types.add(this.enumAsString ? signal.getDataType() : Double.class);
						this.inputOnNewDataMap.put(inputMapIndex++, new InputData(message.getId(), signal, new byte[message.getSize()], updatedData));
					}
			}
		}
		for (String triggerName : this.triggers) {
			names.add(triggerName);
			types.add(Boolean.class);
		}
		updateInputs(names.toArray(String[]::new), types.toArray(Class[]::new));
	}

	private void launch() {
		this.dataMap.clear();
		this.triggerMap.clear();
		if (this.signalTimer != null) {
			this.signalTimer.stop();
			this.signalTimer = null;
		}
		if (this.messagesProperties == null)
			return;
		Collection<InputData> inputDatas = this.inputOnNewDataMap.values();
		for (EncodingMessageProperties emp : this.messagesProperties) {
			DBCMessage message = this.canDBC.getPropById(emp.getId());
			byte[] trameData = null;
			for (InputData inputData : inputDatas)
				if (inputData.getId() == message.getId())
					trameData = inputData.getCanData();
			if (trameData == null)
				trameData = new byte[message.getSize()];
			byte[] finalTrameData = trameData;
			emp.getSignalProperties().forEach(esp -> message.getSignal(esp.getName()).encode(finalTrameData, esp.getValue())); // Initialisation des valeurs par default
			if (emp.getSendMode() == SendMode.ONEXTERNALTRIGGER)
				addToTriggerMap(emp);
			else if (emp.getSendMode() == SendMode.TIMER) {
				if (this.signalTimer == null)
					this.signalTimer = new MessageTimer();
				this.signalTimer.add(emp);
			}
			this.dataMap.put(emp.getId(), trameData);
		}
		this.canTramesToTrigger = new ArrayList<>(this.messagesProperties.length);
		if (this.signalTimer != null)
			this.signalTimer.start();
	}

	@Override
	public void loaded() {
		initStruct();
		// fireStructChanged();
		if (this.canDBC != null && this.canDBC.hasMessage() && this.filters != null) { // || filters.getSelected().isEmpty() si je mets ca pas d'update quad je vide la liste
			ArrayList<Object> toRemove = new ArrayList<>();
			ArrayList<MessageIdentifier> variablesIdentifier = this.canDBC.getMessagesIdentifier();
			for (MessageIdentifier selection : this.filters.getSelected())
				if (!variablesIdentifier.contains(selection))
					toRemove.add(selection);
			if (!toRemove.isEmpty()) {
				HashSet<MessageIdentifier> sel = this.filters.getSelected();
				sel.removeAll(toRemove);
				this.filters = new Selection<>(sel, MessageIdentifier.class);
			}
			EncodingMessageProperties[] newMessagesProperties = new EncodingMessageProperties[this.filters.getSelected().size()];
			int index = 0;
			for (MessageIdentifier encodingMessageProperties : this.filters.getSelected())
				newMessagesProperties[index++] = new EncodingMessageProperties(encodingMessageProperties.getId(), encodingMessageProperties.getName());
			this.messagesProperties = newMessagesProperties;
		} else
			this.filters = null;
		setEnable();
		updateView();
	}

	public CanTrame process() {
		Object[] inputs = getAdditionalInputs();
		synchronized (this) {
			for (int i = 0; i < inputs.length; i++) {
				Object input = inputs[i];
				if (input != null)
					if (input instanceof Boolean) { // C'est un trigger, voir la liste pour avoir son index dedans, attention synchro lors du changement
						String trigger = this.triggers.get(inputs.length - this.triggers.size() + i);
						for (int id : this.triggerMap.get(trigger))
							this.canTramesToTrigger.add(new CanTrame(id, true, this.dataMap.get(id)));
					} else {
						InputData inputData = this.inputOnNewDataMap.get(i); // les signaux sont toujours avant et les triggers après, donc pas de problème avec i
						if (inputData != null) { // null si pas dans un mode de trigger sur data
							if (input instanceof Double)
								inputData.getSignal().encode(inputData.getCanData(), (Double) input);
							else
								inputData.getSignal().encodeAsString(inputData.getCanData(), (String) input);
							boolean[] updatedData = inputData.getUpdatedData();
							if (updatedData == null)
								this.canTramesToTrigger.add(new CanTrame(inputData.getId(), true, inputData.getCanData()));
							else {
								boolean ready = true;
								ArrayList<DBCSignal> signals = this.canDBC.getPropById(inputData.getId()).getSignals();
								for (int j = 0; j < signals.size(); j++)
									if (inputData.getSignal() == signals.get(j))
										updatedData[j] = true;
									else if (!updatedData[j])
										ready = false;
								if (ready) {
									this.canTramesToTrigger.add(new CanTrame(inputData.getId(), true, inputData.getCanData()));
									Arrays.fill(updatedData, false);
								}
							}
						}
					}
			}
		}
		this.canTramesToTrigger.forEach(canTrame -> System.out.println("Trigger output: " + canTrame));
		this.canTramesToTrigger.forEach(canTrame -> triggerOutput(canTrame));
		this.canTramesToTrigger.clear();
		return null;
	}

	private void purgeMessage(EncodingMessageProperties prop) {
		if (prop.getSendMode() == SendMode.TIMER && this.signalTimer.remove(prop))
			this.signalTimer = null;
		else if (prop.getSendMode() == SendMode.ONEXTERNALTRIGGER)
			removeToTriggerMap(prop);
	}

	private void removeToTriggerMap(EncodingMessageProperties messagesPropertie) {
		ArrayList<String> toRemove = new ArrayList<>();
		this.triggerMap.forEach((triggerName, ids) -> {
			for (int id : ids)
				if (id == messagesPropertie.getId()) {
					if (ids.length == 1) {
						toRemove.add(triggerName);
						return;
					}
					int[] newTriggerList = new int[ids.length - 1];
					int index = 0;
					for (int i = 0; i < ids.length; i++)
						if (ids[i] != messagesPropertie.getId())
							newTriggerList[index++] = ids[i];
					this.triggerMap.put(triggerName, newTriggerList);
				}
		});
		toRemove.forEach(e -> this.triggerMap.remove(e));
		this.triggerMap.forEach((triggerName, ids) -> System.out.println(triggerName + ": " + Arrays.toString(ids)));
	}

	public void setCanDBC(CanDBC canDBC) {
		this.canDBC = canDBC;
		loaded();
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "filters", this.canDBC != null && this.canDBC.hasMessage());
		fireSetPropertyEnable(this, "messagesProperties", this.canDBC != null && this.canDBC.hasMessage());
	}

	public void setFilters(Selection<MessageIdentifier> filters) {
		this.filters = new Selection<>(this.canDBC == null || filters == null ? new HashSet<>()
				: this.canDBC.getMessagesIdentifier().stream().filter(e -> filters.getSelected().contains(e)).collect(Collectors.toCollection(HashSet::new)), MessageIdentifier.class);
		loaded();
	}

	public void setMessagesProperties(EncodingMessageProperties[] messagesProperties) {
		if (isAlive()) {
			ArrayList<EncodingMessageProperties> oldMessages = new ArrayList<>(Arrays.asList(this.oldMessagesProperties));
			ArrayList<EncodingMessageProperties> messages = new ArrayList<>(Arrays.asList(messagesProperties));
			this.messagesProperties = messagesProperties;
			initStruct();
			for (int i = 0; i < messagesProperties.length; i++)
				for (int j = 0; j < this.oldMessagesProperties.length; j++) {
					EncodingMessageProperties prop = messagesProperties[i];
					EncodingMessageProperties oldProp = this.oldMessagesProperties[j];
					if (prop.equals(oldProp)) { // Ok c'est les mêmes avant et après, je regarde les signaux
						oldMessages.remove(oldProp);
						messages.remove(prop);
						ArrayList<EncodingSignalProperties> signalsProp = prop.getSignalProperties();
						ArrayList<EncodingSignalProperties> oldSignalsProp = oldProp.getSignalProperties();
						for (int k = 0; k < signalsProp.size(); k++)
							for (int l = 0; l < oldSignalsProp.size(); l++) {
								EncodingSignalProperties signalProp = signalsProp.get(k);
								EncodingSignalProperties oldSignalProp = oldSignalsProp.get(l);
								if (signalProp.equals(oldSignalProp)) {
									if (signalProp.isAsInput() != oldSignalProp.isAsInput())
										System.out.println(signalProp.getName() + " changed asinput");
									else if (signalProp.getValue() != oldSignalProp.getValue()) {
										this.canDBC.getPropById(prop.getId()).getSignal(signalProp.getName()).encode(this.dataMap.get(prop.getId()), signalProp.getValue());
										DBCSignal.showBytes(this.dataMap.get(prop.getId()));
										System.out.println(signalProp.getName() + " changed value");
									}
									break;
								}
							}
						if (oldProp.getSendMode() != prop.getSendMode()) {
							purgeMessage(oldProp);
							addMessage(prop);
						}
						try {
							if ((prop.getSendMode() == SendMode.TIMER || prop.getSendMode() == SendMode.ONEXTERNALTRIGGER)
									&& (oldProp.getAdditionalInfo() == null || !oldProp.getAdditionalInfo().equals(prop.getAdditionalInfo())))
								if (prop.getSendMode() == SendMode.TIMER)
									this.signalTimer.changePeriod(prop);
								else if (prop.getSendMode() == SendMode.ONEXTERNALTRIGGER) {
									removeToTriggerMap(prop);
									addToTriggerMap(prop);
									System.out.println("changement de trigger");
								}
						} catch (NullPointerException e) {
							e.printStackTrace();
						}
						break;
					}
				}
			oldMessages.forEach(oldProp -> {
				purgeMessage(oldProp);
				this.dataMap.remove(oldProp.getId());
			});
			messages.forEach(emp -> {
				addMessage(emp);
				this.dataMap.put(emp.getId(), new byte[this.canDBC.getPropById(emp.getId()).getSize()]);
				// Collection<InputData> inputDatas = inputOnNewDataMap.values();
				// DBCMessage message = canDBC.getPropById(emp.getId());
				// byte[] trameData = null;
				// for (InputData inputData : inputDatas)
				// if (inputData.getId() == message.getId())
				// trameData = inputData.getCanData();
				// if (trameData == null)
				// trameData = new byte[message.getSize()];
			});
			if (!oldMessages.isEmpty())
				System.out.println("old remove message: " + oldMessages.toString());
			if (!messages.isEmpty())
				System.out.println("new add message: " + messages.toString());
		} else {
			ArrayList<EncodingMessageProperties> filteredEMP = new ArrayList<>();
			for (EncodingMessageProperties encodingMessageProperties : messagesProperties)
				if (this.canDBC.getPropById(encodingMessageProperties.getId()) != null)
					filteredEMP.add(encodingMessageProperties);
			this.messagesProperties = filteredEMP.size() == messagesProperties.length ? messagesProperties : filteredEMP.toArray(EncodingMessageProperties[]::new);
			initStruct();
		}
		this.oldMessagesProperties = Arrays.stream(messagesProperties).map(e -> e.clone()).toArray(EncodingMessageProperties[]::new);
	}

	@Override
	public void updateView() {
		fireUpdateView(this, "filters", false);
		fireUpdateView(this, "messagesProperties", false);
	}

	class MessageTimer {
		class MessageTimerInfo {
			private int nextRun;
			private final int period;
			private final ArrayList<DBCMessage> messages;

			public MessageTimerInfo(int period) {
				this.period = period;
				this.messages = new ArrayList<>();
				this.nextRun = period;
			}

			public void addMessage(DBCMessage message) {
				this.messages.add(message);
			}

			public ArrayList<DBCMessage> getMessages() {
				return this.messages;
			}

			public int getNextRun() {
				return this.nextRun;
			}

			public int getPeriod() {
				return this.period;
			}

			public void incNextRun() {
				this.nextRun += this.period;
			}

			public void incNextRun(long nbPeriod) {
				this.nextRun += nbPeriod * this.period;
			}

			@Override
			public String toString() {
				return "nextRun: " + this.nextRun + " period: " + this.period;// + " messages: " + messages;
			}
		}

		private final ArrayList<EncodingMessageProperties> encodingMessageProperties = new ArrayList<>();
		private long restTime = 0;
		private Thread timerThread;
		private MessageTimerInfo group = null;
		private LinkedList<MessageTimerInfo> timeTasks;
		long startTime = 0;

		long timeToReach;

		public void add(EncodingMessageProperties emp) {
			if (this.startTime == 0)
				this.encodingMessageProperties.add(emp);
			else {
				stop();
				this.encodingMessageProperties.add(emp);
				addAtRuntime(emp);
				start();
			}
		}

		private void addAtRuntime(EncodingMessageProperties emp) {
			try {
				int period = Integer.parseInt(emp.getAdditionalInfo());
				DBCMessage message = CanDBCEncoder.this.canDBC.getPropById(emp.getId());
				for (MessageTimerInfo mti : this.timeTasks)
					if (mti.getPeriod() == period) {
						mti.messages.add(message);
						return;
					}
				MessageTimerInfo mti = new MessageTimerInfo(period);
				mti.addMessage(message);
				mti.incNextRun((this.timeToReach - this.startTime - this.restTime) / period);
				ListIterator<MessageTimerInfo> it = this.timeTasks.listIterator();
				while (it.hasNext()) {
					MessageTimerInfo element = it.next();
					if (element.getNextRun() > mti.getNextRun()) {
						it.previous();
						it.add(mti);
						return;
					}
				}
				it.add(mti);
			} catch (NumberFormatException e) {}
		}

		public void changePeriod(EncodingMessageProperties emp) {
			stop();
			removeAtRuntime(emp);
			addAtRuntime(emp);
			start();

		}

		public boolean remove(EncodingMessageProperties emp) {
			stop();
			this.encodingMessageProperties.remove(emp);
			if (this.encodingMessageProperties.isEmpty())
				return true;
			removeAtRuntime(emp);
			start();
			return false;
		}

		private void removeAtRuntime(EncodingMessageProperties emp) {
			DBCMessage messageToChange = CanDBCEncoder.this.canDBC.getPropById(emp.getId());
			Iterator<MessageTimerInfo> iterator = this.timeTasks.iterator();
			while (iterator.hasNext()) {
				MessageTimerInfo mti = iterator.next();
				ArrayList<DBCMessage> messages = mti.messages;
				int indexOfMessage = messages.indexOf(messageToChange);
				if (indexOfMessage != -1) {
					messages.remove(indexOfMessage);
					if (messages.isEmpty())
						iterator.remove();
					return;
				}
			}
			if (this.group != null) {
				ArrayList<DBCMessage> messages = this.group.messages;
				int indexOfMessage = messages.indexOf(messageToChange);
				if (indexOfMessage != -1) {
					messages.remove(indexOfMessage);
					if (messages.isEmpty())
						this.group = null;
					return;
				}
			}
		}

		public void start() {
			if (this.timerThread != null)
				stop();
			this.timerThread = new Thread() {

				@Override
				public void run() {
					if (MessageTimer.this.timeTasks == null) {
						HashMap<Integer, MessageTimerInfo> messageIndex = new HashMap<>();
						for (int i = 0; i < MessageTimer.this.encodingMessageProperties.size(); i++) {
							EncodingMessageProperties emp = MessageTimer.this.encodingMessageProperties.get(i);
							int period = Integer.parseInt(emp.getAdditionalInfo());
							MessageTimerInfo mti = messageIndex.get(period);
							if (mti == null) {
								mti = new MessageTimerInfo(period);
								messageIndex.put(period, mti);
							}
							mti.addMessage(CanDBCEncoder.this.canDBC.getPropById(emp.getId()));
						}
						MessageTimer.this.timeTasks = new LinkedList<>(messageIndex.values());
						MessageTimer.this.timeTasks.sort((a, b) -> Integer.compare(a.getPeriod(), b.getPeriod()));
						MessageTimer.this.startTime = System.currentTimeMillis();
					}
					MessageTimer.this.timeToReach = System.currentTimeMillis();
					try {
						next: while (true) {
							long timeTowait;
							if (MessageTimer.this.restTime != -1) {
								if (MessageTimer.this.group != null)
									MessageTimer.this.restTime = 0;
								timeTowait = MessageTimer.this.restTime;
								MessageTimer.this.timeToReach += timeTowait;
								MessageTimer.this.restTime = -1;
							} else {
								MessageTimer.this.group = MessageTimer.this.timeTasks.pop();
								MessageTimer.this.timeToReach = MessageTimer.this.startTime + MessageTimer.this.group.getNextRun();
								timeTowait = MessageTimer.this.timeToReach - System.currentTimeMillis();
							}
							if (timeTowait > 0)
								synchronized (this) {
									wait(timeTowait);
								}
							if (MessageTimer.this.group != null) {
								ArrayList<DBCMessage> messages = MessageTimer.this.group.getMessages();
								ArrayList<byte[]> datas = new ArrayList<>();
								synchronized (CanDBCEncoder.this) {
									messages.forEach(message -> datas.add(CanDBCEncoder.this.dataMap.get(message.getId())));
								}
								for (int i = 0; i < messages.size(); i++) {
									DBCMessage message = messages.get(i);
									triggerOutput(new CanTrame(message.getId(), true, datas.get(i)));
								}
								MessageTimer.this.group.incNextRun();
								ListIterator<MessageTimerInfo> it = MessageTimer.this.timeTasks.listIterator();
								while (it.hasNext()) {
									MessageTimerInfo element = it.next();
									if (element.getNextRun() > MessageTimer.this.group.getNextRun()) {
										it.previous();
										it.add(MessageTimer.this.group);
										continue next;
									}
								}
								if (MessageTimer.this.group.messages.isEmpty())
									System.err.println("error empty");
								it.add(MessageTimer.this.group);
								MessageTimer.this.group = null;
							}
						}
					} catch (InterruptedException e) {
						MessageTimer.this.restTime = MessageTimer.this.timeToReach - System.currentTimeMillis();
					} catch (NoSuchElementException e) {
						MessageTimer.this.timerThread = null;
						return;
					}
				}
			};
			this.timerThread.start();
		}

		public void stop() {
			if (this.timerThread != null) {
				this.timerThread.interrupt();
				try {
					this.timerThread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

class InputData {
	private final int id;
	private final DBCSignal signal;
	private final byte[] canData;
	private final boolean[] updatedData;

	public InputData(int id, DBCSignal signal, byte[] canData, boolean[] updatedData) {
		this.id = id;
		this.signal = signal;
		this.canData = canData;
		this.updatedData = updatedData;
	}

	public byte[] getCanData() {
		return this.canData;
	}

	public int getId() {
		return this.id;
	}

	public DBCSignal getSignal() {
		return this.signal;
	}

	public boolean[] getUpdatedData() {
		return this.updatedData;
	}
}
