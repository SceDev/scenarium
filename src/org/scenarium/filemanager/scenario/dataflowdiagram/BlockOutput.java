/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.lang.invoke.MethodHandle;

import org.beanmanager.tools.WrapperTools;

public class BlockOutput extends Output implements BlockIO {
	protected final Block block;
	private final MethodHandle methodHandle;

	public BlockOutput(Block block, MethodHandle method, Class<?> type, String name) {
		super(type, name);
		this.block = block;
		this.methodHandle = method;
	}

	@Override
	public Block getBlock() {
		return this.block;
	}

	public MethodHandle getMethodHandle() {
		return this.methodHandle;
	}

	void setType(Class<?> type) {
		this.type = WrapperTools.toWrapper(type);
		if (this.block.operator instanceof EvolvedOperator)
			((EvolvedOperator) this.block.operator).fireStructChanged();
		else
			this.block.fireStructChange();
	}
}
