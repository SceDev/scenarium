/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.dataprocessing.vector;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.InputLinksChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;

public class Vectorizer extends EvolvedOperator implements EvolvedVarArgsOperator, InputLinksChangeListener {
	private Class<?> type;

	@Override
	public void birth() {}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return true;
	}

	@Override
	public void death() {}

	@Override
	public void initStruct() {
		if (this.type != null)
			updateOutputs(new String[] { "Array" }, new Class<?>[] { this.type.arrayType() });
		else
			updateOutputs(new String[] {}, new Class<?>[] {});
	}

	@Override
	public void inputLinkChanged(int indexOfInput) {
		if (indexOfInput == 0) {
			this.type = getOutputLinkToInputTypes()[0];
			initStruct();
		}
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput != null && inputsType.length == 0 || inputsType[0] == null || additionalInput == this.type;
	}

	@ParamInfo(in = "In")
	public void process(Object... objs) {
		long maxTimeStamp = Long.MIN_VALUE;
		for (int i = 0; i < objs.length; i++) {
			long ts = getTimeStamp(i);
			if (ts > maxTimeStamp)
				maxTimeStamp = ts;
		}
		triggerOutput(new Object[] { objs }, maxTimeStamp);
	}
}
