/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.network.bus.serial;

import gnu.io.SerialPort;

public enum RXTXParity {
	PARITY_NONE(SerialPort.PARITY_NONE), PARITY_ODD(SerialPort.PARITY_ODD), PARITY_EVEN(SerialPort.PARITY_EVEN), PARITY_MARK(SerialPort.PARITY_MARK), PARITY_SPACE(SerialPort.PARITY_SPACE);

	private final int value;

	private RXTXParity(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
