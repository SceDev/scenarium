/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.viewer.conversion;

import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.display.drawer.geo.CrossMarker;
import org.scenarium.struct.GeographicCoordinate;

import javafx.scene.paint.Color;

public class ToCrossMarker {
	@NumberInfo(min = 1)
	private double crossSize = 1;
	private Color color = Color.BLACK;
	private boolean skewed = false;

	public void birth() {}

	public CrossMarker[] process(GeographicCoordinate[] geoCoord) {
		CrossMarker[] cm = new CrossMarker[geoCoord.length];
		for (int i = 0; i < cm.length; i++)
			cm[i] = new CrossMarker(geoCoord[i], this.crossSize, this.color, this.skewed);
		return cm;
	}

	public void death() {}

	public double getCrossSize() {
		return this.crossSize;
	}

	public void setCrossSize(double crossSize) {
		this.crossSize = crossSize;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isSkewed() {
		return this.skewed;
	}

	public void setSkewed(boolean skewed) {
		this.skewed = skewed;
	}
}
